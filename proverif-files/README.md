This folder contains the ProVerif files we developed to conduct the security analysis presented in the paper:
    Election Eligibility with OpenID: Turning Authentication into Transferable Proof of Eligibility.

## Files
### Models for OIDELi-id and OIDELI-zk: in [`oideli/`](oideli)
- `default.pvl` is a ProVerif library file that specifies the common term algebra, the events, and restrictions used for all our models. It is loaded by 
default by ProVerif when verifying the other files.

- `oideli_id-eligibility-and-others.pv` and `oideli_id-id-hiding.pv` contain models of the OIDELI-id protocol, i.e., in which no ZKP is used to protect everlasting privacy. The expected results are: all the properties are proven true in `oideli_id-eligibility-and-others.pv`, all properties, which are equivalence properties, are falsified in `oideli_id-id-hiding.pv`. 

- `oideli_zk-eligibility-and-others.pv` and `oideli_zk-id-hiding.pv` contain models of the OIDELI-zk protocol thus including the ZKP to protect everlasting privacy. The expected results are: all the properties are proven true in both files. 

### Models for Helios-like protocol with/without OIDELi: in [`voting-protocol/`](voting-protocol/)
- `default.pvl` is similar to the above.
- there are 3 models for each version of the protocol under study: 
    + `[version_of_protocol]-verifiability.pv`: models verifiability properties (individual, universal, and it recalls eligibility too). 
    + `[version_of_protocol]-vote-secrecy.pv`: models the vote secrecy properties (as an equivalence property between a scenario in which Alice votes 0 and Bob 1 and a scenario with swapped votes). 
    + `[version_of_protocol]-everlasting.pv`: models the practical everlasting property. 
- the three protocol variants are: 
    + `voting_protocol`: the original Helios-like voting protocol.
    + `voting_protocol+oideli_id`: the original Helios-like voting protocol extended with the OIDELI-id protocol.
    + `voting_protocol+oideli_zk`: the original Helios-like voting protocol extended with the OIDELI-zk protocol.


**Note:** Proverif requires up to 72s to conclude on `voting_protocol+oideli_zk-everlasting.pv` and takes less than one minute for the other files.


### Reproducibility
Any environment supporting ProVerif should be suitable to run the proofs. Tested on MacOS and Fedora 38.

All the proofs have been obtained using ProVerif v2.05. To run a proof, first navigate to the corresponding folder (`cd oideli` or `cd voting-protocol`) and execute ProVerif using the following command:
`proverif -lib default.pvl <file_to_analyze.pv>`.

### Modelling choices 
- in `voting-protocol-vote-secrecy.pv` voters individual checks are not modelled. However, to model vote secrecy it assumes that both Alice's and Bob's ballot have been counted in the tally. In practice, this can be achieved with individual verifiability but this limits vote secrecy to voters who verify. The model keeps as general as possible. 

### Threat models
#### Voting-protocol
The threat model depends on the security goal:
 - Everlasting privacy: passive attacker during voting phase, breaking conditional crypto after, other RPs can be malicious
 - Vote secrecy: dishonest voting server, idP, voters, other RPs
 - Eligibility: dishonest voting server, voters, other RPs, tally authorities 
 - Eligibility+ballot identification (through idP): dishonest server, voters (except the one who casts the ballot under consideration), other RPs, tally authorities 
 - Individual verifiability: dishonest voting server, idP, voters, other RPs, tally authorities 
 - Universal verifiability: dishonest voting server, idP, voters, other RPs, tally authorities

#### OIDEli
The threat model depends on the security goal:
 - Eligibility: all entities except the idP are dishonest
 - Eligibility+ballot identification (through idP)+authenticated-as-expected: all entities except the idP are dishonest, also the voters who cast the ballot under consideration should be honest.
 - ID-hiding (strong secrecy of voters' identities `id`): dishonest voters (except the one whose identity must be protected)