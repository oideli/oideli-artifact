(******************************************************************************
 ProVerif models for the paper:
  Election Eligibility with OpenID:
  Turning Authentication into Transferable Proof of Eligibility
******************************************************************************)

(* OIDELi-zk + Helios-like protocol:
   This file specified the term algebra, the events, and restrictions used for
   all voting protocol models. It is loaded by default by ProVerif. *)

(* set privateCommOnPublicTerms = false. *)
(* set ignoreTypes = false. *)
(* set verboseClauses = explained. *)
(* set verboseClauses = short. *)
(* set verboseRules = true. *)
(* set reconstructTrace = false. *)
(* set removeEventsForLemma = true. *)

type secrect_for_debug. 
const target_for_debug:secrect_for_debug [private].

(*** Everlasting privacy modelling: 
- we define a trapdoor to inverse all the crypto primitives that can be broken (i.e. all but hash functions and ZKPs which are information theoretic secure) 
- the trapdoor is revealed to the attacker in phase 2 (we don't use phases but only leak the trapdoor "after the Tally has been done")
*)
type trapdoor.
const trapdoor_everlasting:trapdoor [private]. 
const c_toPhase2:channel. (* channel used to transfer relevant information from phase 1 to phase 2 *)

(***************** THEORY *****************)
(*** TYPES *)
type aud_t.  (* identifier for the Relying Party (RP) *)
type vote. fun t_vote(vote):bitstring [typeConverter].
type ballot. fun t_ballot(ballot):bitstring [typeConverter]. fun mk_ballot(bitstring):ballot [typeConverter].
type nonce. fun t_nonce(nonce):bitstring [typeConverter].
type name. fun t_name(name):bitstring [typeConverter].
type login_t. fun t_login(login_t):bitstring [typeConverter].
type password_t. fun t_password(password_t):bitstring [typeConverter].
type id_t. fun t_id(id_t):bitstring [typeConverter].
type sub_t.fun t_sub(sub_t):bitstring [typeConverter].
fun Id(name):id_t .
fun login(name):login_t. (* Attacker can compute *)
fun password(name):password_t [private]. (* Attacker cannot compute *)
type el_name. (* type of eligible identities *)
fun is_eligible(el_name):name [private]. (* This is used to model the fact that some identies are eligible, others are not. Formally, thos models ID_{el} from the paper. Attacker has an oracle to this functon so that he
can create eligible identities too. *)

reduc forall el_user:el_name; check_is_eligible(Id(is_eligible(el_user))) = true.
fun padID(bitstring,id_t):bitstring [data]. (* padID is modelled as a data constructor hence modelling that id can leak (actually |leak|) from such a message *)
reduc forall n:bitstring,id:id_t; extractN(padID(n,id)) = n.

(*** CONSTANTS *)
const ok,ko,REQ,RESP:bitstring.
const ok_nonce:nonce.
free idP:name. (* there is a unique identity provider *)
const AUD_voting:aud_t. (* the identity of the Relying Party (RP) corresponding to the voting server *)


(*** CHANNELS *)
(* Voters <-> idP: End User (or an attacker) needs login, password to communicate with an OpenID Provider *)
fun c_idP(login_t,password_t) : channel .
(* Channel to the attacker *)
free c_A : channel. (* Always public (models a direct channel from and to the attacker) *)
(* Voters <-> Voting Server *)
free c_S : channel. (* Public for all properties except privacy properties, controlled by the attacker *)
free c_BB : channel [private]. (* Always public (the BB is made public), controlled by the attacker *)

(* For modeling equivalence properties (e.g., id-hiding) *)
const c_S_hon:channel [private]. 
const c_S_dis:channel.
const el_user_cst: el_name.

(*** TABLES *)
table IdP_UserInfo_DB(aud_t,name,login_t,password_t,id_t,sub_t). (* Stores the OpenID Provider database of users. *)

(*** CRYPTOGRAPHIC PRIMITIVES *)
(* Asymmetric encryption *)
type skey.
type pkey.
type random.
fun pk(skey):pkey.
reduc forall x1:skey; EPtrapdoor_pk(pk(x1),trapdoor_everlasting) = x1.
fun aenc(pkey,bitstring,random):bitstring.
reduc forall sk:skey, x:bitstring, r:random; adec(aenc(pk(sk),x,r),sk) = x.
reduc forall x1:pkey, x2:bitstring, x3:random; EPtrapdoor_aenc(aenc(x1,x2,x3),trapdoor_everlasting) = (x1,x2,x3).

(* Signature *)
type sskey.
type pskey.
fun psk(sskey):pskey.
reduc forall x1:sskey; EPtrapdoor_psk(psk(x1),trapdoor_everlasting) = x1.
fun sign(sskey,bitstring):bitstring.
reduc forall ssk:sskey, x:bitstring; verify(sign(ssk,x),psk(ssk),x) = true.
reduc forall x1:sskey, x2:bitstring; EPtrapdoor_sign(sign(x1,x2),trapdoor_everlasting) = (x1,x2).

(* OpenID Provider's signature key *)
fun ssk(name):sskey [private]. (* To authenticate user/server *)

(* Hash function *)
fun hash(bitstring):bitstring. (* Can be ZKP-friendly like Poseidon *)
(* for sanity checks only *)
(* reduc forall x1:bitstring; EPtrapdoor_hash(hash(x1),trapdoor_everlasting) = x1.  *)
fun sha256(bitstring):bitstring. (* Imposed by OpenID: Used in the RS256 crypto mode (RFC 7518) *)
(* for sanity checks only *)
(* reduc forall x1:bitstring; EPtrapdoor_sha256(sha256(x1),trapdoor_everlasting) = x1.  *)

(* Zero-knowledge proofs *)
(* IDel is modeled through is_eligible() *)
(* TODO *)

(* ZK_b for ballots *)
fun ZKP_b(random,vote,bitstring,pkey):bitstring.
reduc forall r:random,v:vote,pkE:pkey;
  let c = aenc(pkE,t_vote(v),r) in
  verifyZKP_b(ZKP_b(r,v,c,pkE), c, pkE) = true.
(* for sanity checks only (ZKP zero-knowledge property is unconditional ): *)
(* reduc forall x1:random,x2:vote,x3:bitstring,x4:pkey; EPtrapdoor_zkpb(ZKP_b(x1,x2,x3,x4),trapdoor_everlasting) = (x1,x2,x3,x4).  *)

(* ZK_dec for correct decryption *)
(* Note: the property "universal verifiability" is trivial with this ZKP. *)
type permutation. fun t_secret_perm(permutation):bitstring [typeConverter]. 
fun mixed(bitstring,permutation):bitstring.
fun zkp_dec(skey,permutation,bitstring,bitstring):bitstring. 
reduc forall x:bitstring,skE:skey,r:random,p:permutation; verifyZKP_dec(zkp_dec(skE,p,x,mixed(aenc(pk(skE),x,r),p)), x,mixed(aenc(pk(skE),x,r),p),pk(skE)) = true. 
(* for sanity checks only: *)
(* reduc forall x1:skey,x2:permutation,x3:bitstring,x4:bitstring; EPtrapdoor_zkpdec(zkp_dec(x1,x2,x3,x4),trapdoor_everlasting) = (x1,x2,x3,x4).  *)

(* Zero-knowledge proofs *)
(* ZK_s for signature *)
fun ZKP_s(nonce,id_t,  sub_t,bitstring,bitstring):bitstring.
reduc forall user:name, el_user:el_name, n_V:bitstring, n_S:nonce, sub:sub_t,b:bitstring;
  let id = Id(is_eligible(el_user)) in
  let N = padID(hash((n_S,n_V)),id) in
  let H = sha256((N,sub,id,AUD_voting)) in
  verifyZKP_s(ZKP_s(n_S,id, sub,n_V,H), sub,n_V,H) = true.
(* IDel is modeled through is_eligible() *)

(* Authorization request *)
type authorize_request.
fun AuthReq(bitstring):authorize_request [data].
(* Authorization code *)
type authorize_code.
fun authorize(bitstring):authorize_code. fun t_authorize_code(authorize_code):bitstring [typeConverter].
reduc forall x:bitstring; openAuthorize(authorize(x)) = x.

(*** EVENTS *)
fun prevent_zkp_duplicates(bitstring):bitstring.
type stamp. fun t_stamp(stamp):bitstring [typeConverter].
event PublishSUBs(stamp,sub_t).
event PublishBallots(stamp,sub_t).
event UniqueSub(bitstring,sub_t).
event PublishBallot(ballot,bitstring).
event Voted(ballot,id_t).
event Verified(ballot,id_t).
event Result(pkey,bitstring,bitstring,bitstring).
event VoterWantsToCast(ballot,id_t,bitstring).
event IdProViderAuthorized(login_t,bitstring).
event MaliciousServer. (* Triggered when the voting server can be compromised *)
event MaliciousVoter(name). (* Triggered for eligible voters who can be compromised and attacker-controlled *)
event PublishZKPs(stamp,bitstring).
event MaliciousIDP.
(* For sanity checks *)
event Target1.event Target2.event Target3.event Target4.event Target5.event Target6.event Target7.event Target8.

event Uniq(bitstring,stamp). (* Useful for imposing uniqueness of some events, not used here. *)
(* restriction x:bitstring, st,st':stamp; event(Uniq(x,st)) && event(Uniq(x,st')) ==> st = st'. *)


(** Theory used to model privacy **)
const Alice,Bob:el_name. 
const ZERO,ONE:vote.
fun c_private(name):channel [private].
const c_mix:channel [private].

(***************** ROLE PROCESSES *****************)
letfun get_b_from_entry_without_oideli(entry:bitstring) = 
    let (x_b:ballot,_:bitstring) = entry in 
    x_b.

letfun get_b_from_entry_with_oideli(entry:bitstring) = 
    let (x_b:ballot,_:bitstring,_:bitstring,_:bitstring,_:bitstring) = entry in 
    x_b.

letfun get_b_from_entry(entry:bitstring,use_oideli:bool) = 
  if use_oideli then 
    get_b_from_entry_with_oideli(entry)
  else
    get_b_from_entry_without_oideli(entry). 
     

(* Process that models a voter using a voting client running OIDEli *)
let Voter_voting(user:name, c_S:channel, pkE:pkey, v:vote, flag:bool, use_oideli:bool) = (* flag is used to model privacy, see below *)
  (* Ballot construction of the Helios-like protocol *)
  new rV:random; 
  let c = aenc(pkE,t_vote(v),rV) in
  let pi_b = ZKP_b(rV,v,c,pkE) in
  let b = mk_ballot((c,pi_b)) in (* ballot *)

  let id = Id(user) in 

    out(c_S, (b,id)); (* The server knows the voter's identity id and ballot b when OIDELi starts *)

    if use_oideli then (
      (* Start OIDEli-zk *)
      new n_V0:nonce;
      let n_V = hash((b,n_V0)) in
      out(c_S, n_V); 

event Target1;
      in(c_S, (AuthReq(x_N:bitstring),x_nS:nonce,=id));
        if padID(hash((x_nS,n_V)),id) = x_N then (
          event VoterWantsToCast(b,id,x_N);
          out(c_idP(login(user),password(user)), (REQ, AuthReq(x_N), AUD_voting)); (* login/password, already in channel!, voter checks that they have authenticated for RP = Voting server (here AUD_voting) *)
          in(c_idP(login(user),password(user)), (=RESP, x_auth_code:authorize_code, =AUD_voting)); (* RESP is to avoid oracle to get what is sent on the c_idP channel *)
          out(c_S, x_auth_code);
event Target4;
            in(c_S, x_JWT:bitstring);
            let (token:bitstring, x_H:bitstring, sigma:bitstring) = x_JWT in
            let (=x_N, x_sub:sub_t, =id, x_aud:aud_t) = token in (* Id(id) is also displayed to and checked by the voter *)
            if x_H = sha256(token) && verify(sigma, psk(ssk(idP)), x_H) then (
event Target6;
              event Voted(b,id);  
              out(c_S, n_V0);
              ( 
                (* trick to model privacy *)
                if flag then 
                  ! out(c_private(user),b)
              ) |
              (  
                (* Individual verifiability check (of the Helios-like protocol) *)
                in(c_BB, (_:stamp, entry:bitstring)); (* the stamp is used in the everlasting model *)
                if get_b_from_entry(entry,use_oideli) = b then  
                  event Verified(b,id);
                0
              )
            )
      )
    ) else (
  event Target6;
      event Voted(b,id); 
      (* trick to model privacy *)
      (
        if flag then 
        ! out(c_private(user),b)
      ) | (
        (* Individual verifiability check (of the Helios-like protocol) *)
        in(c_BB, (_:stamp, entry:bitstring)); (* the stamp is used in the everlasting model *)
        if get_b_from_entry(entry,use_oideli) = b then  
          event Verified(b,id);
          0 
      )
    ) .

(* We assume the attacker can also trigger voters to log-in to other RP in other contexts (for example a malicious web server whose RP's identity is aud is using the same OpenID Provider ) *) 
let Voter_other_RP(user:name, c_S:channel) = 
  in(c_A, (aud:aud_t, (=REQ, ar:authorize_request)));
  if aud <> AUD_voting then (* However, as specified in the paper, the voters must check to never authenticate to the OpenID Provider with AUD_voting displayed on the consent screen, and yet when outside the context of an election *)
  out(c_idP(login(user),password(user)), (REQ, ar, aud));
  in(c_idP(login(user),password(user)), (=RESP, x_auth_code:authorize_code, =aud)); (* AUTH is to avoid oracle to get what is sent on the c_idP channel *)
  out(c_S, x_auth_code); 
event Target8; 
  0.

(* Main voter process *)
let Voter(user:name, c_S:channel, pkE:pkey, v:vote, flag:bool, use_oideli:bool) =
  Voter_voting(user, c_S, pkE, v, flag, use_oideli) | Voter_other_RP(user, c_S).

(* Process that models the OpenId Provider used for the election *)
let Id_provider() =
  out(c_A, psk(ssk(idP))) (* Signature public verification is made public *)
  (* Register all the End Users (voters) *)
  |
  !(
    in(c_A, (x_aud:aud_t,x:name));
    new x_sub[]:sub_t;
    if x <> is_eligible(el_user_cst) then (* this specific user is used to encode id-hiding. Their registration is specific too and cpmpletely defined in the corresponding model *)
    event UniqueSub((x_aud,login(x), password(x)), x_sub); (* We create at most one sub per user, per Relying Party (RP) *)
    insert IdP_UserInfo_DB(x_aud, x, login(x), password(x), Id(x), x_sub);
    0
  ) |
  (* Authenticate end users *)
  !(
    get IdP_UserInfo_DB(x_aud:aud_t, _, x_login:login_t, x_pwd:password_t, x_info:id_t, x_sub:sub_t) in
    in(c_idP(x_login,x_pwd), (=REQ, ar:authorize_request, =x_aud)) [precise];
    (* if x_aud=AUD_voting then *)
    let AuthReq(x_nonce) = ar in (
        let token = (x_nonce,x_sub,x_info,x_aud) in
        let idTokenJWT = (token, sha256(token), sign(ssk(idP), sha256(token))) in
        let auth_token = authorize(idTokenJWT) in
        event IdProViderAuthorized(x_login, token);
event Target3;
        out(c_idP(x_login,x_pwd), (RESP, auth_token, x_aud))
    )
  ).




