(******************************************************************************
 ProVerif models for the paper:
  Election Eligibility with OpenID:
  Turning Authentication into Transferable Proof of Eligibility
******************************************************************************)

(**** OIDELi-zk + Helios-like protocol: vote secrecy. *)

(* -lib default.pvl *)

letfun use_oideli_flag = true.

(***************** RESTRICTIONS *****************)
(* Each user receives at most one sub identifier *)
restriction xL,xR,xR':bitstring, sub1L,sub1R,sub1R',sub2L,sub2R:sub_t; event(UniqueSub(diff[xL,xR],diff[sub1L,sub1R])) && event(UniqueSub(diff[xL,xR'],diff[sub2L,sub2R])) ==> sub1L = sub2L .
restriction xL,xR,xL':bitstring, sub1L,sub1R,sub1R',sub2L,sub2R:sub_t; event(UniqueSub(diff[xL,xR],diff[sub1L,sub1R])) && event(UniqueSub(diff[xL',xR],diff[sub2L,sub2R])) ==> sub1R = sub2R .

(* Ballot weeding: no two published ZK proofs with the same sub *)
restriction n_VL,n_VL',HL,HL':bitstring, n_SL,n_SL':nonce, idL,idL':id_t, subL,subL':sub_t, st1L,st2L:stamp,
            n_VR,n_VR',HR,HR':bitstring, n_SR,n_SR':nonce, idR,idR':id_t, subR,subR':sub_t, st1R,st2R:stamp;
  event(PublishZKPs(diff[st1L,st1R],ZKP_s(diff[n_SL,n_SR],diff[idL,idR], diff[subL,subR],diff[n_VL,n_VR],diff[HL,HR]))) && event(PublishZKPs(diff[st2L,st2R],ZKP_s(diff[n_SL',n_SR'],diff[idL',idR'], diff[subL',subR'],diff[n_VL',n_VR'],diff[HL',HR']))) ==> st1L = st2L || subL <> subL' .

restriction n_VL,n_VL',HL,HL':bitstring, n_SL,n_SL':nonce, idL,idL':id_t, subL,subL':sub_t, st1L,st2L:stamp,
            n_VR,n_VR',HR,HR':bitstring, n_SR,n_SR':nonce, idR,idR':id_t, subR,subR':sub_t, st1R,st2R:stamp;
  event(PublishZKPs(diff[st1L,st1R],ZKP_s(diff[n_SL,n_SR],diff[idL,idR], diff[subL,subR],diff[n_VL,n_VR],diff[HL,HR]))) && event(PublishZKPs(diff[st2L,st2R],ZKP_s(diff[n_SL',n_SR'],diff[idL',idR'], diff[subL',subR'],diff[n_VL',n_VR'],diff[HL',HR']))) ==> st1R = st2R || subR <> subR' .

fun register_each_user_at_most_once(name):bitstring. 
restriction xL,xR,xR':bitstring, stL,stR,stL',stR':stamp; event(Uniq(diff[xL,xR],diff[stL,stR])) && event(Uniq(diff[xL,xR'],diff[stL',stR'])) ==> stL = stL' .
restriction xL,xR,xL':bitstring, stL,stR,stL',stR':stamp; event(Uniq(diff[xL,xR],diff[stL,stR])) && event(Uniq(diff[xL',xR],diff[stL',stR'])) ==> stR = stR' .


(***************** ROLE PROCESSES *****************)
(* Voter process is in default.pvl *)

(* Models all the ckecks of an entry on the BB, returns a boolean *)
letfun check_BB_entry(x_b:ballot,x_pubToken:bitstring,x_nV0:nonce,zkp_s:bitstring,sigma:bitstring) = 
  let (x_sub:sub_t,x_nV:bitstring,H:bitstring) = x_pubToken in 
  if x_nV = hash((x_b,x_nV0)) then 
  if verifyZKP_s(zkp_s, x_sub,x_nV,H) then
  if verify(sigma, psk(ssk(idP)), H) then
  new st[]:stamp; event Uniq(prevent_zkp_duplicates(zkp_s),st);
  new st2[]:stamp; event PublishZKPs(st2,zkp_s);
  true.

(* Process that models the voting server, when running honestly (not compromised)  *)
let Voting_server(c_S:channel,pkE:pkey) =
  in(c_S, (x_b:ballot, x_id:id_t)) [precise];
  let mk_ballot((x_c:bitstring,zkp_b:bitstring)) = x_b in
  if verifyZKP_b(zkp_b, x_c,pkE) then (
    
    (* Start OIDEli-zk *)
    in(c_S, x_nV:bitstring) [precise];
    new n_S[]:nonce;
    let N = padID(hash((n_S,x_nV)), x_id) in
    out(c_S, (AuthReq(N),n_S,x_id));
    
event Target2;
        in(c_S, x_auth_code:authorize_code);
        (* Abstraction of an interaction between to server and the OpenId provider to retrieve the id_token *)
        let idTokenJWT = openAuthorize(x_auth_code) in
        out(c_S, idTokenJWT);
event Target5;
        in(c_S, x_nV0:nonce);
        if x_nV = hash((x_b,x_nV0)) then (
        let (token:bitstring, x_H:bitstring,sigma:bitstring) = idTokenJWT in
        let (=N, x_sub:sub_t, =x_id, =AUD_voting) = token in
        if verify(sigma, psk(ssk(idP)), x_H) && x_H = sha256(token) then (
            let zkp_s = ZKP_s(n_S,x_id,  x_sub,x_nV,x_H) in
            let pubToken = (x_sub,x_nV,x_H) in 
            let entry = (x_b, pubToken, t_nonce(x_nV0), zkp_s, sigma) in        
            if check_BB_entry(x_b,pubToken,x_nV0,zkp_s,sigma) then 
            event PublishBallot(x_b,zkp_s);
event Target7; 
            new st[]:stamp; !out(c_BB, (st,entry)) | 
            out(c_A, (st,entry));
            0
        )
        )
  ).


let Dishonest_Voting_server() =
  in(c_A, entry:bitstring);
  let (x_b:ballot,x_pubToken:bitstring,t_nonce(x_nV0:nonce),zkp_s:bitstring,sigma:bitstring) = entry in
  if check_BB_entry(x_b,x_pubToken,x_nV0,zkp_s,sigma) then
  event PublishBallot(x_b,zkp_s); 
  new st[]:stamp; !out(c_BB, (st,entry)) | 
  out(c_A, (st,entry));
  0.


let Check_Tally(pkE:pkey) = 
  ! ( 
  in(c_A,(x:bitstring,c:bitstring,pi_dec:bitstring));
  if verifyZKP_dec(pi_dec, x,c,pkE) then 
  event Result(pkE,x,c,pi_dec)
  ).

event Target9.

let Tally(skE:skey) = 
  new p:permutation; 
  ( (* Mixing and decrypting ballots: first dealing with the special case of Alice's and Bob's ballots, which need to be swapped *)
    (* We only tally BB having ballots for Alice and Bob, for which votes are swapped/diff *)
    in(c_private(is_eligible(Alice)),b_Alice:ballot);
    in(c_private(is_eligible(Bob)),b_Bob:ballot);

    in(c_BB,(stAlice:stamp,xAlice:bitstring)) [precise]; 
    in(c_BB,(stBob:stamp,xBob:bitstring)) [precise];  
    new stUAlice[]:stamp; event Uniq(t_stamp(stAlice),stUAlice); (* We tally Alice's and Bob's ballot at most once *)
    new stUBob[]:stamp; event Uniq(t_stamp(stBob),stUBob);

    if get_b_from_entry(xAlice,use_oideli_flag) = b_Alice && get_b_from_entry(xBob,use_oideli_flag) = b_Bob then (
      let mk_ballot((cAlice:bitstring,pi_bAlice:bitstring)) = get_b_from_entry(xAlice,use_oideli_flag) in 
      let mk_ballot((cBob:bitstring,pi_bBob:bitstring)) = get_b_from_entry(xBob,use_oideli_flag) in 
      out(c_mix,diff[cAlice,cBob]) | (* As is standard with the swapping technique, Alice's and Bob's ballots are swapped again before revealing the election result *)
      out(c_mix,diff[cBob,cAlice]) | 
      (
        in(c_mix,x_mix_1:bitstring);
        in(c_mix,x_mix_2:bitstring);
        let c_mixed1 = mixed(x_mix_1,p) in 
        let c_mixed2 = mixed(x_mix_2,p) in 
        let r1 = adec(x_mix_1,skE) in
        let r2 = adec(x_mix_2,skE) in
        let pi_dec1 = zkp_dec(skE,p,r1,c_mixed1) in 
        let pi_dec2 = zkp_dec(skE,p,r2,c_mixed2) in 
        out(c_A,(r1,c_mixed1,pi_dec1));
        out(c_A,(r2,c_mixed2,pi_dec2));
        event Target8
      )
    )
  ) | 
  ! ( (* Mixing and decrypting ballots: the other case, no need to swap *)
  in(c_BB,(st:stamp,x:bitstring));
  new stU[]:stamp; event Uniq(t_stamp(st),stU);

  in(c_private(is_eligible(Alice)),b_Alice:ballot); (* checking we're not mixing and decrypting Alice's or Bob's ballots, note this is not blocking as outputs on c_private(.) are always replicated (!) *)
  in(c_private(is_eligible(Bob)),b_Bob:ballot);

  (* Note: duplicates are removed before publication of the BB *)
  if get_b_from_entry(x,use_oideli_flag) = b_Alice || get_b_from_entry(x,use_oideli_flag) = b_Bob then (
    0
  ) else (
    let mk_ballot((c:bitstring,pi_b:bitstring)) = get_b_from_entry(x,use_oideli_flag) in 
    if verifyZKP_b(pi_b, c,pk(skE)) then 
    let c_mixed = mixed(c,p) in 
    let r = adec(c,skE) in
    let pi_dec = zkp_dec(skE,p,r,c_mixed) in 
    out(c_A,(r,c_mixed,pi_dec));
    event Target9
  )
).

(***************** PROPERTIES *****************)
(*** Sanity checks *)
  (* lemma 
    event(Target1) ;
    event(Target2) ;
(*    event(Target3) ; (* IDP is compromised so we do not expect this event to be reached *) *)
    event(Target4) ;
    event(Target5) ;
    event(Target6) ;
    event(Target7) ;
    event(Target8) ;
    event(Target9)
    . *)


not id:name; attacker(c_private(id)).

process 
    (*** Creations of the public key of the electon *)
    new skE[]:skey; 
    let pkE = pk(skE) in 
    out(c_A,pkE) |

    (*** Creations of the unique ID Provider *)
    (* the ID Provider is dishonest for vote secrecy and verifiability *)
    (* !Id_provider() *)
    (
        out(c_A,ssk(idP)); 
        !(
            in(c_A,user:name); 
            out(c_A, (login(user),password(user))) (* the attacker can the communicate over c_idP(.,.) *)
        )
    )
    |
    (*** Creation of the unique voting Server (either honest or malicious) *)
    ( in(c_A, is_honest:bool) ;
        if is_honest then
        !Voting_server(c_S,pkE)
        else (
        event MaliciousServer;
        !Dishonest_Voting_server()
        )
    )
    (*** Creations of the voter identities *)
    | 
    !
    (
        new el_user:el_name;
        let user:name = is_eligible(el_user) in
        out(c_A, (el_user,user))
    )
    (* Attacker controls some malicious eligible voters too (can log in to OpenID provider with the password) *)
    | 
    !
    (
    in(c_A, el_user:el_name);
    let user:name = is_eligible(el_user) in
    event MaliciousVoter(user);
    out(c_A, (el_user,user, password(user)))
    )
    (* Attacker chooses which voters execute the protocol and chooses the vote (v) *)
    | 
    !
    (
    in(c_A, user:name);
    in(c_A, v:vote);
    Voter(user, c_S, pkE, v, false, use_oideli_flag)
    )
    (* Attacker chooses which voters authenticate to another RP using OpenID Connect *)
    | 
    !
    (
    in(c_A, user:name);
    Voter_other_RP(user, c_S)
    ) | 
    ! Check_Tally(pkE) | 

    (* process used to model vote secrecy *)
    ( 
    (* Define Alice and Bob and register them into the IdP *)
    let Alice_isEl = is_eligible(Alice) in
    let Bob_isEl = is_eligible(Bob) in
    out(c_A,is_eligible(Alice));
    out(c_A,is_eligible(Bob));
    new x_sub_Alice[]:sub_t; 
    new x_sub_Bob[]:sub_t; 

    event UniqueSub((AUD_voting,login(Alice_isEl),password(Alice_isEl)),diff[x_sub_Alice,x_sub_Bob]); 
    event UniqueSub((AUD_voting,login(Bob_isEl),password(Bob_isEl)),diff[x_sub_Bob,x_sub_Alice]); 
    insert IdP_UserInfo_DB(AUD_voting,Alice_isEl,login(Alice_isEl),password(Alice_isEl),Id(Alice_isEl), diff[x_sub_Alice,x_sub_Bob]);
    insert IdP_UserInfo_DB(AUD_voting,Bob_isEl,login(Bob_isEl),password(Bob_isEl),Id(Bob_isEl), diff[x_sub_Bob,x_sub_Alice]);

    (* the two target sessions of Alice and Bob used to encode privacy *)
    Voter(Alice_isEl, c_S,pkE, diff[ZERO,ONE],true,use_oideli_flag) |
    Voter(Bob_isEl, c_S,pkE, diff[ONE,ZERO],true,use_oideli_flag) | 

    Tally(skE)
    )