## Notes

### tests: what is covered?

- generation
    - modify start or end (toggle first/last bit/byte) of any constraint
        - [code](./src/zkp/generate.rs)
- verification
    - modify each component of the circuit sizing by +/- 1
        - [code](./src/zkp/verify.rs)
    - modify each public input (toggle first bit/byte)
        - [code](./src/zkp/public_inputs.rs)
        - in particular: if the id in the merkle tree was modified, the merkle root (cap in our case) will change and it is enough to test a modified root

### test from cli
```
RUST_LOG=debug cargo +nightly build --release
RUST_BACKTRACE=1 RUST_LOG=debug ./target/release/oideli_zkp compute-circuit-sizing
time RUST_BACKTRACE=1 RUST_LOG=debug ./target/release/oideli_zkp prove
RUST_BACKTRACE=1 RUST_LOG=debug ./target/release/oideli_zkp verify --skip-jwks-request
```

### example run
```
$ RUST_LOG=debug cargo +nightly run --release -- compute-circuit-sizing

data written to tmp/circuit_sizing.json

$ RUST_LOG=debug cargo +nightly run --release -- prove

jwt body: {"iss":"accounts.google.com","azp":"71307269411-0lrk3veg9jnqrbm19jkj1gd6ioommsqf.apps.googleusercontent.com","aud":"71307269411-0lrk3veg9jnqrbm19jkj1gd6ioommsqf.apps.googleusercontent.com","sub":"103333552062856499099","email":"0.plonky.oauth.000000000000@gmail.com","email_verified":true,"at_hash":"fIk6OEau4t3xlR7m-t7tlg","nonce":"OSZpoN8r7otPXYw7lEMLURbzMVBRmYVr4Q8qvUuMiuMA","iat":1697486530,"exp":1697490130}
number of gates after adding sha256: 64047
[2023-10-17T12:18:52Z DEBUG plonky2::util::context_tree] 67747 gates to root
[2023-10-17T12:18:52Z DEBUG plonky2::plonk::circuit_builder] Total gate counts:
[2023-10-17T12:18:52Z DEBUG plonky2::plonk::circuit_builder] - 6475 instances of BaseSumGate { num_limbs: 63 } + Base: 2
[2023-10-17T12:18:52Z DEBUG plonky2::plonk::circuit_builder] - 2200 instances of U32ArithmeticGate { num_ops: 3, _phantom: PhantomData<plonky2_field::goldilocks_field::GoldilocksField> }
[2023-10-17T12:18:52Z DEBUG plonky2::plonk::circuit_builder] - 23 instances of PoseidonGate(PhantomData<plonky2_field::goldilocks_field::GoldilocksField>)<WIDTH=12>
[2023-10-17T12:18:52Z DEBUG plonky2::plonk::circuit_builder] - 52353 instances of ArithmeticGate { num_ops: 20 }
[2023-10-17T12:18:52Z DEBUG plonky2::plonk::circuit_builder] - 6696 instances of BaseSumGate { num_limbs: 32 } + Base: 2
number of gates: 67747
[2023-10-17T12:18:52Z INFO  plonky2::plonk::circuit_builder] Degree before blinding & padding: 67850
[2023-10-17T12:18:52Z INFO  plonky2::plonk::circuit_builder] Adding 4342 blinding terms for witness polynomials, and 4344*2 for Z polynomials
[2023-10-17T12:18:52Z INFO  plonky2::plonk::circuit_builder] Degree after blinding & padding: 131072
[2023-10-17T12:19:14Z DEBUG plonky2::plonk::circuit_builder] Building circuit took 22.231861s
proof successful
data written to tmp/proof.json

$ RUST_LOG=debug cargo +nightly run --release -- verify

number of gates after adding sha256: 64047
[2023-10-17T12:20:58Z DEBUG plonky2::util::context_tree] 67747 gates to root
[2023-10-17T12:20:58Z DEBUG plonky2::plonk::circuit_builder] Total gate counts:
[2023-10-17T12:20:58Z DEBUG plonky2::plonk::circuit_builder] - 6696 instances of BaseSumGate { num_limbs: 32 } + Base: 2
[2023-10-17T12:20:58Z DEBUG plonky2::plonk::circuit_builder] - 6475 instances of BaseSumGate { num_limbs: 63 } + Base: 2
[2023-10-17T12:20:58Z DEBUG plonky2::plonk::circuit_builder] - 52353 instances of ArithmeticGate { num_ops: 20 }
[2023-10-17T12:20:58Z DEBUG plonky2::plonk::circuit_builder] - 2200 instances of U32ArithmeticGate { num_ops: 3, _phantom: PhantomData<plonky2_field::goldilocks_field::GoldilocksField> }
[2023-10-17T12:20:58Z DEBUG plonky2::plonk::circuit_builder] - 23 instances of PoseidonGate(PhantomData<plonky2_field::goldilocks_field::GoldilocksField>)<WIDTH=12>
number of gates: 67747
[2023-10-17T12:20:58Z INFO  plonky2::plonk::circuit_builder] Degree before blinding & padding: 67850
[2023-10-17T12:20:58Z INFO  plonky2::plonk::circuit_builder] Adding 4342 blinding terms for witness polynomials, and 4344*2 for Z polynomials
[2023-10-17T12:20:58Z INFO  plonky2::plonk::circuit_builder] Degree after blinding & padding: 131072
[2023-10-17T12:21:19Z DEBUG plonky2::plonk::circuit_builder] Building circuit took 20.808865s
verification successful
```

### number of gates in circuit
should be in the logs when running with debug log level: `RUST_LOG=debug cargo +nightly run --release`

- proper base64 circuit after poseidon: 64398
- with double b64 for nonce: 64811
- with base64 on whole jwt: 67804
- with lookup on bits: 77138
    - sha: 83%
    - lookup 12%


```
number of gates after adding sha256: 64047
number of gates after poseidon: 64055
number of gates after merkle tree: 64080
number of gates after base64 on nonce: 64357
number of gates after lookup: 73674
number of gates after base64 on jwt body: 77138

>>> 64047 / 77138
0.8302911664808525
>>> (73674 - 64357) / 77138
0.12078353081490316

[gates-count] sha256: 64047
[gates-count] poseidon: 8
[gates-count] merkle: 18
[gates-count] nonceb64: 277
[gates-count] lookup: 9246
[gates-count] bodyb64: 3464
77131 gates to root

64047+8+18+277+9246+3464=77060
diff: 71 
--> conversion pre lookup

[gates-count] sha256: 64047
[gates-count] poseidon: 8
[gates-count] merkle: 29
[gates-count] nonceb64: 277
[gates-count] convprelookup: 457
[gates-count] lookup: 1205
[gates-count] bodyb64: 3465

64047+8+29+277+457+1205+3465
69488

with aud:
[gates-count] sha256: 64047
[gates-count] poseidon: 8
[gates-count] merkle: 29
[gates-count] nonceb64: 277
[gates-count] convprelookup: 457
[gates-count] lookup: 1929
[gates-count] bodyb64: 3465

64047+8+29+277+457+1929+3465
70212
```

- types of gate:
    - run `cargo +nightly test _ops_count -- --nocapture`
    - BaseSumGate: splits a wire into limbs (bits I guess?)
        - num ops: 1
        - num wires: 33 or 64
        - num constraints: 33 or 63
        - num constants: 0
    - ArithmeticGate: "can perform a weighted multiply-add", number of ops depends on config
        - num ops: 20
        - num wires: 80
        - num constraints: 20
        - num constants: 2
    - U32ArithmeticGate: simplified ArithmeticGate
        - num ops: 3
        - num wires: 114
        - num constraints: 108
        - num constants: 0
    - PoseidonGate
        - ops: 1 ?? this is surprising
        - num wires: 135
        - num constraints: 123
        - num constants: 0

### proof size
`println!("verifier data size: {} bytes", circuit.compress(proof.clone()).unwrap().to_bytes().len());`

using Rust MessagePack:
```
ll tmp/proof.json 
-rw-r--r--. 1 194K Jan 25 16:44 tmp/proof.json
```

### bit order
rule: all the bit strings are always big-endian

but the function from plonky2 to split a target into boolean target is little-endian, thus conversions have to be made a bit everywhere

### nonce size and base 64
output of poseidon: 256 bits

to avoid padding during base64 conversion, manually add a zero byte: 
- 256 + 8 = 264
- 264 / 3 * 4 = 352 = 44 bytes

### size of token

full base64: 1014 B
body JSON: 426 B
```
{"iss":"accounts.google.com","azp":"71307269411-0lrk3veg9jnqrbm19jkj1gd6ioommsqf.apps.googleusercontent.com","aud":"71307269411-0lrk3veg9jnqrbm19jkj1gd6ioommsqf.apps.googleusercontent.com","sub":"108070721566211787386","email":"oideli.zkp@gmail.com","email_verified":true,"at_hash":"Vxjzrw7zNBCt7SAIlX4-CA","nonce":"OSZpoN8r7otPXYw7lEMLURbzMVBRmYVr4Q8qvUuMiuMA..............................","iat":1701450013,"exp":1701453613}
```

### pesto calc specs

```
$ grep "^[c]pu MHz" /proc/cpuinfo
cpu MHz         : 3493.313

$ lscpu
  Nom de modèle :                         AMD EPYC 7F52 16-Core Processor
```

### test coverage
https://doc.rust-lang.org/rustc/instrument-coverage.html

```
RUSTFLAGS="-C instrument-coverage" cargo +nightly test --tests
llvm-profdata merge -sparse default_*.profraw -o json5format.profdata
llvm-cov report \
    $( \
      for file in \
        $( \
          RUSTFLAGS="-C instrument-coverage" \
            cargo +nightly test --tests --no-run --message-format=json \
              | jq -r "select(.profile.test == true) | .filenames[]" \
              | grep -v dSYM - \
        ); \
      do \
        printf "%s %s " -object $file; \
      done \
    ) \
  --instr-profile=json5format.profdata --summary-only --ignore-filename-regex='/.cargo/registry' --ignore-filename-regex='/.cargo/git'
```

running all tests with the flag is too long...

### lines of code

```
➜ tokei .
===============================================================================
 Language            Files        Lines         Code     Comments       Blanks
===============================================================================
 JSON                    3           28           28            0            0
 Markdown                3          470            0          395           75
 Python                  4          160           89           31           40
 TeX                     1          141          100           10           31
 Plain Text              3         1026            0         1026            0
 TOML                    1           52           44            1            7
-------------------------------------------------------------------------------
 Rust                   27         3981         3338          198          445
 |- Markdown            14          137            0          134            3
 (Total)                           4118         3338          332          448
===============================================================================
 Total                  42         5858         3599         1661          598
===============================================================================
```