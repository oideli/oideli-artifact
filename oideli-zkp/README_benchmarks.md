# Benchmarks

Some simple benchmarks are available in the [benches](./benches/) folder (runnable with the default `cargo bench`), but these greadily run on all available core.
To measure the scalability of the zkp generation, we lock a proof on a single thread and start independent proofs in parallel.
This is done in [this file](./examples/parallel_proofs.rs), located in the `examples` folder for ease of execution.

```
# for example: generate proofs for 5 hours on 16 cores
PARALLELISM=16 RUNNING_TIME_MINUTES=300 cargo run --release --example parallel_proofs
```

## Resources

The generation of proofs is very memory intensive.
The profiling below shows that each core generating a proof has a peak consumption of about 5GB of RAM.
This means that the system running the benchmarks should have at least `PARALLELISM` * 5GB of memory available.

## Results

Generating as many proofs as possible for 5 hours:

| Number of cores | Proofs per hour | Mean proof time (ms) | Std (ms) |
|-----------------|-----------------|----------------------|----------|
| 1               | 43.8            | 82064                | 325      |
| 16              | 679.2           | 84639                | 1021     |
| 32              | 1347            | 85532                | 885      |
| 64              | 1576            | 146286               | 3593     |

<img src="./examples/results/proof_scaling.png" width="500"/>

Verifications on a laptop (i7-8665U CPU @ 1.90GHz) for 30min:

| Number of cores | Verifications per hour | Mean verif time (ms) | Std (ms) |
|-----------------|------------------------|----------------------|----------|
| 4               | 1095908                | 13.05                | 1.17     |



## Various useful commands

### Memory profiling
```
valgrind --tool=massif --stacks=yes ./target/release/oideli_zkp prove
```

### Monitoring available memory
```
while sleep 1; do cat /proc/meminfo | grep MemAvailable | awk '{print strftime("%s,"), $2}' >> mem.log ; done
```

### Limiting memory of process

5 GB:
```
ulimit -S -v 5000000; ./target/release/oideli_zkp prove
```
```
ulimit -Sa
```

### Prepare logs for analysis

The logs are a bit clunky and not so well formated, we need some bash magic

```
cat bench_16_workers.log | grep "\[bench\]\[prooftime\]" | cut -c 19- > bench_16_workers.csv

# for analyze_proof_time_worker.py
cat bench_32_workers.log | grep "\[bench\]\[worker" | grep done | sed -E 's/.*(worker:)([0-9]+).*proof ([0-9]*) .* (.*)s/\2,\3,\4/' > bench_32_workers_filtered.csv
```

## Notes
- plan:
  - generate proofs
    - for each amount of workers
      - 16, 32, 64
    - for 3 hours
    - collect
      - time per proof (id: worker/count)
  - in parallel, record memory usage
    - this will let us set ulimit properly
- first results:
  - 16 workers: 
    - 14.85 prfs/min
    - memory: ~88GB -> 5.5 GB/core
  - 32 workers: 
    - 15.15 prfs/min
    - memory: ~161GB -> 5.03 GB/core
  - 64 workers: 
    - 15.15 prfs/min
    - memory peak: ~287GB -> 4.48 GB/core
- 2nd round: limit each worker to one core
  - mem: 5GB should be okay (4 is too little)
  - proof time
    - gets worse with more in parallel
      - probably some limitation from memory I/O
      - but quite stable up to 32 workers
  - total number of proofs
    - linearly increase up to 32, then flatten
      - slope just bellow 1, due to the increase in proof time
      - flatten due to physical vs virtual core?
        - maybe just memory I/O because I didn't only select cores with even id -> half of physical cores
    - check ratio 1-32
      - 1: 0.48 prfs/min
      - 32: 13.78 prfs/min
      - 13.78 / .48 = 28.71
  - time per worker
    - it looks like some cores are faster (why?)
    - the first proof of each worker is slower
- 3rd round, 5h
  - 1 workers
    - proofs/hour:  29.6
    - count       148.000000
    - mean     121162.054054
    - std         598.279049
  - 32 workers
    - proofs/hour:  851.2
    - count      4256.000000
    - mean     134803.159070
    - std        6410.297446
- without circuit rebuild
  - prove
    - 1 workers
      - proofs/hour:  44.4
      - count      222.000000
      - mean     81032.450450
      - std        265.775421
    - 32 workers
      - proofs/hour:  1355.2
      - count      6776.000000
      - mean      84813.690821
      - std        2624.024595
    - 64 workers
      - proofs/hour:  1450.6
      - count      7253.000000
      - mean     156827.605405
      - std       46251.105110
  - verify
    - 1 workers
      - verifications/hour:  94042
      - count    47021.000000
      - mean         8.684729
      - std          0.065255
    - 32 workers
      - verifications/hour:  2111518
      - count    1.055759e+06
      - mean     9.143217e+00
      - std      1.707057e-01
- with merkle depth 21
  - uses much more memory
    - around 7GB per core it seems
    - probably du to merkle leaves copied around...
- with the aud field
  - 1 workers                                    
    - proofs/hour:  43.8
    - count      219.000000
    - mean     82064.091324
    - std        325.755453                                                          
  - 16 workers                                   
    - proofs/hour:  679.2                          
    - count      3396.000000                         
    - mean      84639.732921                         
    - std        1021.223902                                                       
  - 32 workers                                   
    - proofs/hour:  1347.2                         
    - count      6736.000000                         
    - mean      85532.245992                         
    - std         885.596073                                                           
  - 64 workers                                   
    - proofs/hour:  1576.4                         
    - count      7882.000000                         
    - mean     146286.020299                         
    - std        3593.069659

```
ulimit -S -v unlimited
PARALLELISM=1 RUNNING_TIME_MINUTES=300 cargo +nightly run --release --example parallel_proofs &>> bench_1_workers.log
ulimit -S -v 80000000
PARALLELISM=16 RUNNING_TIME_MINUTES=300 cargo +nightly run --release --example parallel_proofs &>> bench_16_workers.log
ulimit -S -v 160000000
PARALLELISM=32 RUNNING_TIME_MINUTES=300 cargo +nightly run --release --example parallel_proofs &>> bench_32_workers.log
ulimit -S -v 320000000
PARALLELISM=64 RUNNING_TIME_MINUTES=300 cargo +nightly run --release --example parallel_proofs &>> bench_64_workers.log
ulimit -S -v unlimited

cat bench_1_workers.log | grep "\[bench\]\[prooftime\]" | cut -c 19- > bench_1_workers.csv
cat bench_16_workers.log | grep "\[bench\]\[prooftime\]" | cut -c 19- > bench_16_workers.csv
cat bench_32_workers.log | grep "\[bench\]\[prooftime\]" | cut -c 19- > bench_32_workers.csv
cat bench_64_workers.log | grep "\[bench\]\[prooftime\]" | cut -c 19- > bench_64_workers.csv

cat bench_32_workers.log | grep "\[bench\]\[worker" | grep done | sed -E 's/.*(worker:)([0-9]+).*proof ([0-9]*) .* (.*)s/\2,\3,\4/' > bench_32_workers_filtered.csv
```

### Verify

```
VERIFY=true PARALLELISM=1 RUNNING_TIME_MINUTES=30 cargo +nightly run --release --example parallel_proofs &>> verify_1_workers.log
VERIFY=true PARALLELISM=32 RUNNING_TIME_MINUTES=30 cargo +nightly run --release --example parallel_proofs &>> verify_32_workers.log

cat verify_1_workers.log | grep "\[bench\]\[worker" | grep done | sed -E 's/.*(worker:)([0-9]+).*verification ([0-9]*) .* (.*)ms/\2,\3,\4/' > verify_1_workers.csv
cat verify_32_workers.log | grep "\[bench\]\[worker" | grep done | sed -E 's/.*(worker:)([0-9]+).*verification ([0-9]*) .* (.*)ms/\2,\3,\4/' > verify_32_workers.csv

VERIFY=true PARALLELISM=4 RUNNING_TIME_MINUTES=30 cargo +nightly run --release --example parallel_proofs &>> verify_4_workers.log

cat verify_4_workers.log | grep "\[bench\]\[worker" | grep done | sed -E 's/.*(worker:)([0-9]+).*verification ([0-9]*) .* (.*)ms/\2,\3,\4/' > verify_4_workers.csv

- 4 workers
  verifications/hour:  1095908
  count    547954.000000
  mean         13.052198
  std           1.170992
```

### Number of shas
```
PARALLELISM=1 NUMBER_SHAS=1 RUNNING_TIME_MINUTES=60 cargo +nightly run --release --example multiple_sha256 &>> shas_1_shas_1_workers.log
PARALLELISM=1 NUMBER_SHAS=2 RUNNING_TIME_MINUTES=60 cargo +nightly run --release --example multiple_sha256 &>> shas_2_shas_1_workers.log
PARALLELISM=1 NUMBER_SHAS=4 RUNNING_TIME_MINUTES=60 cargo +nightly run --release --example multiple_sha256 &>> shas_4_shas_1_workers.log
PARALLELISM=1 NUMBER_SHAS=39 RUNNING_TIME_MINUTES=60 cargo +nightly run --release --example multiple_sha256 &>> shas_39_shas_1_workers.log
PARALLELISM=32 NUMBER_SHAS=1 RUNNING_TIME_MINUTES=60 cargo +nightly run --release --example multiple_sha256 &>> shas_1_shas_32_workers.log
PARALLELISM=32 NUMBER_SHAS=2 RUNNING_TIME_MINUTES=60 cargo +nightly run --release --example multiple_sha256 &>> shas_2_shas_32_workers.log
PARALLELISM=32 NUMBER_SHAS=4 RUNNING_TIME_MINUTES=60 cargo +nightly run --release --example multiple_sha256 &>> shas_4_shas_32_workers.log
PARALLELISM=32 NUMBER_SHAS=39 RUNNING_TIME_MINUTES=60 cargo +nightly run --release --example multiple_sha256 &>> shas_39_shas_32_workers.log

cat shas_1_shas_1_workers.log | grep "\[bench\]\[prooftime\]" | cut -c 19- > shas_1_shas_1_workers.csv
cat shas_2_shas_1_workers.log | grep "\[bench\]\[prooftime\]" | cut -c 19- > shas_2_shas_1_workers.csv
cat shas_4_shas_1_workers.log | grep "\[bench\]\[prooftime\]" | cut -c 19- > shas_4_shas_1_workers.csv
cat shas_39_shas_1_workers.log | grep "\[bench\]\[prooftime\]" | cut -c 19- > shas_39_shas_1_workers.csv
cat shas_1_shas_32_workers.log | grep "\[bench\]\[prooftime\]" | cut -c 19- > shas_1_shas_32_workers.csv
cat shas_2_shas_32_workers.log | grep "\[bench\]\[prooftime\]" | cut -c 19- > shas_2_shas_32_workers.csv
cat shas_4_shas_32_workers.log | grep "\[bench\]\[prooftime\]" | cut -c 19- > shas_4_shas_32_workers.csv
cat shas_39_shas_32_workers.log | grep "\[bench\]\[prooftime\]" | cut -c 19- > shas_39_shas_32_workers.csv

- 1 workers, 1 shas   
count       45.000000 
mean     78727.111111 
std        528.641966 
- 1 workers, 2 shas   
count        21.000000
mean     165521.714286
std        1619.831230
- 1 workers, 4 shas   
count        11.000000
mean     324225.909091
std        1503.333127
- 1 workers, 39 shas               
  proofs/hour:  0.2    
count          1.0                           
mean     2874049.0                            
std            NaN
- 32 workers, 1 shas  
count      1316.000000
mean      86252.522796
std        6374.018544
- 32 workers, 2 shas  
count       594.000000
mean     188799.122896
std       21949.577345
- 32 workers, 4 shas  
count       279.000000
mean     397890.996416
std       62969.130930 
- 32 workers, 39 shas                    
  proofs/hour:  0.0
```