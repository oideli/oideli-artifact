import matplotlib.pyplot as plt
import seaborn as sns

import pandas as pd

dfs = []

num_workers = [32]
# num_workers = [64]
for w in num_workers:
    # filename = "./results/02-single-cpu-workers/bench_{}_workers_filtered.csv".format(w)
    # filename = "./results/03-with-ulimit-5h/bench_{}_workers_filtered.csv".format(w)
    # filename = "./results/04-no-build-circuit/bench_{}_workers_filtered.csv".format(w)
    # filename = "./results/05-actually-no-build-circuit/bench_{}_workers_filtered.csv".format(w)
    filename = "./results/07-avoid-copying-merkle/bench_{}_workers_filtered.csv".format(w)

    events = []
    with open(filename) as file:
        for line in file:
            # format: 9,129.51
            worker, proof_n, proof_time = line.strip().split(",")
            event = {"worker": int(worker), "proof_n": int(proof_n), "proof_time": float(proof_time)}
            events.append(event)

    df = pd.DataFrame(events)
    print(df)
    dfs.append(df)


    print(df.proof_time.describe())

    print("mean by worker")
    print(df.groupby("worker").mean())
    print("std by worker")
    print(df.groupby("worker").std())
    print("mean by proof number")
    print(df.groupby("proof_n").mean())

    # plt.bar(range(0, num_workers[0]), df.groupby("worker").mean().proof_time)
    sns.scatterplot(data=df, x='worker', y='proof_time', hue='proof_n')

plt.show()
