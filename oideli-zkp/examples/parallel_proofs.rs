use std::{
    env,
    sync::{Arc, Mutex},
    thread,
    time::{Duration, Instant, SystemTime, UNIX_EPOCH},
};

use libafl_bolts::core_affinity;
use oideli_zkp::{
    build_circuit,
    merkle::Merkle,
    mock::{
        jwt::{mock_jwt, mock_nonces},
        merkle::mock_merkle,
    },
    prove::{compute_circuit_sizing, generate_proof},
};
use rand::{thread_rng, Rng};

/// Runs `PARALLELISM` threads in parallel, generating as many proofs as possible (or verification if `VERIFY`)
fn main() {
    let parallelism: usize = env::var("PARALLELISM")
        .map(|v| v.parse::<usize>().unwrap())
        .unwrap_or(2);
    let merkle_depth: usize = env::var("MERKLE_DEPTH")
        .map(|v| v.parse::<usize>().unwrap())
        .unwrap_or(21);
    let running_time_minutes: u64 = env::var("RUNNING_TIME_MINUTES")
        .map(|v| v.parse::<u64>().unwrap())
        .unwrap_or(5);
    let verify_only: bool = env::var("VERIFY")
        .map(|v| v.parse::<bool>().unwrap())
        .unwrap_or(false);

    println!("[bench] merkle depth: {}", merkle_depth);
    let merkle = mock_merkle(merkle_depth, None);

    let (circuit, targets) = {
        let (ns, nv, _, _) = mock_nonces(43);
        let mock_jwt = mock_jwt(&ns, &nv, &merkle.voter_id);
        let circuit_sizing = compute_circuit_sizing(&mock_jwt.jwt, &merkle).unwrap();
        build_circuit(&circuit_sizing).unwrap()
    };
    let circuit = Arc::new(circuit);

    let core_ids = core_affinity::get_core_ids().unwrap();
    println!("[bench] number of cores detected: {}", core_ids.len());
    assert!(
        core_ids.len() >= parallelism,
        "cannot have more parallelism than cores"
    );

    let mut rng = thread_rng();
    let r: u64 = rng.gen();
    let (ns, nv, _, _) = mock_nonces(r);
    let voter_index = rng.gen_range(0..merkle.voter_ids.len());
    let voter_id = merkle.voter_ids[voter_index].clone();
    let merkle = Merkle {
        voter_id: voter_id.clone(),
        voter_index,
        ..merkle.clone()
    };
    println!("voter: {}", voter_id);
    let mock_jwt = mock_jwt(&ns, &nv, &voter_id);
    let proof_0 = if verify_only {
        Some(
            generate_proof(
                ns.clone(),
                nv.clone(),
                &mock_jwt.jwt,
                &merkle,
                None,
                Some((circuit.as_ref(), targets.clone())),
            )
            .unwrap(),
        )
    } else {
        None
    };

    let counter = Arc::new(Mutex::new(0));
    for worker in 0..parallelism {
        // clone values for worker
        let ns = ns.clone();
        let nv = nv.clone();
        let mock_jwt = mock_jwt.clone();
        let merkle = merkle.clone();
        let circuit = circuit.clone();
        let targets = targets.clone();
        let proof_0 = proof_0.clone();

        let core_id = core_ids[worker];

        let counter = Arc::clone(&counter);
        thread::spawn(move || {
            core_id.set_affinity().unwrap();
            // this thread pool forces the underlying plonky2 parallelism to use only one thread
            rayon::ThreadPoolBuilder::new()
                .num_threads(1)
                .build()
                .unwrap()
                .install(|| {
                    let mut i = 0;
                    loop {
                        // clone values for loop iter
                        let ns = ns.clone();
                        let nv = nv.clone();
                        let targets = targets.clone();
                        let proof_0 = proof_0.clone();

                        if !verify_only {
                            println!("[bench][worker:{}] start proof {}", worker, i);
                            let now = Instant::now();
                            generate_proof(
                                ns,
                                nv,
                                &mock_jwt.jwt,
                                &merkle,
                                None,
                                Some((circuit.as_ref(), targets)),
                            )
                            .unwrap();
                            let elapsed = now.elapsed();
                            println!(
                                "[bench][worker:{}] done proof {} in {:.2?}",
                                worker, i, elapsed
                            );
                            println!(
                                "[bench][prooftime]{},{}",
                                SystemTime::now()
                                    .duration_since(UNIX_EPOCH)
                                    .unwrap()
                                    .as_secs(),
                                elapsed.as_millis()
                            );
                        } else {
                            let now = Instant::now();
                            circuit.verify(proof_0.unwrap().zkp).unwrap();
                            let elapsed = now.elapsed();
                            println!(
                                "[bench][worker:{}] done verification {} in {:.2?}",
                                worker, i, elapsed
                            );
                            println!(
                                "[bench][veriftime]{},{}",
                                SystemTime::now()
                                    .duration_since(UNIX_EPOCH)
                                    .unwrap()
                                    .as_secs(),
                                elapsed.as_millis()
                            );
                        }
                        let mut num = counter.lock().unwrap();
                        *num += 1;
                        i += 1;
                    }
                });
        });
    }

    thread::sleep(Duration::from_secs(running_time_minutes * 60));
    println!(
        "[bench] stopping process after {} minutes",
        running_time_minutes
    );
    println!(
        "[bench] number or finished proofs: {}",
        *counter.lock().unwrap()
    );
}
