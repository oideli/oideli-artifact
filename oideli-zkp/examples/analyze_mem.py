import matplotlib.pyplot as plt
from datetime import datetime

import pandas as pd


# filename = "./results/01-first-round/mem.csv"
# filename = "./results/02-single-cpu-workers/mem.csv"
# filename = "./results/05-actually-no-build-circuit/mem.csv"
filename = "./results/06-merkle-depth-21/mem.csv"
filename = "./results/07-avoid-copying-merkle/mem.csv"

events = []
with open(filename) as file:
    for line in file:
        # format: 1702394279, 520404960
        timestamp, mem_available = line.strip().split(", ")
        time = datetime.fromtimestamp(int(timestamp))
        event = {"time": time, "mem_available": int(mem_available)}
        events.append(event)

df = pd.DataFrame(events)

print(df)

plt.plot(df.time, df.mem_available)
plt.show()