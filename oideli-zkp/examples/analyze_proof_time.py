import matplotlib.pyplot as plt
from datetime import datetime
import seaborn as sns
import itertools

import pandas as pd


# https://stackoverflow.com/questions/5283649/plot-smooth-line-with-pyplot
def smooth(scalars, weight: float):  # Weight between 0 and 1
    last = scalars[0]  # First value in the plot (first timestep)
    smoothed = list()
    for point in scalars:
        smoothed_val = last * weight + (1 - weight) * point  # Calculate smoothed value
        smoothed.append(smoothed_val)                        # Save it
        last = smoothed_val                                  # Anchor the last smoothed value
    return smoothed


dfs = []

num_hours = 5

# num_workers = [1]
# num_workers = [1, 2, 4, 8, 16, 32, 64]
# num_workers = [32]
# num_workers = [1,32,64]
# num_workers = [1,32]
num_workers = [1,16,32,64]
for w in num_workers:
# num_shas = [1,2,4,39]
# for w,shas in itertools.product(num_workers, num_shas):
    # filename = "./results/02-single-cpu-workers/bench_{}_workers.csv".format(w)
    # filename = "./results/03-with-ulimit-5h/bench_{}_workers.csv".format(w)
    # filename = "./results/04-no-build-circuit/bench_{}_workers.csv".format(w)
    # filename = "./results/05-actually-no-build-circuit/bench_{}_workers.csv".format(w)
    # filename = "./results/06-merkle-depth-21/bench_{}_workers.csv".format(w)
    # filename = "./results/07-avoid-copying-merkle/bench_{}_workers.csv".format(w)
    filename = "./results/08-with-aud-and-shas/bench_{}_workers.csv".format(w)
    # filename = "./results/08-with-aud-and-shas/shas_{}_shas_{}_workers.csv".format(shas, w)

    events = []
    with open(filename) as file:
        for line in file:
            # format: 1702394450,84255
            timestamp, proof_time = line.strip().split(",")
            time = datetime.fromtimestamp(int(timestamp))
            event = {"time": time, "proof_time": int(proof_time)}
            events.append(event)

    df = pd.DataFrame(events)
    # print(df)
    dfs.append(df)


    print("- " + str(w) + " workers")
    # print("- " + str(w) + " workers, " + str(shas) + " shas")
    print("  proofs/hour: ", len(df) / num_hours)
    print(df.proof_time.describe())


    # plt.plot(df.time, df.proof_time)
    # plt.plot(df.time, smooth(df.proof_time, 0.95))

    # sns.histplot(df.proof_time, kde=True, stat="density")

plt.plot(num_workers, [len(df)/num_hours for df in dfs], marker='o',)
plt.xlabel ('Number of cores')
plt.ylabel ('Proof rate')
ax = plt.gca()
ax.set_xlim([0, 65])
ax.set_ylim([0, 1650])

# plt.show()
plt.savefig("results/proof_scaling.png")
