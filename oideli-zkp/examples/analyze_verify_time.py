import matplotlib.pyplot as plt
from datetime import datetime
import seaborn as sns

import pandas as pd

dfs = []

# num_workers = [1,32]
num_workers = [4]
# num_workers = [1]
for w in num_workers:
    # filename = "./results/05-actually-no-build-circuit/verify_{}_workers.csv".format(w)
    filename = "./results/09-verify-laptop/verify_{}_workers.csv".format(w)

    events = []
    with open(filename) as file:
        for line in file:
            # format: 0,20,8.68
            worker, proof_n, proof_time = line.strip().split(",")
            event = {"worker": int(worker), "verif_n": int(proof_n), "verif_time": float(proof_time)}
            events.append(event)

    df = pd.DataFrame(events)
    # print(df)
    dfs.append(df)

    print("- " + str(w) + " workers")
    print("  verifications/hour: ", len(df) * 2)
    print(df.verif_time.describe())

    # plt.plot(df.verif_time)

# plt.show()
