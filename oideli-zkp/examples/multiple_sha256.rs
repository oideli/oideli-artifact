use std::{
    env,
    sync::{Arc, Mutex},
    thread,
    time::{Duration, Instant, SystemTime, UNIX_EPOCH},
};

use itertools::Itertools;
use libafl_bolts::core_affinity;
use plonky2::{
    iop::{
        target::BoolTarget,
        witness::{PartialWitness, WitnessWrite},
    },
    plonk::{
        circuit_builder::CircuitBuilder,
        circuit_data::{CircuitConfig, CircuitData},
        config::{GenericConfig, PoseidonGoldilocksConfig},
    },
};
use plonky2_field::goldilocks_field::GoldilocksField;
use plonky2_sha256::circuit::{array_to_bits, Sha256Targets};
use sha2::{Digest, Sha256};

const D: usize = 2;
type C = PoseidonGoldilocksConfig;
type F = <C as GenericConfig<D>>::F;

fn main() {
    let parallelism: usize = env::var("PARALLELISM")
        .map(|v| v.parse::<usize>().unwrap())
        .unwrap_or(1);
    let running_time_minutes: u64 = env::var("RUNNING_TIME_MINUTES")
        .map(|v| v.parse::<u64>().unwrap())
        .unwrap_or(5);
    let number_shas: usize = env::var("NUMBER_SHAS")
        .map(|v| v.parse::<usize>().unwrap())
        .unwrap_or(2);
    println!("number of shas: {}", number_shas);

    let core_ids = core_affinity::get_core_ids().unwrap();
    println!("[bench] number of cores detected: {}", core_ids.len());
    assert!(
        core_ids.len() >= parallelism,
        "cannot have more parallelism than cores"
    );

    let xs = {
        let mut xs = Vec::new();
        for i in 0..number_shas {
            let mut x = vec![0; 671];
            for j in 0..671 {
                x[i] = (j * i) as u8;
            }
            xs.push(x);
        }
        xs
    };

    let h = {
        let mut hasher = Sha256::new();
        hasher.update(xs[0].clone());
        array_to_bits(hasher.finalize().as_slice())
    };

    let xs_bits: Vec<Vec<bool>> = xs.into_iter().map(|x| array_to_bits(&x)).collect();

    // circuit: h = hash(x_1) or .. or h = hash(x_n)
    let mut builder = CircuitBuilder::<F, D>::new(CircuitConfig::standard_recursion_zk_config());

    let sha_targets: Vec<Sha256Targets> = xs_bits
        .iter()
        .map(|bits| plonky2_sha256::circuit::make_circuits(&mut builder, bits.len() as u64))
        .collect();

    let h_targets: Vec<BoolTarget> = {
        let mut ts: Vec<BoolTarget> = Vec::new();
        for _ in 0..256 {
            let t = builder.add_virtual_bool_target_unsafe();
            ts.push(t);
        }
        ts
    };
    println!("number of gates (only shas): {}", builder.num_gates());

    let sha_equal = sha_targets
        .iter()
        .map(|sha| are_bits_equal(&mut builder, sha.digest.as_slice(), h_targets.as_slice()))
        .collect();
    let at_least_one_sha = or_many(&mut builder, &sha_equal);
    builder.assert_one(at_least_one_sha.target);

    println!(
        "number of gates (with disjunction): {}",
        builder.num_gates()
    );
    let circuit = builder.build::<C>();
    let circuit = Arc::new(circuit);

    let counter = Arc::new(Mutex::new(0));
    for worker in 0..parallelism {
        // clone values for worker
        let circuit = circuit.clone();
        let sha_targets = sha_targets.iter().map(|t| t.message.clone()).collect_vec();
        let xs_bits = xs_bits.clone();
        let h_targets = h_targets.clone();
        let h = h.clone();

        let core_id = core_ids[worker];

        let counter = Arc::clone(&counter);
        thread::spawn(move || {
            core_id.set_affinity().unwrap();
            // this thread pool forces the underlying plonky2 parallelism to use only one thread
            rayon::ThreadPoolBuilder::new()
                .num_threads(1)
                .build()
                .unwrap()
                .install(|| {
                    let mut i = 0;
                    loop {
                        // clone values for loop iter
                        let sha_targets = sha_targets.clone();
                        let xs_bits = xs_bits.clone();
                        let h_targets = h_targets.clone();
                        let h = h.clone();

                        println!("[bench][worker:{}] start proof {}", worker, i);
                        let now = Instant::now();
                        generate_proof(
                            number_shas,
                            sha_targets,
                            xs_bits,
                            h_targets,
                            h,
                            circuit.as_ref(),
                        );
                        let elapsed = now.elapsed();
                        println!(
                            "[bench][worker:{}] done proof {} in {:.2?}",
                            worker, i, elapsed
                        );
                        println!(
                            "[bench][prooftime]{},{}",
                            SystemTime::now()
                                .duration_since(UNIX_EPOCH)
                                .unwrap()
                                .as_secs(),
                            elapsed.as_millis()
                        );

                        let mut num = counter.lock().unwrap();
                        *num += 1;
                        i += 1;
                    }
                });
        });
    }

    thread::sleep(Duration::from_secs(running_time_minutes * 60));
    println!(
        "[bench] stopping process after {} minutes",
        running_time_minutes
    );
    println!(
        "[bench] number or finished proofs: {}",
        *counter.lock().unwrap()
    );
}

fn generate_proof(
    number_shas: usize,
    sha_targets: Vec<Vec<BoolTarget>>,
    xs_bits: Vec<Vec<bool>>,
    h_targets: Vec<BoolTarget>,
    h: Vec<bool>,
    circuit: &CircuitData<GoldilocksField, PoseidonGoldilocksConfig, 2>,
) {
    let mut pw = PartialWitness::new();

    for i in 0..number_shas {
        for (target, value) in sha_targets[i]
            .clone()
            .into_iter()
            .zip(xs_bits[i].clone().into_iter())
        {
            pw.set_bool_target(target, value)
        }
    }
    for i in 0..h.len() {
        pw.set_bool_target(h_targets[i], h[i])
    }
    circuit.prove(pw).unwrap();
}

fn are_bits_equal(
    builder: &mut CircuitBuilder<GoldilocksField, D>,
    b1: &[BoolTarget],
    b2: &[BoolTarget],
) -> BoolTarget {
    assert_eq!(b1.len(), b2.len());
    let mut bits_equal: Vec<BoolTarget> = Vec::new();
    for i in 0..b1.len() {
        let bit_equal = builder.is_equal(b1[i].target, b2[i].target);
        bits_equal.push(bit_equal);
    }
    let product = builder.mul_many(bits_equal.into_iter().map(|t| t.target));
    let one = builder.one();
    builder.is_equal(product, one)
}

fn or_many(
    builder: &mut CircuitBuilder<GoldilocksField, D>,
    bools: &Vec<BoolTarget>,
) -> BoolTarget {
    let sum_matches = builder.add_many(bools.into_iter().map(|t| t.target));
    let zero = builder.zero();
    let sum_is_zero = builder.is_equal(sum_matches, zero);
    builder.not(sum_is_zero)
}
