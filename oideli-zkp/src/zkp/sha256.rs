use plonky2::plonk::circuit_builder::CircuitBuilder;
use plonky2_field::goldilocks_field::GoldilocksField;
use plonky2_sha256::circuit::make_circuits;
use plonky2_sha256::circuit::Sha256Targets;

pub fn generate_targets_sha256(
    builder: &mut CircuitBuilder<GoldilocksField, { crate::D }>,
    len: usize,
) -> Sha256Targets {
    make_circuits(builder, len as u64)
}

#[cfg(test)]
mod tests {
    use plonky2::iop::witness::{PartialWitness, WitnessWrite};
    use plonky2::plonk::circuit_builder::CircuitBuilder;
    use plonky2::plonk::circuit_data::CircuitConfig;
    use plonky2_sha256::circuit::array_to_bits;
    use sha2::{Digest, Sha256};

    use crate::zkp::circuit_builder_transmuted::CircuitBuilderPrime;
    use crate::C;
    use crate::D;
    use crate::F;

    use super::generate_targets_sha256;

    #[test]
    // This test build a sha256 proof.
    fn test_hash() {
        let mut x = vec![0; 8];
        for i in 0..8 {
            x[i] = i as u8;
        }

        let mut y = vec![0; 8];
        for i in 0..8 {
            y[i] = i as u8;
        }

        let h = hash_two_inputs(&x, &y);

        let x_bits = array_to_bits(&x);
        let y_bits = array_to_bits(&y);

        let mut builder =
            CircuitBuilder::<F, D>::new(CircuitConfig::standard_recursion_zk_config());
        let targets = generate_targets_sha256(&mut builder, x_bits.len() + y_bits.len());
        let data = builder.build::<C>();

        // let (data, targets) =
        // crate::zkp::sha256::generate_circuit_sha256(x_bits.len(), y_bits.len(), h.len());

        let mut pw = PartialWitness::new();

        for i in 0..x_bits.len() {
            pw.set_bool_target(targets.message[i], x_bits[i])
        }
        for i in 0..y_bits.len() {
            pw.set_bool_target(targets.message[i + x_bits.len()], y_bits[i])
        }
        for i in 0..h.len() {
            pw.set_bool_target(targets.digest[i], h[i])
        }

        let proof = data.prove(pw).unwrap();
        data.verify(proof).unwrap();
    }

    #[test]
    #[should_panic]
    // This test tries to build a proof where the given hash is not the true hash of the input.
    fn test_wrong_hash() {
        let mut x = vec![0; 8];
        for i in 0..8 {
            x[i] = i as u8;
        }

        let mut y = vec![0; 8];
        for i in 0..8 {
            y[i] = i as u8;
        }

        let mut h = hash_two_inputs(&x, &y);

        h[4] = !h[4];

        let x_bits = array_to_bits(&x);
        let y_bits = array_to_bits(&y);

        let mut builder =
            CircuitBuilder::<F, D>::new(CircuitConfig::standard_recursion_zk_config());
        let targets = generate_targets_sha256(&mut builder, x_bits.len() + y_bits.len());
        let data = builder.build::<C>();

        let mut pw = PartialWitness::new();

        for i in 0..x_bits.len() {
            pw.set_bool_target(targets.message[i], x_bits[i])
        }
        for i in 0..y_bits.len() {
            pw.set_bool_target(targets.message[i + x_bits.len()], y_bits[i])
        }
        for i in 0..h.len() {
            pw.set_bool_target(targets.digest[i], h[i])
        }

        let proof = data.prove(pw).unwrap();
        data.verify(proof).unwrap();
    }

    fn hash_two_inputs(x: &[u8], y: &[u8]) -> Vec<bool> {
        let mut hasher = Sha256::new();
        hasher.update(x);
        hasher.update(y);
        array_to_bits(hasher.finalize().as_slice())
    }

    #[test]
    fn test_sha256_ops_count() {
        let mut builder =
            CircuitBuilder::<F, D>::new(CircuitConfig::standard_recursion_zk_config());
        let _ = generate_targets_sha256(&mut builder, 600 * 8);
        println!("number of gates: {}", builder.num_gates());

        // hackyyy
        // https://blog.knoldus.com/safe-way-to-access-private-fields-in-rust/
        let builder_exposed: CircuitBuilderPrime<F, D> = unsafe { std::mem::transmute(builder) };

        for gate in builder_exposed.gates.iter().cloned() {
            let count = builder_exposed
                .gate_instances
                .iter()
                .filter(|inst| inst.gate_ref == gate)
                .count();
            println!("- {} instances of {}", count, gate.0.id());
            println!("  - num ops: {}", gate.0.num_ops());
            println!("  - num wires: {}", gate.0.num_wires());
            println!("  - num constraints: {}", gate.0.num_constraints());
            println!("  - num constants: {}", gate.0.num_constants());
        }
    }
}
