use plonky2::hash::hash_types::HashOut;
use plonky2::hash::hash_types::HashOutTarget;
use plonky2::hash::hash_types::MerkleCapTarget;
use plonky2::hash::merkle_tree::MerkleCap;
use plonky2::hash::poseidon::PoseidonHash;
use plonky2::iop::target::Target;
use plonky2::iop::witness::PartialWitness;
use plonky2::iop::witness::WitnessWrite;
use plonky2::plonk::circuit_builder::CircuitBuilder;
use plonky2_field::goldilocks_field::GoldilocksField;
use plonky2_field::types::Field;

use plonky2::hash::merkle_proofs::MerkleProofTarget;
use plonky2::plonk::config::GenericConfig;

use crate::MAX_ID_LEN;

/// Fill a builder with the elements of a circuit for a merkle proof.
pub fn fill_builder_with_merkle(
    builder: &mut CircuitBuilder<GoldilocksField, { crate::D }>,
    depth: usize,
) -> (MerkleProofTarget, Target, MerkleCapTarget, Vec<Target>) {
    let merkle_proof = MerkleProofTarget {
        siblings: builder.add_virtual_hashes(depth),
    };

    let cap = builder.add_virtual_cap(0);

    let leaf_index = builder.add_virtual_target();
    let leaf_index_bits = builder.split_le(leaf_index, depth);

    let leaf_data = builder.add_virtual_targets(MAX_ID_LEN);

    builder.verify_merkle_proof_to_cap::<<crate::C as GenericConfig<{ crate::D }>>::InnerHasher>(
        leaf_data.clone(),
        &leaf_index_bits,
        &cap,
        &merkle_proof,
    );

    (merkle_proof, leaf_index, cap, leaf_data)
}

pub fn fill_circuit_with_merkle(
    pw: &mut PartialWitness<crate::F>,
    leaf_targets: Vec<Target>,
    siblings_target: Vec<HashOutTarget>,
    cap_target: MerkleCapTarget,
    leaf_index: Target,
    leaf_data: &Vec<GoldilocksField>,
    voter_index: usize,
    siblings: Vec<HashOut<crate::F>>,
    cap: &MerkleCap<GoldilocksField, PoseidonHash>,
) {
    // Set the merkle path in the proof
    assert_eq!(siblings.len(), siblings_target.len());
    for i in 0..siblings.len() {
        pw.set_hash_target(siblings_target[i], siblings[i]);
    }

    // Set the cap target
    pw.set_cap_target(&cap_target, &cap);

    // Set the wanted index
    pw.set_target(leaf_index, crate::F::from_canonical_usize(voter_index));

    // Set the leave data (from the same index)
    assert_eq!(leaf_data.len(), leaf_targets.len());
    for j in 0..leaf_data.len() {
        pw.set_target(leaf_targets[j], leaf_data[j]);
    }
}

#[cfg(test)]
mod test {

    use plonky2_field::goldilocks_field::GoldilocksField;

    use plonky2::hash::merkle_tree::MerkleTree;

    use plonky2::field::types::Field;
    use plonky2::iop::witness::PartialWitness;
    use plonky2::plonk::circuit_builder::CircuitBuilder;
    use plonky2::plonk::circuit_data::CircuitConfig;
    use plonky2::plonk::config::GenericConfig;
    use rand::Rng;

    use crate::C;
    use crate::D;
    use crate::F;
    use crate::MAX_ID_LEN;

    use super::fill_builder_with_merkle;
    use super::fill_circuit_with_merkle;

    fn generate_leaves(n: usize) -> Vec<Vec<GoldilocksField>> {
        let mut rng = rand::thread_rng();
        let mut leaves = Vec::new();
        for _i in 0..n {
            let mut leave = Vec::new();
            for _j in 0..MAX_ID_LEN {
                leave.push(F::from_canonical_u8(rng.gen()));
            }
            leaves.push(leave);
        }
        leaves
    }

    #[test]
    fn test_zkp_merkle_tree_ok() {
        let mut pw = PartialWitness::new();

        let log_n = 10;
        let n = 1 << log_n;
        let cap_height = 0;
        let leaves = generate_leaves(n);
        let tree = MerkleTree::<F, <C as GenericConfig<D>>::Hasher>::new(leaves, cap_height);
        let i: usize = 4;
        let proof = tree.prove(i);

        let mut builder =
            CircuitBuilder::<F, D>::new(CircuitConfig::standard_recursion_zk_config());

        let (proof_t, leaf_index, cap_t, leaf_data) = fill_builder_with_merkle(&mut builder, log_n);
        println!("number of gates: {}", builder.num_gates());
        let circuit = builder.build::<C>();

        fill_circuit_with_merkle(
            &mut pw,
            leaf_data,
            proof_t.siblings,
            cap_t,
            leaf_index,
            &tree.leaves[i],
            i,
            proof.siblings,
            &tree.cap,
        );

        let proof = circuit.prove(pw).unwrap();

        circuit.verify(proof).unwrap();
    }

    #[test]
    #[should_panic]
    fn test_zkp_merkle_tree_wrong_index() {
        let mut pw = PartialWitness::new();

        let log_n = 10;
        let n = 1 << log_n;
        let cap_height = 0;
        let leaves = generate_leaves(n);
        let tree = MerkleTree::<F, <C as GenericConfig<D>>::Hasher>::new(leaves, cap_height);
        let i: usize = 4;
        let proof = tree.prove(i);

        let mut builder =
            CircuitBuilder::<F, D>::new(CircuitConfig::standard_recursion_zk_config());

        let (proof_t, leaf_index, cap_t, leaf_data) = fill_builder_with_merkle(&mut builder, log_n);
        let circuit = builder.build::<C>();

        fill_circuit_with_merkle(
            &mut pw,
            leaf_data,
            proof_t.siblings,
            cap_t,
            leaf_index,
            &tree.leaves[i],
            i + 1,
            proof.siblings,
            &tree.cap,
        );

        let proof = circuit.prove(pw).unwrap();

        circuit.verify(proof).unwrap();
    }
    #[test]
    #[should_panic]
    fn test_zkp_merkle_tree_wrong_path() {
        let mut pw = PartialWitness::new();

        let log_n = 10;
        let n = 1 << log_n;
        let cap_height = 0;
        let leaves = generate_leaves(n);
        let tree = MerkleTree::<F, <C as GenericConfig<D>>::Hasher>::new(leaves, cap_height);
        let i: usize = 4;
        let mut proof = tree.prove(i);

        let mut builder =
            CircuitBuilder::<F, D>::new(CircuitConfig::standard_recursion_zk_config());

        let (proof_t, leaf_index, cap_t, leaf_data) = fill_builder_with_merkle(&mut builder, log_n);
        let circuit = builder.build::<C>();

        proof.siblings.reverse();

        fill_circuit_with_merkle(
            &mut pw,
            leaf_data,
            proof_t.siblings,
            cap_t,
            leaf_index,
            &tree.leaves[i],
            i,
            proof.siblings,
            &tree.cap,
        );

        let proof = circuit.prove(pw).unwrap();

        circuit.verify(proof).unwrap();
    }
}
