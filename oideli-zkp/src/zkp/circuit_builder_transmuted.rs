#![allow(dead_code)]

// copy/pasted stuff to be able to count the number of operations in a gate
// the idea is to copy (transmute) the circuit builder into a custom struct with fields that are accessible
// https://blog.knoldus.com/safe-way-to-access-private-fields-in-rust/

use plonky2::{
    gates::gate::{GateInstance, GateRef},
    hash::hash_types::RichField,
    iop::{generator::WitnessGenerator, target::Target},
    plonk::circuit_data::{CircuitConfig, CommonCircuitData, VerifierCircuitTarget},
};
use plonky2_field::{
    extension::Extendable,
    types::{Field, Field64},
};

pub struct CircuitBuilderPrime<F: RichField + Extendable<D>, const D: usize> {
    pub config: CircuitConfig,

    /// A domain separator, which is included in the initial Fiat-Shamir seed. This is generally not
    /// needed, but can be used to ensure that proofs for one application are not valid for another.
    /// Defaults to the empty vector.
    domain_separator: Option<Vec<F>>,

    /// The types of gates used in this circuit.
    pub gates: hashbrown::HashSet<GateRef<F, D>>,

    /// The concrete placement of each gate.
    pub(crate) gate_instances: Vec<GateInstance<F, D>>,

    /// Targets to be made public.
    public_inputs: Vec<Target>,

    /// The next available index for a `VirtualTarget`.
    virtual_target_index: usize,

    copy_constraints: Vec<CopyConstraintPrime>,

    /// A tree of named scopes, used for debugging.
    context_log: ContextTreePrime,

    /// Generators used to generate the witness.
    generators: Vec<Box<dyn WitnessGenerator<F>>>,

    constants_to_targets: hashbrown::HashMap<F, Target>,
    targets_to_constants: hashbrown::HashMap<Target, F>,

    /// Memoized results of `arithmetic` calls.
    pub(crate) base_arithmetic_results: hashbrown::HashMap<BaseArithmeticOperationPrime<F>, Target>,

    /// Memoized results of `arithmetic_extension` calls.
    pub(crate) arithmetic_results:
        hashbrown::HashMap<ExtensionArithmeticOperation<F, D>, ExtensionTarget<D>>,

    /// Map between gate type and the current gate of this type with available slots.
    current_slots: hashbrown::HashMap<GateRef<F, D>, CurrentSlot<F, D>>,

    /// List of constant generators used to fill the constant wires.
    constant_generators: Vec<ConstantGenerator<F>>,

    /// Optional common data. When it is `Some(goal_data)`, the `build` function panics if the resulting
    /// common data doesn't equal `goal_data`.
    /// This is used in cyclic recursion.
    pub(crate) goal_common_data: Option<CommonCircuitData<F, D>>,

    /// Optional verifier data that is registered as public inputs.
    /// This is used in cyclic recursion to hold the circuit's own verifier key.
    pub(crate) verifier_data_public_input: Option<VerifierCircuitTarget>,
}

pub struct CopyConstraintPrime {
    pub pair: (Target, Target),
    pub name: String,
}

pub(crate) struct ContextTreePrime {
    /// The name of this scope.
    name: String,
    /// The level at which to log this scope and its children.
    level: log::Level,
    /// The gate count when this scope was created.
    enter_gate_count: usize,
    /// The gate count when this scope was destroyed, or None if it has not yet been destroyed.
    exit_gate_count: Option<usize>,
    /// Any child contexts.
    children: Vec<ContextTreePrime>,
}

pub(crate) struct BaseArithmeticOperationPrime<F: Field64> {
    const_0: F,
    const_1: F,
    multiplicand_0: Target,
    multiplicand_1: Target,
    addend: Target,
}
pub(crate) struct ExtensionArithmeticOperation<F: Field64 + Extendable<D>, const D: usize> {
    const_0: F,
    const_1: F,
    multiplicand_0: ExtensionTarget<D>,
    multiplicand_1: ExtensionTarget<D>,
    addend: ExtensionTarget<D>,
}
pub struct ExtensionTarget<const D: usize>(pub [Target; D]);
pub struct CurrentSlot<F: RichField + Extendable<D>, const D: usize> {
    pub current_slot: hashbrown::HashMap<Vec<F>, (usize, usize)>,
}
pub(crate) struct ConstantGenerator<F: Field> {
    pub row: usize,
    pub constant_index: usize,
    pub wire_index: usize,
    pub constant: F,
}
