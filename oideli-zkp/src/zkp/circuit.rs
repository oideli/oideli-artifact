use itertools::Itertools;

use plonky2::iop::target::{BoolTarget, Target};

use plonky2::field::types::Field;
use plonky2::plonk::circuit_builder::CircuitBuilder;
use plonky2::plonk::circuit_data::CircuitConfig;
use plonky2_field::goldilocks_field::GoldilocksField;

use crate::zkp::base64;
use crate::zkp::lookup::lookup;
use crate::zkp::merkle::fill_builder_with_merkle;
use crate::zkp::poseidon::add_poseidon_gate;
use crate::zkp::public_inputs::{register_public_inputs, PublicInputTargets};
use crate::zkp::sha256::generate_targets_sha256;
use crate::{Circuit, HASH_SIZE_BYTES, NONCE_B64_BITS_LEN, NONCE_BITS_LEN};

use super::{CircuitSizing, Targets};
use crate::C;
use crate::D;
use crate::F;

/// Builds the circuit and returns it with the corresponding targets
///
/// The resulting circuit can then be input in the `generate_zkp` function
/// to actually build a zkp based on the circuit.
///
/// Simplified diagram of the circuit:
/// ```no_run
///                                digest
///                                   │
///                           ┌───────┴──────┐
///                           │    sha256    │
///                           └──┬────────┬──┘
///                              │        │
///                       jwt.header  jwt.body
///                                       │
///                                ┌──────┴─────┐
///                                │   base 64  │
///                                └──────┬─────┘
///                                       │
///                                  json jwt.body
///                                       |
///                                 ┌─────┴────┐
///                                 │  lookup  │
///                                 └──┬────┬──┘
///                                    │    │
///  ┌─────────┐    ┌───┐   nonce field│    │email field   ┌───┐
///  │ base 64 ├────┤ = ├──────────────┘    └──────────────┤ = │
///  └────┬────┘    └───┘                                  └─┬─┘
///       │                                                  │
///     nonce                                            voter id
///       │                                                  │
/// ┌─────┴────┐                                     ┌───────┴───────┐
/// │ Poseidon │                                     │  Merkle tree  │
/// └──┬────┬──┘                                     └──┬─────────┬──┘
///    │    │                                           │         │
///   ns    nv                                   voter index     voters
/// ```
pub fn build_circuit(sizing: &CircuitSizing) -> Result<(Circuit, Targets), anyhow::Error> {
    println!("generating circuit...");
    let mut builder = CircuitBuilder::<F, D>::new(CircuitConfig::standard_recursion_zk_config());

    // add targets
    // ---------------------------------------------

    // sha256 part
    let jwt_b64_len = sizing.jwt_header_b64_bits + 8 + base64::converted_len(sizing.jwt_body * 8); // dot in between
    let g_before_sha = builder.num_gates();
    let sha256 = generate_targets_sha256(&mut builder, jwt_b64_len);
    println!(
        "[gates-count] sha256: {}",
        builder.num_gates() - g_before_sha
    );
    let jwt_header_b64 = sha256.message[0..(sizing.jwt_header_b64_bits + 8)].to_vec();
    // poseidon hash part
    let g_before_poseidon = builder.num_gates();
    let (ns_nv, nonce_hashed) = add_poseidon_gate(&mut builder, 2 * HASH_SIZE_BYTES);
    println!(
        "[gates-count] poseidon: {}",
        builder.num_gates() - g_before_poseidon
    );

    // merkle tree part
    let g_before_merkle = builder.num_gates();
    let (merkle_witness, leaf_index, merkle_cap, merkle_leaf) =
        fill_builder_with_merkle(&mut builder, sizing.merkle_depth);
    assert_eq!(merkle_cap.0.len(), 1);
    println!(
        "[gates-count] merkle: {}",
        builder.num_gates() - g_before_merkle
    );

    let jwt_body = builder.add_virtual_targets(sizing.jwt_body);
    let sub = builder.add_virtual_targets(sizing.sub_len);
    let aud = builder.add_virtual_targets(sizing.aud_len);

    // register public inputs
    let public_inputs = PublicInputTargets {
        digest: sha256.digest.as_slice().try_into()?,
        nv: ns_nv[HASH_SIZE_BYTES..].try_into()?,
        merkle_cap: merkle_cap.0[0].elements,
        sub: sub.clone(),
        aud: aud.clone(),
    };
    register_public_inputs(&mut builder, public_inputs);

    // connect circuit parts
    // ---------------------------------------------
    let g_before_nonceb64 = builder.num_gates();
    let nonce_b64 = nonce_pad_and_base64(&mut builder, nonce_hashed);
    println!(
        "[gates-count] nonceb64: {}",
        builder.num_gates() - g_before_nonceb64
    );

    let g_before_convprelookup = builder.num_gates();
    let jwt_body_bits = bytes_to_bits(&mut builder, jwt_body.clone());
    let nonce_b64_bytes = bits_to_bytes(&mut builder, &nonce_b64);
    println!(
        "[gates-count] convprelookup: {}",
        builder.num_gates() - g_before_convprelookup
    );

    let g_before_lookup = builder.num_gates();
    lookup(
        &mut builder,
        &sizing,
        &merkle_leaf.clone().try_into().unwrap(),
        &nonce_b64_bytes.try_into().unwrap(),
        &sub.clone(),
        &aud.clone(),
        &jwt_body,
    );
    println!(
        "[gates-count] lookup: {}",
        builder.num_gates() - g_before_lookup
    );

    let g_before_bodyb64 = builder.num_gates();
    connect_jwt_body_sha256(
        &mut builder,
        sizing.jwt_header_b64_bits,
        &jwt_body_bits,
        &sha256.message,
    );
    println!(
        "[gates-count] bodyb64: {}",
        builder.num_gates() - g_before_bodyb64
    );

    builder.print_gate_counts(0);
    let circuit = builder.build::<C>();

    let targets = Targets {
        jwt_body,
        jwt_header_b64,
        jwt_digest: sha256.digest,
        sub,
        aud,
        ns_nv,
        merkle_witness,
        merkle_leaf,
        merkle_leaf_index: leaf_index,
        merkle_cap,
    };

    Ok((circuit, targets))
}

fn nonce_pad_and_base64(
    builder: &mut CircuitBuilder<GoldilocksField, D>,
    nonce: [Target; 4],
) -> Vec<BoolTarget> {
    let mut nonce_bits = field_elems_to_bits(builder, nonce.to_vec());
    assert_eq!(nonce_bits.len(), NONCE_BITS_LEN);
    // add one byte of padding to reach mod 24, which makes the base64 conversion land on round bytes
    // 24 * 11 = 264 = 33 bytes
    // 33 * 4/3 = 44
    let zero = builder.zero();
    let mut padding = builder.split_le(zero, 8);
    nonce_bits.append(&mut padding);
    // base64 wiring
    let nonce_b64_bits = base64::convert_bits(builder, &nonce_bits);
    assert_eq!(nonce_b64_bits.len(), NONCE_B64_BITS_LEN);
    nonce_b64_bits
}

fn connect_jwt_body_sha256(
    builder: &mut CircuitBuilder<GoldilocksField, D>,
    jwt_header_b64_bits_len: usize,
    jwt_body_targets: &Vec<BoolTarget>,
    sha256_targets: &Vec<BoolTarget>,
) {
    let jwt_body_b64 = base64::convert_bits(builder, jwt_body_targets);
    let body_bits_index = jwt_header_b64_bits_len + 8;
    assert!(
        {
            // sha256 input is padded, cf https://github.com/polymerdao/plonky2-sha256/blob/06d128e78ed8d29b21d58294b069e852c1866f8d/src/circuit.rs#L285
            body_bits_index + jwt_body_b64.len()
        } < sha256_targets.len()
    );
    for (jwt_body_b64_bit, sha256_bit) in jwt_body_b64.iter().zip_eq(
        sha256_targets
            .iter()
            .skip(body_bits_index)
            .take(jwt_body_b64.len()),
    ) {
        builder.connect(jwt_body_b64_bit.target, sha256_bit.target);
    }
}

pub fn bytes_to_bits(
    builder: &mut CircuitBuilder<GoldilocksField, D>,
    bytes: Vec<Target>,
) -> Vec<BoolTarget> {
    bytes
        .into_iter()
        .map(|byte| {
            let mut byte_bits = builder.split_le(byte, 8);
            byte_bits.reverse();
            byte_bits
        })
        .flatten()
        .collect()
}

fn bits_to_bytes(
    builder: &mut CircuitBuilder<GoldilocksField, D>,
    bits_in: &Vec<BoolTarget>,
) -> Vec<Target> {
    assert_eq!(bits_in.len() % 8, 0);
    bits_in
        .chunks_exact(8)
        .map(|chunk| -> Target {
            let powers_of_2: Vec<Target> = chunk
                .iter()
                .zip_eq((0..8).rev()) // power of 2 corresponding to this bit
                .map(|(bit, exp)| {
                    builder.mul_const(F::from_canonical_u8(u8::pow(2, exp)), bit.target)
                })
                .collect();
            builder.add_many(powers_of_2)
        })
        .collect()
}

pub fn field_elems_to_bits(
    builder: &mut CircuitBuilder<GoldilocksField, D>,
    fs: Vec<Target>,
) -> Vec<BoolTarget> {
    fs.into_iter()
        .flat_map(|f| {
            let mut byte_bits = builder.split_le(f, 64); // field elem is 64 bits
            byte_bits.reverse(); // convert to big-endian
            byte_bits
        })
        .collect()
}

#[cfg(test)]
mod test {
    use itertools::Itertools;
    use plonky2::iop::target::BoolTarget;
    use plonky2::{
        iop::witness::{PartialWitness, WitnessWrite},
        plonk::{circuit_builder::CircuitBuilder, circuit_data::CircuitConfig},
    };
    use plonky2_sha256::circuit::array_to_bits;

    use crate::mock::jwt::mock_nonces;
    use crate::zkp::circuit::{connect_jwt_body_sha256, nonce_pad_and_base64};
    use crate::{C, D, F};

    #[test]
    fn test_zkp_nonce_pad_and_base64() {
        let (_, _, nonce, nonce_b64) = mock_nonces(42);
        assert_eq!(nonce_b64.len(), 44);
        let nonce_b64_bits = array_to_bits(&nonce_b64);

        let mut builder =
            CircuitBuilder::<F, D>::new(CircuitConfig::standard_recursion_zk_config());

        let nonce_targets = builder.add_virtual_targets(4).try_into().unwrap();
        let nonce_b64_targets = nonce_pad_and_base64(&mut builder, nonce_targets);
        assert_eq!(nonce_b64_bits.len(), nonce_b64_targets.len());

        let mut pw = PartialWitness::<F>::new();
        for i in 0..4 {
            pw.set_target(nonce_targets[i], nonce[i]);
        }
        for i in 0..nonce_b64_bits.len() {
            pw.set_bool_target(nonce_b64_targets[i], nonce_b64_bits[i]);
        }

        let circuit = builder.build::<C>();
        let proof = circuit.prove(pw).unwrap();
        circuit.verify(proof).unwrap()
    }

    #[test]
    fn test_connect_jwt_body_sha256() {
        let header_b64 = String::from("a").as_bytes().to_vec();
        let body = String::from("sometext").as_bytes().to_vec();
        assert_eq!(body.len(), 8); // 64 bits, rounded up mod 6 gives 66, so 11 base64 chars
        let body_bits = array_to_bits(&body);

        let expected_output = {
            let mut o = header_b64.clone();
            o.push('.' as u8);
            o.append(&mut base64_url::encode(&body).into_bytes());
            o
        };
        assert_eq!(expected_output.len(), 1 + 1 + 11); // header + dot + body
        let output_bits = array_to_bits(&expected_output);

        let mut builder =
            CircuitBuilder::<F, D>::new(CircuitConfig::standard_recursion_zk_config());
        let in_targets: Vec<BoolTarget> = body_bits
            .iter()
            .map(|_| builder.add_virtual_bool_target_unsafe())
            .collect();
        let out_targets: Vec<BoolTarget> = output_bits
            .iter()
            .chain(vec![&false; 512 - (output_bits.len() % 512)])
            .map(|_| builder.add_virtual_bool_target_unsafe())
            .collect();
        connect_jwt_body_sha256(
            &mut builder,
            header_b64.len() * 8,
            &in_targets,
            &out_targets,
        );
        let circuit = builder.build::<C>();

        let mut pw = PartialWitness::<F>::new();
        for (value, target) in body_bits.into_iter().zip_eq(in_targets.into_iter()) {
            pw.set_bool_target(target, value);
        }
        let out_len = output_bits.len();
        for (value, target) in output_bits
            .into_iter()
            .zip_eq(out_targets.into_iter().take(out_len))
        {
            pw.set_bool_target(target, value);
        }

        let proof = circuit.prove(pw).unwrap();
        circuit.verify(proof).unwrap()
    }
}
