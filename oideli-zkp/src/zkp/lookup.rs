use plonky2::field::types::Field;
use plonky2::iop::target::BoolTarget;
use plonky2::iop::target::Target;
use plonky2::plonk::circuit_builder::CircuitBuilder;
use plonky2_field::goldilocks_field::GoldilocksField;

use crate::D;
use crate::F;
use crate::ID_SUFFIX;
use crate::ID_SUFFIX_LEN;
use crate::MAX_ID_LEN;
use crate::MAX_LOOKUP_OFFSET;
use crate::MIN_ID_LEN;
use crate::NONCE_B64_BYTES_LEN;

use super::CircuitSizing;

pub fn lookup(
    builder: &mut CircuitBuilder<GoldilocksField, D>,
    sizing: &CircuitSizing,
    id: &[Target; MAX_ID_LEN],
    nonce: &[Target; NONCE_B64_BYTES_LEN],
    sub: &Vec<Target>,
    aud: &Vec<Target>,
    jwt_body: &Vec<Target>,
) {
    // prepare suffix
    let suffix: [Target; ID_SUFFIX_LEN] = ID_SUFFIX
        .as_bytes()
        .to_owned()
        .into_iter()
        .map(|byte| builder.constant(F::from_canonical_u8(byte)))
        .collect::<Vec<Target>>()
        .try_into()
        .unwrap();

    // iterate over the possible sizes of id
    // offset is the difference between the minimal id size and the currently considered one
    let mut offset_matches: Vec<BoolTarget> = Vec::new();
    for offset in 0..=MAX_LOOKUP_OFFSET {
        let id_matches = if sizing.is_nonce_after_id {
            // match id on len (MIN_ID_LEN + offset)
            is_array_equal(
                builder,
                &id[0..(MIN_ID_LEN + offset)],
                &jwt_body[sizing.id_min_index..(sizing.id_min_index + MIN_ID_LEN + offset)],
            )
        } else {
            // match id from id_index + (length of nonce padding)
            is_array_equal(
                builder,
                &id[0..(MIN_ID_LEN + offset)],
                &jwt_body[sizing.id_min_index + (MAX_LOOKUP_OFFSET - offset)
                    ..(sizing.id_min_index + (MAX_LOOKUP_OFFSET - offset) + (MIN_ID_LEN + offset))],
            )
        };
        // match suffix of id
        let id_suffix_matches = is_array_equal(
            builder,
            &suffix,
            &id[(MIN_ID_LEN + offset - ID_SUFFIX_LEN)..(MIN_ID_LEN + offset)],
        );

        let nonce_matches = if sizing.is_nonce_after_id {
            // match nonce from nonce_index + offset
            is_array_equal(
                builder,
                nonce,
                &jwt_body[(sizing.nonce_min_index + offset)
                    ..(sizing.nonce_min_index + offset + NONCE_B64_BYTES_LEN)],
            )
        } else {
            // match nonce from nonce_index
            // could be taken out of loop
            is_array_equal(
                builder,
                nonce,
                &jwt_body[(sizing.nonce_min_index)..(sizing.nonce_min_index + NONCE_B64_BYTES_LEN)],
            )
        };

        let sub_matches = if sizing.is_sub_between_id_nonce {
            if sizing.is_nonce_after_id {
                // order: id sub nonce
                // match sub from sub_index + offset
                is_array_equal(
                    builder,
                    &sub,
                    &jwt_body[sizing.sub_min_index + offset
                        ..(sizing.sub_min_index + offset + sizing.sub_len)],
                )
            } else {
                // order: nonce sub id
                // match sub from sub_index + (MAX_OFFSET - offset)
                is_array_equal(
                    builder,
                    &sub,
                    &jwt_body[sizing.sub_min_index + (MAX_LOOKUP_OFFSET - offset)
                        ..(sizing.sub_min_index + (MAX_LOOKUP_OFFSET - offset) + sizing.sub_len)],
                )
            }
        } else {
            // match sub from sub_index
            // could be taken out of loop
            is_array_equal(
                builder,
                &sub,
                &jwt_body[sizing.sub_min_index..(sizing.sub_min_index + sizing.sub_len)],
            )
        };

        // assuming aud is at beginning
        let aud_matches = is_array_equal(
            builder,
            &aud,
            &jwt_body[sizing.aud_min_index..(sizing.aud_min_index + sizing.aud_len)],
        );

        let and_1 = builder.and(id_matches, id_suffix_matches);
        let and_2 = builder.and(and_1, nonce_matches);
        let and_3 = builder.and(and_2, sub_matches);
        let all_fields_match = builder.and(and_3, aud_matches);
        offset_matches.push(all_fields_match)
    }
    let disjunction = or_many(builder, &offset_matches);
    builder.assert_one(disjunction.target)
}

fn or_many(
    builder: &mut CircuitBuilder<GoldilocksField, D>,
    bools: &Vec<BoolTarget>,
) -> BoolTarget {
    let sum_matches = builder.add_many(bools.into_iter().map(|t| t.target));
    let zero = builder.zero();
    let sum_is_zero = builder.is_equal(sum_matches, zero);
    builder.not(sum_is_zero)
}

fn is_array_equal(
    builder: &mut CircuitBuilder<GoldilocksField, D>,
    a1: &[Target],
    a2: &[Target],
) -> BoolTarget {
    assert_eq!(a1.len(), a2.len());
    let mut elems_equal: Vec<BoolTarget> = Vec::new();
    for i in 0..a1.len() {
        let elem_equal = builder.is_equal(a1[i], a2[i]);
        elems_equal.push(elem_equal);
    }
    let product = builder.mul_many(elems_equal.into_iter().map(|t| t.target));
    let one = builder.one();
    builder.is_equal(product, one)
}

#[cfg(test)]
mod test {
    use plonky2::{
        iop::witness::{PartialWitness, WitnessWrite},
        plonk::{circuit_builder::CircuitBuilder, circuit_data::CircuitConfig},
    };

    use crate::{
        jwt::{padded_id, padded_nonce_b64},
        zkp::CircuitSizing,
        C, D, F, MAX_ID_LEN,
    };
    use plonky2::field::types::Field;

    use super::lookup;

    const NONCE_PROPER_LEN: &str = "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb";

    #[test]
    fn test_zkp_lookup_ok_sub_id_nonce() {
        let id = "abcd@gmail.com";
        let padded_id = padded_id(id);
        let nonce = NONCE_PROPER_LEN;
        let padded_nonce = &padded_nonce_b64(nonce, id);
        let sub = "aa";
        let aud = "bbb";
        let jwt = format!(
            "{{aud:{},sub:{},email:{},nonce:{}}}",
            aud, sub, id, padded_nonce,
        );
        let sizing = CircuitSizing {
            jwt_header_b64_bits: 0,
            jwt_body: jwt.len(),
            merkle_depth: 0,
            id_min_index: 22,
            nonce_min_index: 40,
            sub_min_index: 13,
            sub_len: sub.len(),
            aud_min_index: 5,
            aud_len: aud.len(),
            is_nonce_after_id: true,
            is_sub_between_id_nonce: false,
        };
        test_zkp_lookup(&padded_id, nonce, sub, aud, &jwt, &sizing)
    }

    #[test]
    fn test_zkp_lookup_ok_max_size() {
        let id = "abcdabcdabcdabcdabcdabcdabcdabcdabcdabcd@gmail.com";
        assert_eq!(id.len(), MAX_ID_LEN);
        let padded_id = padded_id(id);
        let nonce = NONCE_PROPER_LEN;
        let padded_nonce = &padded_nonce_b64(nonce, id);
        let sub = "aa";
        let aud = "bbb";
        let jwt = format!(
            "{{aud:{},sub:{},email:{},nonce:{}}}",
            aud, sub, id, padded_nonce
        );
        let sizing = CircuitSizing {
            jwt_header_b64_bits: 0,
            jwt_body: jwt.len(),
            merkle_depth: 0,
            id_min_index: 22,
            nonce_min_index: 40,
            sub_min_index: 13,
            sub_len: sub.len(),
            aud_min_index: 5,
            aud_len: aud.len(),
            is_nonce_after_id: true,
            is_sub_between_id_nonce: false,
        };
        test_zkp_lookup(&padded_id, nonce, sub, aud, &jwt, &sizing)
    }

    #[test]
    fn test_zkp_lookup_ok_sub_nonce_id() {
        let id = "abcd@gmail.com";
        let padded_id = padded_id(id);
        let nonce = NONCE_PROPER_LEN;
        let padded_nonce = &padded_nonce_b64(nonce, id);
        let sub = "aa";
        let aud = "bbb";
        let jwt = format!(
            "{{aud:{},sub:{},nonce:{},email:{}}}",
            aud, sub, padded_nonce, id
        );
        let sizing = CircuitSizing {
            jwt_header_b64_bits: 0,
            jwt_body: jwt.len(),
            merkle_depth: 0,
            id_min_index: 73,
            nonce_min_index: 22,
            sub_min_index: 13,
            sub_len: sub.len(),
            aud_min_index: 5,
            aud_len: aud.len(),
            is_nonce_after_id: false,
            is_sub_between_id_nonce: false,
        };
        test_zkp_lookup(&padded_id, nonce, sub, aud, &jwt, &sizing)
    }

    #[test]
    fn test_zkp_lookup_ok_id_sub_nonce() {
        let id = "abcd@gmail.com";
        let padded_id = padded_id(id);
        let nonce = NONCE_PROPER_LEN;
        let padded_nonce = &padded_nonce_b64(nonce, id);
        let sub = "aa";
        let aud = "bbb";
        let jwt = format!(
            "{{aud:{},email:{},sub:{},nonce:{}}}",
            aud, id, sub, padded_nonce
        );
        let sizing = CircuitSizing {
            jwt_header_b64_bits: 0,
            jwt_body: jwt.len(),
            merkle_depth: 0,
            id_min_index: 15,
            nonce_min_index: 40,
            sub_min_index: 31,
            sub_len: sub.len(),
            aud_min_index: 5,
            aud_len: aud.len(),
            is_nonce_after_id: true,
            is_sub_between_id_nonce: true,
        };
        test_zkp_lookup(&padded_id, nonce, sub, aud, &jwt, &sizing)
    }

    #[test]
    fn test_zkp_lookup_ok_nonce_sub_id() {
        let id = "abcd@gmail.com";
        let padded_id = padded_id(id);
        let nonce = NONCE_PROPER_LEN;
        let padded_nonce = &padded_nonce_b64(nonce, id);
        let sub = "aa";
        let aud = "bbb";
        let jwt = format!(
            "{{aud:{},nonce:{},sub:{},email:{}}}",
            aud, padded_nonce, sub, id
        );
        let sizing = CircuitSizing {
            jwt_header_b64_bits: 0,
            jwt_body: jwt.len(),
            merkle_depth: 0,
            id_min_index: 73,
            nonce_min_index: 15,
            sub_min_index: 64,
            sub_len: sub.len(),
            aud_min_index: 5,
            aud_len: aud.len(),
            is_nonce_after_id: false,
            is_sub_between_id_nonce: true,
        };
        test_zkp_lookup(&padded_id, nonce, sub, aud, &jwt, &sizing)
    }

    #[test]
    #[should_panic(expected = "was set twice with different values")]
    fn test_zkp_lookup_fail_id() {
        let id = "abcd@gmail.com";
        let padded_id = padded_id(id);
        let nonce = NONCE_PROPER_LEN;
        let padded_nonce = &padded_nonce_b64(nonce, id);
        let sub = "aa";
        let aud = "bbb";
        let jwt = format!(
            "{{aud:{},sub:{},email:{},nonce:{}}}",
            aud, sub, "abc@gmail.com", padded_nonce
        );
        let sizing = CircuitSizing {
            jwt_header_b64_bits: 0,
            jwt_body: jwt.len(),
            merkle_depth: 0,
            id_min_index: 22,
            nonce_min_index: 40,
            sub_min_index: 13,
            sub_len: sub.len(),
            aud_min_index: 5,
            aud_len: aud.len(),
            is_nonce_after_id: true,
            is_sub_between_id_nonce: false,
        };
        test_zkp_lookup(&padded_id, nonce, sub, aud, &jwt, &sizing)
    }

    #[test]
    #[should_panic(expected = "was set twice with different values")]
    fn test_zkp_lookup_fail_suffix() {
        let id = "abcd@gmail.com";
        let padded_id = padded_id(id);
        let nonce = NONCE_PROPER_LEN;
        let padded_nonce = &padded_nonce_b64(nonce, id);
        let sub = "aa";
        let aud = "bbb";
        let jwt = format!(
            "{{aud:{},sub:{},email:{},nonce:{}}}",
            aud, sub, "abcd@gmall.com", padded_nonce
        );
        let sizing = CircuitSizing {
            jwt_header_b64_bits: 0,
            jwt_body: jwt.len(),
            merkle_depth: 0,
            id_min_index: 22,
            nonce_min_index: 40,
            sub_min_index: 13,
            sub_len: sub.len(),
            aud_min_index: 5,
            aud_len: aud.len(),
            is_nonce_after_id: true,
            is_sub_between_id_nonce: false,
        };
        test_zkp_lookup(&padded_id, nonce, sub, aud, &jwt, &sizing)
    }

    #[test]
    #[should_panic(expected = "was set twice with different values")]
    fn test_zkp_lookup_fail_nonce() {
        let id = "abcd@gmail.com";
        let padded_id = padded_id(id);
        let nonce = NONCE_PROPER_LEN;
        let sub = "aa";
        let aud = "bbb";
        let jwt = format!(
            "{{aud:{},sub:{},email:{},nonce:{}}}",
            aud,
            sub,
            id,
            &padded_nonce_b64(&("a".to_owned() + &nonce[1..]), id)
        );
        let sizing = CircuitSizing {
            jwt_header_b64_bits: 0,
            jwt_body: jwt.len(),
            merkle_depth: 0,
            id_min_index: 22,
            nonce_min_index: 40,
            sub_min_index: 13,
            sub_len: sub.len(),
            aud_min_index: 5,
            aud_len: aud.len(),
            is_nonce_after_id: true,
            is_sub_between_id_nonce: false,
        };
        test_zkp_lookup(&padded_id, nonce, sub, aud, &jwt, &sizing)
    }

    #[test]
    #[should_panic(expected = "was set twice with different values")]
    fn test_zkp_lookup_fail_sub() {
        let id = "abcd@gmail.com";
        let padded_id = padded_id(id);
        let nonce = NONCE_PROPER_LEN;
        let padded_nonce = &padded_nonce_b64(nonce, id);
        let sub = "aa";
        let aud = "bbb";
        let jwt = format!(
            "{{aud:{},sub:{},email:{},nonce:{}}}",
            ("q".to_owned() + &sub[1..]),
            aud,
            id,
            padded_nonce
        );
        let sizing = CircuitSizing {
            jwt_header_b64_bits: 0,
            jwt_body: jwt.len(),
            merkle_depth: 0,
            id_min_index: 22,
            nonce_min_index: 40,
            sub_min_index: 13,
            sub_len: sub.len(),
            aud_min_index: 5,
            aud_len: aud.len(),
            is_nonce_after_id: true,
            is_sub_between_id_nonce: false,
        };
        test_zkp_lookup(&padded_id, nonce, sub, aud, &jwt, &sizing)
    }

    fn test_zkp_lookup(
        id: &str,
        nonce: &str,
        sub: &str,
        aud: &str,
        jwt: &str,
        sizing: &CircuitSizing,
    ) {
        let mut builder =
            CircuitBuilder::<F, D>::new(CircuitConfig::standard_recursion_zk_config());

        let jwt_targets = builder.add_virtual_targets(jwt.len());
        let id_targets = builder.add_virtual_targets(id.len());
        let sub_targets = builder.add_virtual_targets(sub.len());
        let aud_targets = builder.add_virtual_targets(aud.len());
        let nonce_targets = builder.add_virtual_targets(nonce.len());

        lookup(
            &mut builder,
            &sizing,
            &id_targets.clone().try_into().unwrap(),
            &nonce_targets.clone().try_into().unwrap(),
            &sub_targets,
            &aud_targets,
            &jwt_targets,
        );

        let mut pw = PartialWitness::<F>::new();

        let jwt_bytes = jwt.as_bytes();
        for i in 0..jwt.len() {
            pw.set_target(jwt_targets[i], F::from_canonical_u8(jwt_bytes[i]));
        }
        let id_bytes = id.as_bytes();
        for i in 0..id.len() {
            pw.set_target(id_targets[i], F::from_canonical_u8(id_bytes[i]));
        }
        let nonce_bytes = nonce.as_bytes();
        for i in 0..nonce.len() {
            pw.set_target(nonce_targets[i], F::from_canonical_u8(nonce_bytes[i]));
        }
        let sub_bytes = sub.as_bytes();
        for i in 0..sub.len() {
            pw.set_target(sub_targets[i], F::from_canonical_u8(sub_bytes[i]));
        }
        let aud_bytes = aud.as_bytes();
        for i in 0..aud.len() {
            pw.set_target(aud_targets[i], F::from_canonical_u8(aud_bytes[i]));
        }

        // TODO remove
        println!("jwt: {}", jwt);

        println!("number of gates: {}", builder.num_gates());
        let circuit = builder.build::<C>();
        let proof = circuit.prove(pw).unwrap();
        circuit.verify(proof).unwrap()
    }
}
