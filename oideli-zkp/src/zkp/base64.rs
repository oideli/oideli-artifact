use itertools::Itertools;
use plonky2::field::types::Field;
use plonky2::{
    iop::target::{BoolTarget, Target},
    plonk::circuit_builder::CircuitBuilder,
};
use plonky2_field::goldilocks_field::GoldilocksField;

use crate::zkp::base64;

use crate::D;
use crate::F;

pub fn converted_len(initial_len: usize) -> usize {
    padded_mod_6(initial_len) * 4 / 3
}

pub fn padded_mod_6(x: usize) -> usize {
    x + ((6 - (x % 6)) % 6)
}

/// Converts a sequence of bits into its base64url ascii encoding
pub fn convert_bits(
    builder: &mut CircuitBuilder<GoldilocksField, D>,
    bits_in: &Vec<BoolTarget>,
) -> Vec<BoolTarget> {
    let as_6_bit_targets = to_6_bits_targets(builder, bits_in);
    let converted: Vec<Target> = as_6_bit_targets
        .iter()
        .map(|target| -> Target { convert(builder, *target) })
        .collect();
    let bits_out: Vec<BoolTarget> = converted
        .iter()
        .flat_map(|target| {
            let mut bits = builder.split_le(*target, 8);
            bits.reverse(); // big-endian
            bits
        })
        .collect();

    assert_eq!(base64::converted_len(bits_in.len()), bits_out.len());

    bits_out
}

/// Converts an integer on 6 bits into the ascii encoding of its base64url representation
///
/// `x` - target corresponding to an integer in (0, ..., 63)
pub fn convert(builder: &mut CircuitBuilder<GoldilocksField, D>, x: Target) -> Target {
    // the base64url conversion table consists of five ranges: A-Z, a-z, 0-9, -, _
    // they are handled individually (test of range and multiplication by shifted value)
    // and sumed together

    let lt26 = lower_than(builder, x, 26);
    let plus65 = builder.add_const(x, F::from_canonical_u8(65));
    let term_upper_case = builder.mul(lt26.target, plus65);

    let lt52 = lower_than(builder, x, 52);
    let ge26 = builder.not(lt26);
    let in_range_26_52 = builder.and(lt52, ge26);
    let plus71 = builder.add_const(x, F::from_canonical_u8(71));
    let term_lower_case = builder.mul(in_range_26_52.target, plus71);

    let lt62 = lower_than(builder, x, 62);
    let ge52 = builder.not(lt52);
    let in_range_52_62 = builder.and(lt62, ge52);
    let four = builder.constant(F::from_canonical_u8(4));
    let minus4 = builder.sub(x, four);
    let term_numbers = builder.mul(in_range_52_62.target, minus4);

    let const62 = builder.constant(F::from_canonical_u8(62));
    let eq62 = builder.is_equal(x, const62);
    let const45 = builder.constant(F::from_canonical_u8(45));
    let term_dash = builder.mul(eq62.target, const45);

    let const63 = builder.constant(F::from_canonical_u8(63));
    let eq63 = builder.is_equal(x, const63);
    let const95 = builder.constant(F::from_canonical_u8(95));
    let term_underscore = builder.mul(eq63.target, const95);

    builder.add_many([
        term_upper_case,
        term_lower_case,
        term_numbers,
        term_dash,
        term_underscore,
    ])
}

// c.f. bits_to_bytes
fn to_6_bits_targets(
    builder: &mut CircuitBuilder<GoldilocksField, D>,
    bits_in: &Vec<BoolTarget>,
) -> Vec<Target> {
    let mut padded = bits_in.clone(); // bits need to be padded mod 6
    padded.append(&mut vec![builder._false(); (6 - (bits_in.len() % 6)) % 6]);
    padded
        .chunks_exact(6)
        .map(|chunk| -> Target {
            let powers_of_2: Vec<Target> = chunk
                .iter()
                .zip_eq((0..6).rev()) // power of 2 corresponding to this bit
                .map(|(bit, exp)| {
                    builder.mul_const(F::from_canonical_u8(u8::pow(2, exp)), bit.target)
                })
                .collect();
            builder.add_many(powers_of_2)
        })
        .collect()
}

/// `x` and `y` have to be below 64
fn lower_than(builder: &mut CircuitBuilder<GoldilocksField, D>, x: Target, y: u8) -> BoolTarget {
    // idea: check if x + (64-y) is lower than 64
    assert!(y < 64);
    let diff_to_64: u8 = 64 - y;
    let shift_y_to_64 = builder.add_const(x, F::from_canonical_u8(diff_to_64));
    let bits = builder.split_le(shift_y_to_64, 7);
    builder.not(bits[6]) // last bit not set => lower than 64
}

#[cfg(test)]
mod tests {
    use itertools::Itertools;
    use plonky2::field::types::Field;
    use plonky2::iop::target::BoolTarget;
    use plonky2::{
        iop::witness::{PartialWitness, WitnessWrite},
        plonk::{circuit_builder::CircuitBuilder, circuit_data::CircuitConfig},
    };
    use plonky2_sha256::circuit::array_to_bits;

    use crate::zkp::base64::{convert_bits, converted_len, padded_mod_6, to_6_bits_targets};
    use crate::{C, D, F};

    use super::{convert, lower_than};

    #[test]
    fn test_base64_converted_len() {
        assert_eq!(converted_len(6), 8);
        assert_eq!(converted_len(5), 8);
        assert_eq!(converted_len(4), 8);
        assert_eq!(converted_len(7), 16);
        assert_eq!(converted_len(12), 16);
    }

    #[test]
    fn test_base64_padded_mod_6() {
        assert_eq!(padded_mod_6(6), 6);
        assert_eq!(padded_mod_6(1), 6);
        assert_eq!(padded_mod_6(7), 12);
        assert_eq!(padded_mod_6(12), 12);
    }

    #[test]
    fn test_base64_convert_bits() {
        let input_bytes = String::from("beleniosbeleniosbelenios").as_bytes().to_vec();
        assert_eq!(input_bytes.len(), 24);
        let input_bits = array_to_bits(&input_bytes);

        let output_bytes = base64_url::encode(&input_bytes).into_bytes();
        assert_eq!(output_bytes.len(), 32);
        let output_bits = array_to_bits(&output_bytes);

        helper_convert_bits(input_bits, output_bits);
    }

    #[test]
    fn test_base64_convert_bits_not_mod_6() {
        let input_bytes = String::from("bele").as_bytes().to_vec();
        assert_eq!(input_bytes.len(), 4); // 32 bits, rounded up mod 6 gives 36
        let input_bits = array_to_bits(&input_bytes);

        let output_bytes = base64_url::encode(&input_bytes).into_bytes();
        assert_eq!(output_bytes.len(), 6); // 6 base64 chars
        let output_bits = array_to_bits(&output_bytes);

        helper_convert_bits(input_bits, output_bits);
    }

    fn helper_convert_bits(input_bits: Vec<bool>, output_bits: Vec<bool>) {
        let mut builder =
            CircuitBuilder::<F, D>::new(CircuitConfig::standard_recursion_zk_config());
        let in_targets: Vec<BoolTarget> = input_bits
            .iter()
            .map(|_| builder.add_virtual_bool_target_unsafe())
            .collect();
        let out_targets = convert_bits(&mut builder, &in_targets);
        assert_eq!(out_targets.len(), output_bits.len());

        println!(
            "number of gates for converting {} bits: {}",
            input_bits.len(),
            builder.num_gates()
        );
        let circuit = builder.build::<C>();

        let mut pw = PartialWitness::<F>::new();
        for (value, target) in input_bits.into_iter().zip_eq(in_targets.into_iter()) {
            pw.set_bool_target(target, value);
        }
        for (value, target) in output_bits.into_iter().zip_eq(out_targets.into_iter()) {
            pw.set_bool_target(target, value);
        }

        let proof = circuit.prove(pw).unwrap();
        circuit.verify(proof).unwrap()
    }

    #[test]
    fn test_base64_convert() {
        let mut builder =
            CircuitBuilder::<F, D>::new(CircuitConfig::standard_recursion_zk_config());
        let x1 = builder.add_virtual_target();
        let y1 = convert(&mut builder, x1);
        let x2 = builder.add_virtual_target();
        let y2 = convert(&mut builder, x2);
        let x3 = builder.add_virtual_target();
        let y3 = convert(&mut builder, x3);
        let x4 = builder.add_virtual_target();
        let y4 = convert(&mut builder, x4);
        let x5 = builder.add_virtual_target();
        let y5 = convert(&mut builder, x5);

        println!(
            "number of gates for 5 base64 conversions: {}",
            builder.num_gates()
        );

        let circuit = builder.build::<C>();
        let mut pw = PartialWitness::<F>::new();
        pw.set_target(x1, F::from_canonical_u8(1));
        pw.set_target(y1, F::from_canonical_u8(66));
        pw.set_target(x2, F::from_canonical_u8(26));
        pw.set_target(y2, F::from_canonical_u8(97));
        pw.set_target(x3, F::from_canonical_u8(54));
        pw.set_target(y3, F::from_canonical_u8(50));
        pw.set_target(x4, F::from_canonical_u8(62));
        pw.set_target(y4, F::from_canonical_u8(45));
        pw.set_target(x5, F::from_canonical_u8(63));
        pw.set_target(y5, F::from_canonical_u8(95));

        let proof = circuit.prove(pw).unwrap();
        circuit.verify(proof).unwrap()
    }

    #[test]
    fn test_base64_to_6_bits_targets() {
        let mut builder =
            CircuitBuilder::<F, D>::new(CircuitConfig::standard_recursion_zk_config());
        let x = builder.add_virtual_target();
        let mut x_bits = builder.split_le(x, 6);
        x_bits.reverse(); // big-endian
        let y = to_6_bits_targets(&mut builder, &x_bits);
        assert_eq!(y.len(), 1);

        let circuit = builder.build::<C>();
        let mut pw = PartialWitness::<F>::new();
        pw.set_target(x, F::from_canonical_u8(5));
        pw.set_target(y[0], F::from_canonical_u8(5));

        let proof = circuit.prove(pw).unwrap();
        circuit.verify(proof).unwrap()
    }

    #[test]
    fn test_base64_to_6_bits_targets_not_mod_6() {
        let mut builder =
            CircuitBuilder::<F, D>::new(CircuitConfig::standard_recursion_zk_config());
        let x = builder.add_virtual_target();
        let mut x_bits = builder.split_le(x, 11);
        x_bits.reverse();
        let y = to_6_bits_targets(&mut builder, &x_bits);
        assert_eq!(y.len(), 2);

        let circuit = builder.build::<C>();
        let mut pw = PartialWitness::<F>::new();
        pw.set_target(x, F::from_canonical_u8(66));
        // bits in little-endian are completely reversed => (66 << 1) and split in two 6 bits groups gives 2 and 4
        pw.set_target(y[0], F::from_canonical_u8(2));
        pw.set_target(y[1], F::from_canonical_u8(4));

        let proof = circuit.prove(pw).unwrap();
        circuit.verify(proof).unwrap()
    }

    #[test]
    fn test_base64_lower_than() {
        let mut builder =
            CircuitBuilder::<F, D>::new(CircuitConfig::standard_recursion_zk_config());
        let x = builder.add_virtual_target();
        let lt26 = lower_than(&mut builder, x, 26);
        let lt25 = lower_than(&mut builder, x, 25);
        builder.register_public_input(lt26.target);
        builder.register_public_input(lt25.target);

        let circuit = builder.build::<C>();
        let mut pw = PartialWitness::<F>::new();
        pw.set_target(x, F::from_canonical_u8(25));
        pw.set_bool_target(lt26, true);
        pw.set_bool_target(lt25, false);

        let proof = circuit.prove(pw).unwrap();
        circuit.verify(proof).unwrap()
    }
}
