use plonky2::field::types::Field;
use plonky2::iop::witness::{PartialWitness, WitnessWrite};
use plonky2_sha256::circuit::array_to_bits;

use crate::zkp::merkle::fill_circuit_with_merkle;
use crate::{Circuit, Zkproof, F};

use super::{TargetValues, Targets};

/// Generate the zkp for the given circuit, targets and target values
pub fn generate_zkp(
    circuit: &Circuit,
    targets: Targets,
    values: TargetValues,
) -> Result<Zkproof, anyhow::Error> {
    let mut pw = PartialWitness::<F>::new();
    // set jwt body
    for i in 0..values.jwt_body.len() {
        pw.set_target(
            targets.jwt_body[i],
            F::from_canonical_u8(values.jwt_body[i]),
        );
    }
    // set jwt header and '.'
    for i in 0..values.jwt_header_b64.len() {
        pw.set_bool_target(targets.jwt_header_b64[i], values.jwt_header_b64[i]);
    }
    let dot_bits = array_to_bits(&['.' as u8]);
    assert_eq!(dot_bits.len(), 8);
    for i in 0..8 {
        pw.set_bool_target(
            targets.jwt_header_b64[values.jwt_header_b64.len() + i],
            dot_bits[i],
        );
    }
    // set sub
    for i in 0..values.sub.len() {
        pw.set_target(targets.sub[i], F::from_canonical_u8(values.sub[i]));
    }
    // set aud
    for i in 0..values.aud.len() {
        pw.set_target(targets.aud[i], F::from_canonical_u8(values.aud[i]));
    }
    // set sha256 digest
    for i in 0..values.digest.len() {
        pw.set_bool_target(targets.jwt_digest[i], values.digest[i]);
    }
    // set poseidon targets
    for i in 0..values.ns.len() {
        pw.set_target(targets.ns_nv[i], F::from_canonical_u8(values.ns[i]));
    }
    for i in 0..values.nv.len() {
        let target = targets.ns_nv[i + values.ns.len()];
        pw.set_target(target, F::from_canonical_u8(values.nv[i]));
    }
    // set merkle targets
    fill_circuit_with_merkle(
        &mut pw,
        targets.merkle_leaf,
        targets.merkle_witness.siblings,
        targets.merkle_cap,
        targets.merkle_leaf_index,
        &values.merkle_leaf,
        values.merkle_leaf_index,
        values.merkle_witness.siblings,
        &values.merkle_cap,
    );

    let proof = circuit.prove(pw)?;

    Ok(proof)
}

#[cfg(test)]
mod test {
    use crate::{mock::zkp::mock_circuit_and_targets, zkp::TargetValues};
    use plonky2_field::types::Field64;
    use strum::IntoEnumIterator;
    use strum_macros::{EnumIter, IntoStaticStr};
    use FieldToModify::*;
    use PartToModify::*;

    use super::generate_zkp;

    #[test]
    fn test_zkp_generate_zkp_ok() {
        let (_, circuit, targets, values) = mock_circuit_and_targets(42).unwrap();
        let proof = generate_zkp(&circuit, targets, values).unwrap();
        circuit.verify(proof).unwrap()
    }
    #[test]
    fn test_zkp_generate_zkp_fail() {
        // all scenarios in series, could write a macro to have separate tests
        for field in FieldToModify::iter() {
            for part in PartToModify::iter() {
                let field_str: &'static str = field.into();
                let part_str: &'static str = part.into();
                println!("test with modified [{}] [{}]", field_str, part_str);
                let result = std::panic::catch_unwind(|| {
                    let (_, circuit, targets, values) = mock_circuit_and_targets(42).unwrap();
                    let modified_values = modify_values(values, (field, part)).unwrap();
                    generate_zkp(&circuit, targets, modified_values)
                });
                assert!(result.is_err());
            }
        }
    }

    #[derive(EnumIter, Clone, Copy, IntoStaticStr)]
    pub enum FieldToModify {
        Header,
        Body,
        Digest,
        Ns,
        Nv,
        MerkleLeaf,
    }
    #[derive(EnumIter, Clone, Copy, IntoStaticStr)]
    pub enum PartToModify {
        Start,
        End,
    }

    pub fn modify_values(
        values: TargetValues,
        to_modify: (FieldToModify, PartToModify),
    ) -> Result<TargetValues, anyhow::Error> {
        let mut ns_prime = values.ns.clone();
        let mut nv_prime = values.nv.clone();
        let mut digest_prime = values.digest.clone();
        let mut body_prime = values.jwt_body.clone();
        let mut header_prime = values.jwt_header_b64.clone();
        let mut leaf_prime = values.merkle_leaf.clone();
        match to_modify {
            (Ns, Start) => ns_prime[0] ^= 1,
            (Ns, End) => ns_prime[values.ns.len() - 1] ^= 1,
            (Nv, Start) => nv_prime[0] ^= 1,
            (Nv, End) => nv_prime[values.nv.len() - 1] ^= 1,
            (Digest, Start) => digest_prime[0] ^= true,
            (Digest, End) => digest_prime[values.digest.len() - 1] ^= true,
            (Body, Start) => body_prime[0] ^= 1,
            (Body, End) => body_prime[values.jwt_body.len() - 1] ^= 1,
            (Header, Start) => header_prime[0] ^= true,
            (Header, End) => header_prime[values.jwt_header_b64.len() - 1] ^= true,
            (MerkleLeaf, Start) => leaf_prime[0] = leaf_prime[0].add_one(),
            (MerkleLeaf, End) => {
                leaf_prime[values.merkle_leaf.len() - 1] =
                    leaf_prime[leaf_prime.len() - 1].add_one()
            }
        };

        let modified = TargetValues {
            jwt_header_b64: header_prime,
            jwt_body: body_prime,
            digest: digest_prime,
            sub: values.sub,
            aud: values.aud,
            ns: ns_prime,
            nv: nv_prime,
            merkle_witness: values.merkle_witness,
            merkle_leaf_index: values.merkle_leaf_index,
            merkle_cap: values.merkle_cap,
            merkle_leaf: leaf_prime,
        };

        Ok(modified)
    }
}
