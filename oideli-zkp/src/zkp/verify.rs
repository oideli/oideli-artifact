use crate::Zkproof;

use super::{circuit::build_circuit, CircuitSizing};

pub fn verify_zkp(circuit_sizing: CircuitSizing, proof: Zkproof) -> Result<(), anyhow::Error> {
    let (circuit, _) = build_circuit(&circuit_sizing)?;
    circuit.verify(proof)
}

#[cfg(test)]
mod test {

    use strum::IntoEnumIterator;
    use strum_macros::{EnumIter, IntoStaticStr};

    use super::verify_zkp;
    use crate::{
        mock::zkp::mock_circuit_and_targets,
        zkp::{generate_zkp, CircuitSizing},
    };

    #[test]
    fn test_verify_zkp_ok() {
        let (circuit_sizing, circuit, targets, values) = mock_circuit_and_targets(39).unwrap();
        let proof = generate_zkp(&circuit, targets, values).unwrap();
        verify_zkp(circuit_sizing, proof).unwrap()
    }

    #[test]
    fn test_verify_zkp_fail() {
        // all scenarios in series, could write a macro to have separate tests
        for field in FieldToModify::iter() {
            for shift in [-1, 1] {
                let field_str: &'static str = field.into();
                println!("test with [{}] shifted by [{}]", field_str, shift);
                let result = std::panic::catch_unwind(|| {
                    let (sizing, circuit, targets, values) = mock_circuit_and_targets(42).unwrap();
                    let proof = generate_zkp(&circuit, targets, values).unwrap();
                    let modified_sizing = modify_sizing(sizing, field, shift);
                    verify_zkp(modified_sizing, proof).unwrap()
                });
                assert!(result.is_err());
            }
        }
    }

    #[derive(EnumIter, Clone, Copy, IntoStaticStr)]
    pub enum FieldToModify {
        Header,
        Body,
        MerkleDepth,
        IdIndex,
        NonceIndex,
        SubIndex,
        SubLen,
    }

    use FieldToModify::*;
    fn modify_sizing(sizing: CircuitSizing, field: FieldToModify, shift: i32) -> CircuitSizing {
        match field {
            Header => CircuitSizing {
                jwt_header_b64_bits: ((sizing.jwt_header_b64_bits as i32) + shift)
                    .try_into()
                    .unwrap(),
                ..sizing
            },
            Body => CircuitSizing {
                jwt_body: ((sizing.jwt_body as i32) + shift).try_into().unwrap(),
                ..sizing
            },
            MerkleDepth => CircuitSizing {
                merkle_depth: ((sizing.merkle_depth as i32) + shift).try_into().unwrap(),
                ..sizing
            },
            IdIndex => CircuitSizing {
                id_min_index: ((sizing.id_min_index as i32) + shift).try_into().unwrap(),
                ..sizing
            },
            NonceIndex => CircuitSizing {
                nonce_min_index: ((sizing.nonce_min_index as i32) + shift)
                    .try_into()
                    .unwrap(),
                ..sizing
            },
            SubIndex => CircuitSizing {
                sub_min_index: ((sizing.sub_min_index as i32) + shift).try_into().unwrap(),
                ..sizing
            },
            SubLen => CircuitSizing {
                sub_len: ((sizing.sub_len as i32) + shift).try_into().unwrap(),
                ..sizing
            },
        }
    }
}
