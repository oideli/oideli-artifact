use plonky2::hash::hash_types::MerkleCapTarget;
use plonky2::hash::merkle_proofs::{MerkleProof, MerkleProofTarget};
use plonky2::hash::merkle_tree::MerkleCap;
use plonky2::hash::poseidon::PoseidonHash;
use plonky2::iop::target::{BoolTarget, Target};
use plonky2_field::goldilocks_field::GoldilocksField;
use serde::{Deserialize, Serialize};

mod base64;
mod circuit;
mod circuit_builder_transmuted;
mod generate;
mod lookup;
mod merkle;
mod poseidon;
mod public_inputs;
mod sha256;
mod verify;

pub use circuit::build_circuit;
pub use generate::generate_zkp;
pub use public_inputs::verify_public_inputs;
pub use public_inputs::PublicInputValues;
pub use verify::verify_zkp;

/// Lengths and indexes of data needed to build a circuit
///
/// This struct represents the "blueprint" of a circuit, i.e. it is the only necessary data to build a circuit
///
/// The indexes are the ones for an id of minimal length (the circuit takes care of checking possible lengths), thus designated as "min"
#[derive(Serialize, Deserialize, Clone)]
pub struct CircuitSizing {
    pub jwt_header_b64_bits: usize,
    pub jwt_body: usize,
    pub merkle_depth: usize,
    pub id_min_index: usize,
    pub nonce_min_index: usize,
    pub sub_min_index: usize,
    pub sub_len: usize,
    pub aud_min_index: usize,
    pub aud_len: usize,
    pub is_nonce_after_id: bool,
    pub is_sub_between_id_nonce: bool,
}

/// Targets of a circuit grouped in one struct
///
/// A target is a "wire" of the circuit. The targets in this struct are the input wires,
/// thus they need to be set to generate a zkp.
#[derive(Clone)]
pub struct Targets {
    jwt_body: Vec<Target>,
    jwt_header_b64: Vec<BoolTarget>,
    jwt_digest: Vec<BoolTarget>,
    sub: Vec<Target>,
    aud: Vec<Target>,
    ns_nv: Vec<Target>,
    merkle_witness: MerkleProofTarget,
    merkle_leaf: Vec<Target>,
    merkle_leaf_index: Target,
    merkle_cap: MerkleCapTarget,
}

/// Values to assign to input targets
///
/// Values corresponding to the targets of a `Targets` struct, to be assigned to generate a zkp.
pub struct TargetValues {
    pub jwt_header_b64: Vec<bool>,
    pub jwt_body: Vec<u8>,
    pub digest: Vec<bool>,
    pub sub: Vec<u8>,
    pub aud: Vec<u8>,
    pub ns: Vec<u8>,
    pub nv: Vec<u8>,
    pub merkle_witness: MerkleProof<GoldilocksField, PoseidonHash>,
    pub merkle_leaf_index: usize,
    pub merkle_cap: MerkleCap<GoldilocksField, PoseidonHash>,
    pub merkle_leaf: Vec<GoldilocksField>,
}
