use plonky2::hash::poseidon::PoseidonHash;

use plonky2::iop::target::Target;
use plonky2::plonk::circuit_builder::CircuitBuilder;

use plonky2_field::goldilocks_field::GoldilocksField;

use crate::D;

pub fn add_poseidon_gate(
    builder: &mut CircuitBuilder<GoldilocksField, D>,
    number_inputs: usize,
) -> (Vec<Target>, [Target; 4]) {
    let inputs = builder.add_virtual_targets(number_inputs);
    let hashed = builder.hash_n_to_hash_no_pad::<PoseidonHash>(inputs.clone());
    (inputs, hashed.elements)
}

#[cfg(test)]
mod test {
    use plonky2::hash::poseidon::PoseidonHash;
    use rand::Rng;

    use plonky2::field::types::Field;
    use plonky2::iop::witness::PartialWitness;
    use plonky2::iop::witness::WitnessWrite;
    use plonky2::plonk::circuit_builder::CircuitBuilder;
    use plonky2::plonk::circuit_data::CircuitConfig;

    use crate::poseidon::hashout_to_bits;
    use crate::poseidon::poseidon_hash;
    use crate::zkp::circuit_builder_transmuted::CircuitBuilderPrime;
    use crate::zkp::poseidon::add_poseidon_gate;
    use crate::C;
    use crate::D;
    use crate::F;

    #[test]
    fn test_poseidon_gate_ok() {
        let mut rng = rand::thread_rng();
        let ns: Vec<u8> = (0..64).map(|_v| rng.gen()).collect();
        let nv: Vec<u8> = (0..64).map(|_v| rng.gen()).collect();

        let n_f = poseidon_hash(&ns, &nv);
        let n_f_bits = hashout_to_bits(&n_f);

        let mut builder =
            CircuitBuilder::<F, D>::new(CircuitConfig::standard_recursion_zk_config());
        let mut pw = PartialWitness::<F>::new();

        let (ns_nv_t, n_hashed) = add_poseidon_gate(&mut builder, ns.len() + nv.len());
        for i in 0..ns.len() {
            pw.set_target(ns_nv_t[i], F::from_canonical_u8(ns[i]));
        }
        for i in 0..nv.len() {
            pw.set_target(ns_nv_t[i + ns.len()], F::from_canonical_u8(nv[i]));
        }
        for i in 0..4 {
            pw.set_target(n_hashed[i], n_f[i])
        }
        for i in 0..4 {
            let chibi_outputs = builder.split_le(n_hashed[i], 64);
            for j in 0..64 {
                pw.set_bool_target(chibi_outputs[j], n_f_bits[(i * 64) + (63 - j)])
            }
        }

        println!("number of gates: {}", builder.num_gates());
        let data = builder.build::<C>();

        data.prove(pw).unwrap();
    }

    #[test]
    #[should_panic]
    fn test_poseidon_gate_panic() {
        let mut rng = rand::thread_rng();
        let ns: Vec<u8> = (0..64).map(|_v| rng.gen()).collect();
        let nv: Vec<u8> = (0..64).map(|_v| rng.gen()).collect();

        let n_f = poseidon_hash(&ns, &nv);
        let mut n_f_bits = hashout_to_bits(&n_f);
        n_f_bits[3] = !n_f_bits[3];

        let mut builder =
            CircuitBuilder::<F, D>::new(CircuitConfig::standard_recursion_zk_config());
        let mut pw = PartialWitness::<F>::new();

        let ns_nv_t = builder.add_virtual_targets(ns.len() + nv.len());

        for i in 0..ns.len() {
            pw.set_target(ns_nv_t[i], F::from_canonical_u8(ns[i]));
        }
        for i in 0..nv.len() {
            pw.set_target(ns_nv_t[i + ns.len()], F::from_canonical_u8(nv[i]));
        }
        let n_hashed = builder.hash_n_to_hash_no_pad::<PoseidonHash>(ns_nv_t);
        for i in 0..4 {
            pw.set_target(n_hashed.elements[i], n_f[i])
        }
        for i in 0..4 {
            let chibi_outputs = builder.split_le(n_hashed.elements[i], 64);
            for j in 0..64 {
                pw.set_bool_target(chibi_outputs[j], n_f_bits[(63 - j) + (i * 64)])
            }
        }

        let data = builder.build::<C>();

        data.prove(pw).unwrap();
    }

    #[test]
    fn test_poseidon_ops_count() {
        let mut builder =
            CircuitBuilder::<F, D>::new(CircuitConfig::standard_recursion_zk_config());
        let _ = add_poseidon_gate(&mut builder, 64);

        println!("number of gates: {}", builder.num_gates());

        // hackyyy
        // https://blog.knoldus.com/safe-way-to-access-private-fields-in-rust/
        let builder_exposed: CircuitBuilderPrime<F, D> = unsafe { std::mem::transmute(builder) };

        for gate in builder_exposed.gates.iter().cloned() {
            let count = builder_exposed
                .gate_instances
                .iter()
                .filter(|inst| inst.gate_ref == gate)
                .count();
            println!("- {} instances of {}", count, gate.0.id());
            println!("  - num ops: {}", gate.0.num_ops());
            println!("  - num wires: {}", gate.0.num_wires());
            println!("  - num constraints: {}", gate.0.num_constraints());
            println!("  - num constants: {}", gate.0.num_constants());
        }
    }
}
