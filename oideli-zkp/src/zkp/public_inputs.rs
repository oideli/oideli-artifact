use plonky2::{
    iop::target::BoolTarget, iop::target::Target, plonk::circuit_builder::CircuitBuilder,
};
use plonky2_sha256::circuit::array_to_bits;
use serde::{Deserialize, Serialize};
use serde_with::serde_as;

use crate::{D, F, HASH_SIZE_BYTES};

const DIGEST_BITS_LEN: usize = HASH_SIZE_BYTES * 8;
const MERKLE_CAP_LEN: usize = 4;

pub struct PublicInputTargets {
    pub digest: [BoolTarget; DIGEST_BITS_LEN],
    pub nv: [Target; HASH_SIZE_BYTES],
    pub merkle_cap: [Target; MERKLE_CAP_LEN],
    pub sub: Vec<Target>,
    pub aud: Vec<Target>,
}

#[serde_as]
#[derive(Serialize, Deserialize, Clone)]
pub struct PublicInputValues {
    /// The result of the hash of the jwt
    #[serde_as(as = "[_; HASH_SIZE_BYTES]")]
    pub digest: [u8; HASH_SIZE_BYTES],
    /// The public part of the preimage of the nonce N
    #[serde_as(as = "[_; HASH_SIZE_BYTES]")]
    pub nv: [u8; HASH_SIZE_BYTES],
    /// The root of the merkle tree containing eligible voters
    #[serde_as(as = "[_; MERKLE_CAP_LEN]")]
    pub merkle_cap: [F; MERKLE_CAP_LEN],
    /// The sub field of the jwt
    pub sub: Vec<u8>,
    /// The aud field of the jwt
    pub aud: Vec<u8>,
}

pub fn register_public_inputs(builder: &mut CircuitBuilder<F, D>, targets: PublicInputTargets) {
    for target in targets.digest {
        builder.register_public_input(target.target)
    }
    for target in targets.nv.into_iter() {
        builder.register_public_input(target);
    }
    for target in targets.merkle_cap.into_iter() {
        builder.register_public_input(target);
    }
    for target in targets.sub {
        builder.register_public_input(target)
    }
    for target in targets.aud {
        builder.register_public_input(target)
    }
}

pub fn verify_public_inputs(inputs: Vec<F>, values: PublicInputValues) {
    // start indexes
    let nv_start_index = DIGEST_BITS_LEN;
    let merkle_cap_start_index = nv_start_index + HASH_SIZE_BYTES;
    let sub_start_index = merkle_cap_start_index + MERKLE_CAP_LEN;
    let aud_start_index = sub_start_index + values.sub.len();

    // verify that the jwt digest is the same as the one in the zkp
    let digest_bits = array_to_bits(values.digest.as_slice());
    for i in 0..digest_bits.len() {
        assert_eq!(
            (inputs[i].0 == 1),
            digest_bits[i],
            "digest does not match at index [{}]",
            i
        );
    }
    // verify the public preimage of the nonce (nv)
    for i in 0..HASH_SIZE_BYTES {
        assert_eq!(
            inputs[nv_start_index + i].0 as u8,
            values.nv[i],
            "nv does not match at index [{}]",
            i
        );
    }
    // verify that the cap (root) of the merkle tree in the zkp is the same as the public one
    for i in 0..4 {
        assert_eq!(
            inputs[merkle_cap_start_index + i],
            values.merkle_cap[i],
            "merkle cap does not match at index [{}]",
            i
        )
    }
    // verify the sub field of the jwt
    for i in 0..values.sub.len() {
        assert_eq!(
            inputs[sub_start_index + i].0 as u8,
            values.sub[i],
            "sub does not match at index [{}]",
            i
        );
    }
    // verify the aud field of the jwt
    for i in 0..values.aud.len() {
        assert_eq!(
            inputs[aud_start_index + i].0 as u8,
            values.aud[i],
            "aud does not match at index [{}]",
            i
        );
    }
    // verify length of public inputs
    assert_eq!(
        inputs.len(),
        aud_start_index + values.aud.len(),
        "length of public inputs array does not match"
    );
}

#[cfg(test)]
mod test {
    use crate::{C, D, F};
    use plonky2::field::types::Field;
    use plonky2::iop::witness::PartialWitness;
    use plonky2::{
        iop::target::{BoolTarget, Target},
        plonk::{circuit_builder::CircuitBuilder, circuit_data::CircuitConfig},
    };
    use plonky2_field::types::Field64;

    use super::{
        register_public_inputs, verify_public_inputs, PublicInputTargets, PublicInputValues,
    };

    #[test]
    fn test_public_inputs_ok() {
        let mut builder =
            CircuitBuilder::<F, D>::new(CircuitConfig::standard_recursion_zk_config());
        let (targets, values) = test_public_inputs(&mut builder);

        register_public_inputs(&mut builder, targets);

        let circuit = builder.build::<C>();
        let pw = PartialWitness::<F>::new();
        let proof = circuit.prove(pw).unwrap();

        verify_public_inputs(proof.public_inputs, values);
    }

    /// public inputs verification should fail when
    /// - digest, nv, cap or sub, aud is modified
    /// - length of sub or aud is modified (other fields have fixed lengths)
    enum FieldToModify {
        Digest,
        Nv,
        Cap,
        Sub,
        SubLen,
        Aud,
        AudLen,
    }

    #[test]
    #[should_panic]
    fn test_public_inputs_wrong_digest() {
        test_public_inputs_modified(FieldToModify::Digest)
    }
    #[test]
    #[should_panic]
    fn test_public_inputs_wrong_nv() {
        test_public_inputs_modified(FieldToModify::Nv)
    }
    #[test]
    #[should_panic]
    fn test_public_inputs_wrong_cap() {
        test_public_inputs_modified(FieldToModify::Cap)
    }
    #[test]
    #[should_panic]
    fn test_public_inputs_wrong_sub() {
        test_public_inputs_modified(FieldToModify::Sub)
    }
    #[test]
    #[should_panic]
    fn test_public_inputs_wrong_sub_len() {
        test_public_inputs_modified(FieldToModify::SubLen)
    }
    #[test]
    #[should_panic]
    fn test_public_inputs_wrong_aud() {
        test_public_inputs_modified(FieldToModify::Aud)
    }
    #[test]
    #[should_panic]
    fn test_public_inputs_wrong_aud_len() {
        test_public_inputs_modified(FieldToModify::AudLen)
    }

    fn test_public_inputs_modified(to_modify: FieldToModify) {
        let mut builder =
            CircuitBuilder::<F, D>::new(CircuitConfig::standard_recursion_zk_config());
        let (targets, values) = test_public_inputs(&mut builder);
        register_public_inputs(&mut builder, targets);
        let circuit = builder.build::<C>();
        let pw = PartialWitness::<F>::new();
        let proof = circuit.prove(pw).unwrap();

        let wrong_values = match to_modify {
            FieldToModify::Digest => PublicInputValues {
                digest: {
                    let mut d = values.clone().digest;
                    d[0] ^= 1;
                    d
                },
                ..values
            },
            FieldToModify::Nv => PublicInputValues {
                nv: {
                    let mut nv = values.clone().nv;
                    nv[0] ^= 1;
                    nv
                },
                ..values
            },
            FieldToModify::Cap => PublicInputValues {
                merkle_cap: {
                    let mut cap = values.clone().merkle_cap;
                    cap[0] = cap[0].add_one();
                    cap
                },
                ..values
            },
            FieldToModify::Sub => PublicInputValues {
                sub: {
                    let mut s = values.clone().sub;
                    s[0] ^= 1;
                    s
                },
                ..values
            },
            FieldToModify::SubLen => PublicInputValues {
                sub: {
                    let mut s = values.clone().sub;
                    s.push(0);
                    s
                },
                ..values
            },
            FieldToModify::Aud => PublicInputValues {
                aud: {
                    let mut s = values.clone().aud;
                    s[0] ^= 1;
                    s
                },
                ..values
            },
            FieldToModify::AudLen => PublicInputValues {
                sub: {
                    let mut s = values.clone().aud;
                    s.push(0);
                    s
                },
                ..values
            },
        };
        verify_public_inputs(proof.public_inputs, wrong_values);
    }

    fn test_public_inputs(
        builder: &mut CircuitBuilder<F, D>,
    ) -> (PublicInputTargets, PublicInputValues) {
        let digest: [u8; 32] = (0..32)
            .into_iter()
            .map(|i| i as u8)
            .collect::<Vec<u8>>()
            .try_into()
            .unwrap();
        let digest_targets: [BoolTarget; 256] = digest
            .into_iter()
            .flat_map(|b| {
                let t = builder.add_virtual_target();
                let b_target = builder.constant(F::from_canonical_u8(b));
                builder.connect(t, b_target);
                let mut bits = builder.split_le(t, 8);
                bits.reverse(); // big endian
                bits
            })
            .collect::<Vec<BoolTarget>>()
            .as_slice()
            .try_into()
            .unwrap();
        let nv: [u8; 32] = (0..32)
            .into_iter()
            .map(|i| i as u8)
            .collect::<Vec<u8>>()
            .try_into()
            .unwrap();
        let nv_targets: [Target; 32] = nv.map(|b| {
            let t = builder.add_virtual_target();
            let b_target = builder.constant(F::from_canonical_u8(b));
            builder.connect(t, b_target);
            t
        });
        let merkle_cap: [F; 4] = (0..4)
            .into_iter()
            .map(|i| F::from_canonical_u8(i))
            .collect::<Vec<F>>()
            .try_into()
            .unwrap();
        let merkle_cap_targets: [Target; 4] = merkle_cap.map(|f| {
            let t = builder.add_virtual_target();
            let f_target = builder.constant(f);
            builder.connect(t, f_target);
            t
        });
        let sub: Vec<u8> = (0..42).into_iter().map(|i| i as u8).collect();
        let sub_targets: Vec<Target> = sub
            .clone()
            .into_iter()
            .map(|b| {
                let t = builder.add_virtual_target();
                let b_target = builder.constant(F::from_canonical_u8(b));
                builder.connect(t, b_target);
                t
            })
            .collect();
        let aud: Vec<u8> = (0..48).into_iter().map(|i| i as u8).collect();
        let aud_targets: Vec<Target> = aud
            .clone()
            .into_iter()
            .map(|b| {
                let t = builder.add_virtual_target();
                let b_target = builder.constant(F::from_canonical_u8(b));
                builder.connect(t, b_target);
                t
            })
            .collect();

        let targets = PublicInputTargets {
            digest: digest_targets,
            nv: nv_targets,
            merkle_cap: merkle_cap_targets,
            sub: sub_targets,
            aud: aud_targets,
        };
        let values = PublicInputValues {
            digest,
            nv,
            merkle_cap,
            sub,
            aud,
        };

        (targets, values)
    }
}
