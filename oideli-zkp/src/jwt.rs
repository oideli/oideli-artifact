use std::fs::File;
use std::io::prelude::*;

use anyhow::bail;
use sha2::{Digest, Sha256};

use crate::{signature::Kid, MAX_ID_LEN};

/// Container for parsed JWT parts
#[derive(Clone)]
pub struct Jwt {
    /// Header in base64
    pub header_b64: Vec<u8>,
    /// Body in json
    pub body: Vec<u8>,
    /// Digest of <header>.<body>
    pub digest: Vec<u8>,
    /// Signature of the digest
    pub signature: Vec<u8>,
    /// Id of the verification key
    pub kid: Kid,
}

/// Reads and parses a jwt from the given file
pub fn get_jwt(filename: &str) -> Result<Jwt, anyhow::Error> {
    let mut jwt_file = File::open(filename)?;
    let mut jwt_string = String::new();
    jwt_file.read_to_string(&mut jwt_string)?;

    parse_jwt(jwt_string)
}

/// Parses jwt
pub fn parse_jwt(mut jwt_string: String) -> Result<Jwt, anyhow::Error> {
    let header_b64: String = jwt_string.drain(..jwt_string.find('.').unwrap()).collect();
    jwt_string.remove(0);
    let body_b64: String = jwt_string.drain(..jwt_string.find('.').unwrap()).collect();
    jwt_string.remove(0);
    let sig_b64: String = jwt_string.trim().to_owned(); // rest of the jwt

    let body_bytes = base64_url::decode(&body_b64)?;
    let body = String::from_utf8(body_bytes)?.into_bytes();

    let digest = {
        let mut sha256_input_bytes = Vec::new();
        sha256_input_bytes.append(&mut header_b64.clone().into_bytes());
        sha256_input_bytes.push(46); // 46 is the ascii for the '.' caracter.
        sha256_input_bytes.append(&mut body_b64.into_bytes());
        Sha256::digest(sha256_input_bytes).to_vec()
    };

    let header_bytes = base64_url::decode(&header_b64)?;
    let header = String::from_utf8(header_bytes)?;
    let (_, kid) = find_field(&header, "kid")?;

    let signature = base64_url::decode(&sig_b64)?;

    Ok(Jwt {
        header_b64: header_b64.into_bytes(),
        body,
        digest,
        signature,
        kid,
    })
}

/// Pads nonce s.t. size of jwt does not leak size of id
pub fn padded_nonce_b64(nonce: &str, unpadded_id: &str) -> String {
    let padding = ".".repeat(MAX_ID_LEN - unpadded_id.len());
    nonce.to_owned() + &padding
}
/// Trim the padding of a nonce
pub fn trim_nonce_b64_padding(padded: &str) -> &str {
    padded.trim_end_matches('.')
}

/// Pads the id with spaces to reach the max len
pub fn padded_id(email: &str) -> String {
    let padding = " ".repeat(MAX_ID_LEN - email.len());
    email.to_owned() + &padding
}

/// Get the index of the beginning of the "email" field and its content.
pub fn find_email(body: &str) -> Result<(usize, String), anyhow::Error> {
    find_field(body, "email")
}

/// Get the index of the beginning of the "nonce" field and its content, without any padding.
pub fn find_nonce(body: &str) -> Result<(usize, String), anyhow::Error> {
    find_field(body, "nonce")
}

/// Get the index of the "sub" field and its content.
pub fn find_sub(body: &str) -> Result<(usize, String), anyhow::Error> {
    find_field(body, "sub")
}

/// Get the index of the "aud" field and its content.
pub fn find_aud(body: &str) -> Result<(usize, String), anyhow::Error> {
    find_field(body, "aud")
}

/// Get the index of a field as well as its content.
fn find_field(json: &str, fieldname: &str) -> Result<(usize, String), anyhow::Error> {
    let field_index = find_index_after(&json, &format!(r#""{}":""#, fieldname));
    field_index.and_then(|start| {
        let field_len_plus_one = find_index_after(&json[start..], "\"");
        field_len_plus_one
            .map(|len_plus_one| (start, json[start..(start + len_plus_one - 1)].to_owned()))
    })
}

fn find_index_after(str: &str, target: &str) -> Result<usize, anyhow::Error> {
    match str.find(target) {
        Some(n) => Ok(n + target.len()),
        None => bail!(format!("jwt is malformed, there is no [{}] claim", target)),
    }
}

#[cfg(test)]
mod tests {
    use crate::jwt::find_aud;

    use super::{find_email, find_nonce, find_sub};

    #[test]
    fn test_jwt_find_email_ok() {
        let (index, email) = find_email(r#"{"nonce":"one","email":"three"}"#).unwrap();
        assert_eq!(index, 24);
        assert_eq!(email, "three".to_owned());
    }

    #[test]
    #[should_panic]
    fn test_jwt_find_email_fail() {
        find_email(r#"{"nonce":"one","sub":"three"}"#).unwrap();
    }

    #[test]
    fn test_jwt_find_nonce() {
        let (index, nonce) = find_nonce(r#"{"nonce":"one.....","email":"three"}"#).unwrap();
        assert_eq!(index, 10);
        assert_eq!(nonce, "one.....".to_owned());
    }

    #[test]
    fn test_jwt_find_sub() {
        let (index, sub) = find_sub(r#"{"nonce":"one","sub":"three"}"#).unwrap();
        assert_eq!(index, 22);
        assert_eq!(sub, "three".to_owned());
    }

    #[test]
    fn test_jwt_find_aud() {
        let (index, aud) = find_aud(r#"{"aud":"one","sub":"three"}"#).unwrap();
        assert_eq!(index, 8);
        assert_eq!(aud, "one".to_owned());
    }
}
