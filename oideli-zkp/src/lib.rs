//! A library that implements zero knowledge proofs for the OIDEli protocol.
#![warn(missing_docs)]
#![feature(slice_flatten)]
use std::usize;

use plonky2::hash::merkle_tree::MerkleTree;
use plonky2::hash::poseidon::PoseidonHash;
use plonky2::plonk::circuit_data::CircuitData;
use plonky2::plonk::config::GenericConfig;
use plonky2::plonk::config::PoseidonGoldilocksConfig;
use plonky2::plonk::proof::ProofWithPublicInputs;
use plonky2_field::goldilocks_field::GoldilocksField;
use serde::{Deserialize, Serialize};

use signature::Kid;
use zkp::PublicInputValues;

/// Contains tools to read a manipulate jwt and jsons
pub mod jwt;
/// Contains tools to build merkle trees for the proofs
pub mod merkle;
/// Contains the poseidon hash function and helpers
pub mod poseidon;
/// Allows to generate proofs
pub mod prove;
/// Allows verification of RSA signatures
mod signature;
/// Allows to verify proofs
pub mod verify;

/// Contains all the plonky2 wiring to generate and verify proofs
mod zkp;
pub use zkp::build_circuit;

/// Mock data for tests and benches
pub mod mock;

const D: usize = 2;
type C = PoseidonGoldilocksConfig;
type F = <C as GenericConfig<D>>::F;

type Zkproof = ProofWithPublicInputs<GoldilocksField, PoseidonGoldilocksConfig, D>;
type Circuit = CircuitData<F, C, D>;
type IdEl = MerkleTree<GoldilocksField, PoseidonHash>;

// global constants
const HASH_SIZE_BYTES: usize = 32;
const MAX_ID_LEN: usize = 50;
const ID_SUFFIX: &str = "@gmail.com";
const ID_SUFFIX_LEN: usize = ID_SUFFIX.len();
const MIN_ID_LEN: usize = ID_SUFFIX.len() + 1;
const MAX_LOOKUP_OFFSET: usize = MAX_ID_LEN - MIN_ID_LEN;
const NONCE_BYTES_LEN: usize = 32;
const NONCE_BITS_LEN: usize = NONCE_BYTES_LEN * 8;
const NONCE_B64_BYTES_LEN: usize = 44;
const NONCE_B64_BITS_LEN: usize = NONCE_B64_BYTES_LEN * 8;

/// This struct contains what needs to be published on the bulleting board.
#[derive(Serialize, Deserialize, Clone)]
pub struct EligibilityProof {
    /// The zero knowledge part.
    pub zkp: Zkproof,
    /// The values for the public inputs of this proof
    pub public_inputs: PublicInputValues,
    /// The signature of the jwt
    pub signature: Vec<u8>,
    /// The id of the key to verify the signature
    pub kid: Kid,
}
