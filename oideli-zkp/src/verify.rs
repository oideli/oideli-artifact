use itertools::Either;
use rsa::RsaPublicKey;

use crate::signature::{get_verifying_key, verify_signature, Kid};
use crate::zkp::{verify_public_inputs, CircuitSizing};
use crate::{zkp::verify_zkp, EligibilityProof, IdEl};

/// Verify if the proof is correct, if the public inputs are matching the expected ones and if the signature is valid.
pub fn verify_full_proof(
    proof: EligibilityProof,
    circuit_sizing: CircuitSizing,
    idel: IdEl,
    verifying_key: Either<RsaPublicKey, &Kid>,
    skip_jwks_request: bool,
    skip_signature_verification: bool,
) -> Result<(), anyhow::Error> {
    if !skip_signature_verification {
        // verify signature
        let key = match verifying_key {
            Either::Left(key) => key,
            Either::Right(kid) => get_verifying_key(
                kid,
                "https://www.googleapis.com/oauth2/v3/certs",
                skip_jwks_request,
            )?,
        };
        verify_signature(&proof.public_inputs.digest.to_vec(), &proof.signature, key)?;
        println!("signature is valid");
    }

    // verify that merkle tree is valid
    assert_eq!(
        idel.cap.0[0].elements, proof.public_inputs.merkle_cap,
        "merkle cap in proof does not match the expected one"
    );

    // verify public inputs
    verify_public_inputs(proof.zkp.clone().public_inputs, proof.public_inputs);
    println!("public inputs do match");

    // verify zkp
    verify_zkp(circuit_sizing, proof.zkp)
}

#[cfg(test)]
mod test {
    use itertools::Either;

    use crate::{
        mock::{
            jwt::{mock_jwt, mock_nonces},
            merkle::mock_merkle,
        },
        prove::{compute_circuit_sizing, generate_proof},
    };

    use super::verify_full_proof;

    #[test]
    #[should_panic(expected = "merkle cap in proof does not match")]
    fn test_verify_full_proof_wrong_cap() {
        // same setup as for `prove`
        let first_seed = 43;
        let merkle = mock_merkle(5, Some(first_seed));
        let (ns, nv, _, _) = mock_nonces(43);
        let mock_jwt = mock_jwt(&ns, &nv, &merkle.voter_id);
        let circuit_sizing = compute_circuit_sizing(&mock_jwt.jwt, &merkle).unwrap();
        let proof = generate_proof(
            ns,
            nv,
            &mock_jwt.jwt,
            &merkle,
            Some(circuit_sizing.clone()),
            None,
        )
        .unwrap();

        let other_merkle = mock_merkle(5, Some(first_seed + 1));
        verify_full_proof(
            proof,
            circuit_sizing,
            other_merkle.tree,
            Either::Left(mock_jwt.verifying_key),
            false,
            false,
        )
        .unwrap()
    }
}
