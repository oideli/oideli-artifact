use std::cmp::{max, min};

use plonky2_sha256::circuit::array_to_bits;

use crate::jwt::{find_aud, trim_nonce_b64_padding, Jwt};
use crate::merkle::Merkle;
use crate::poseidon::{hashout_to_octets, pad_nonce, poseidon_hash};
use crate::zkp::{PublicInputValues, Targets};
use crate::{
    jwt::{find_email, find_nonce, find_sub},
    merkle::field_to_email,
    zkp::{self, CircuitSizing, TargetValues},
    EligibilityProof,
};
use crate::{Circuit, MAX_ID_LEN, MIN_ID_LEN, NONCE_B64_BYTES_LEN};

/// Computes the circuit sizing for the given jwt and merkle tree
pub fn compute_circuit_sizing(jwt: &Jwt, merkle: &Merkle) -> Result<CircuitSizing, anyhow::Error> {
    let body = String::from_utf8(jwt.body.clone())?;
    let (id_index_in_body, id) = find_email(&body)?;
    let (nonce_index_in_body, _) = find_nonce(&body)?;
    let (sub_index_in_body, sub) = find_sub(&body)?;
    let (aud_index_in_body, aud) = find_aud(&body)?;

    let is_nonce_after_id = nonce_index_in_body > id_index_in_body;
    let is_sub_between_id_nonce = min(nonce_index_in_body, id_index_in_body) < sub_index_in_body
        && max(nonce_index_in_body, id_index_in_body) > sub_index_in_body;
    // TODO remove once other scenarios tested
    assert_eq!(
        is_nonce_after_id, true,
        "nonce is before id: has not been tested"
    );
    assert_eq!(
        is_sub_between_id_nonce, false,
        "sub is between id and nonce: has not been tested"
    );

    // compute min index of fields
    let offset = id.len() - MIN_ID_LEN;
    let id_min_index = if is_nonce_after_id {
        id_index_in_body
    } else {
        id_index_in_body - (MAX_ID_LEN - offset)
    };
    let nonce_min_index = if is_nonce_after_id {
        nonce_index_in_body - offset
    } else {
        nonce_index_in_body
    };
    let sub_min_index = if is_sub_between_id_nonce {
        if is_nonce_after_id {
            sub_index_in_body - offset
        } else {
            sub_index_in_body - (MAX_ID_LEN - offset)
        }
    } else {
        sub_index_in_body
    };
    // TODO here we assume that aud is before the other fields
    let aud_min_index = aud_index_in_body;

    Ok(CircuitSizing {
        jwt_header_b64_bits: jwt.header_b64.len() * 8,
        jwt_body: jwt.body.len(),
        merkle_depth: merkle.depth,
        id_min_index,
        nonce_min_index,
        sub_min_index,
        sub_len: sub.len(),
        aud_min_index,
        aud_len: aud.len(),
        is_nonce_after_id,
        is_sub_between_id_nonce,
    })
}

/// Generates a full eligibility proof for the given circuit and input values
pub fn generate_proof(
    ns: Vec<u8>,
    nv: Vec<u8>,
    jwt: &Jwt,
    merkle: &Merkle,
    circuit_sizing: Option<CircuitSizing>,
    circuit: Option<(&Circuit, Targets)>,
) -> Result<EligibilityProof, anyhow::Error> {
    let body = String::from_utf8(jwt.body.clone())?;
    // plaintext witness of the path to the voter_index (~Vec<hashes along the path>)
    let merkle_witness = merkle.tree.prove(merkle.voter_index);
    let merkle_cap = &merkle.tree.cap;
    let leaf_data = &merkle.tree.leaves[merkle.voter_index];

    // build the circuit
    let target_values = {
        // get fields in jwt body
        let (_, id) = find_email(&body)?;
        let (_, nonce) = find_nonce(&body)?;
        let (_, sub) = find_sub(&body)?;
        let (_, aud) = find_aud(&body)?;
        // sanity checks
        assert_eq!(id, field_to_email(leaf_data));
        assert_eq!(
            base64_url::decode(trim_nonce_b64_padding(&nonce).as_bytes())?,
            pad_nonce(&hashout_to_octets(&poseidon_hash(&ns, &nv)))
        );
        assert_eq!(
            nonce.len(),
            NONCE_B64_BYTES_LEN + (MAX_ID_LEN - id.len()),
            "nonce is not padded properly"
        );

        // converts to bit strings
        let jwt_header_b64_bits = array_to_bits(&jwt.header_b64);
        let digest_bits = array_to_bits(&jwt.digest);

        TargetValues {
            jwt_header_b64: jwt_header_b64_bits,
            jwt_body: jwt.body.clone(),
            digest: digest_bits,
            sub: sub.into_bytes(),
            aud: aud.into_bytes(),
            ns,
            nv: nv.clone(),
            merkle_witness,
            merkle_leaf_index: merkle.voter_index,
            merkle_cap: merkle_cap.clone(),
            merkle_leaf: leaf_data.clone(),
        }
    };

    // connect the circuit with its inputs and generate the proof
    let zkp = match circuit {
        // handle both reference and owned value
        Some((circuit, targets)) => zkp::generate_zkp(circuit, targets, target_values)?,
        None => {
            let circuit_sizing = circuit_sizing.unwrap_or(compute_circuit_sizing(jwt, merkle)?);
            let (circuit, targets) = zkp::build_circuit(&circuit_sizing).unwrap();
            zkp::generate_zkp(&circuit, targets, target_values)?
        }
    };

    // gather public inputs
    let (_, sub) = find_sub(&body)?;
    let (_, aud) = find_aud(&body)?;
    assert_eq!(merkle_cap.0.len(), 1);
    let public_inputs = PublicInputValues {
        digest: jwt.digest.clone().as_slice().try_into()?,
        nv: nv.as_slice().try_into()?,
        merkle_cap: merkle_cap.0[0].elements,
        sub: sub.as_bytes().to_vec(),
        aud: aud.as_bytes().to_vec(),
    };

    Ok(EligibilityProof {
        zkp,
        signature: jwt.signature.clone(),
        kid: jwt.kid.clone(),
        public_inputs,
    })
}

#[cfg(test)]
mod test {
    use itertools::Either;

    use crate::{
        jwt::Jwt,
        mock::{
            jwt::{mock_jwt, mock_nonces},
            merkle::mock_merkle,
        },
        verify::verify_full_proof,
    };

    use super::{compute_circuit_sizing, generate_proof};

    #[test]
    fn test_prove_generate_proof_ok() {
        let merkle_depth = 5;
        let merkle = mock_merkle(merkle_depth, None);
        let (ns, nv, _, _) = mock_nonces(43);
        let mock_jwt = mock_jwt(&ns, &nv, &merkle.voter_id);

        let circuit_sizing = compute_circuit_sizing(&mock_jwt.jwt, &merkle).unwrap();
        let proof = generate_proof(
            ns,
            nv,
            &mock_jwt.jwt,
            &merkle,
            Some(circuit_sizing.clone()),
            None,
        )
        .unwrap();

        verify_full_proof(
            proof,
            circuit_sizing,
            merkle.tree,
            Either::Left(mock_jwt.verifying_key),
            false,
            false,
        )
        .unwrap();
    }

    #[test]
    #[should_panic]
    fn test_prove_generate_proof_fail_wrong_ns() {
        // same as above with a slightly different ns
        let merkle_depth = 5;
        let merkle = mock_merkle(merkle_depth, None);
        let (ns, nv, _, _) = mock_nonces(43);
        let mut ns_prime = ns.clone();
        ns_prime[0] = ns_prime[0] + 1;
        println!("ns_prime, {:?}", ns_prime);
        let mock_jwt = mock_jwt(&ns, &nv, &merkle.voter_id);

        generate_proof(ns_prime, nv, &mock_jwt.jwt, &merkle, None, None).unwrap();
    }

    #[test]
    #[should_panic(expected = "nonce is not padded properly")]
    fn test_prove_generate_proof_fail_nonce_not_padded() {
        // same as above with a slightly different ns
        let merkle_depth = 5;
        let merkle = mock_merkle(merkle_depth, None);
        let (ns, nv, _, _) = mock_nonces(43);
        let mock_jwt = mock_jwt(&ns, &nv, &merkle.voter_id);
        // remove some padding
        let jwt_prime = Jwt {
            body: String::from_utf8(mock_jwt.jwt.body)
                .unwrap()
                .replace("....", "..")
                .as_bytes()
                .to_vec(),
            ..mock_jwt.jwt
        };

        generate_proof(ns, nv, &jwt_prime, &merkle, None, None).unwrap();
    }
}
