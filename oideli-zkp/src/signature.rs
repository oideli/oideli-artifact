use std::fs::File;
use std::io::Read;

use rsa::signature::hazmat::PrehashVerifier;
use rsa::{
    pkcs1v15::{Signature, VerifyingKey},
    BigUint, RsaPublicKey,
};
use serde::{Deserialize, Serialize};
use sha2::Sha256;

pub type Kid = String;

pub fn verify_signature(
    digest: &Vec<u8>,
    signature: &Vec<u8>,
    key: RsaPublicKey,
) -> Result<(), anyhow::Error> {
    let sig = Signature::try_from(signature.as_slice())?;
    Ok(VerifyingKey::<Sha256>::new(key).verify_prehash(digest, &sig)?)
}

pub fn get_verifying_key(
    kid: &Kid,
    endpoint: &str,
    skip_jwks_request: bool,
) -> Result<RsaPublicKey, anyhow::Error> {
    let response = if !skip_jwks_request {
        println!("getting jwks from [{}] ...", endpoint);
        let res = reqwest::blocking::get(endpoint)?.text()?;
        println!("received jwks from [{}]", endpoint);
        res
    } else {
        let mut jwks_file = File::open("./data/jwks.json")?;
        let mut jwks_string = String::new();
        jwks_file.read_to_string(&mut jwks_string)?;
        jwks_string
    };
    let jwks: Jwks = serde_json::from_str(&response)?;
    let jwk = jwks
        .keys
        .iter()
        .find(|k| &k.kid == kid)
        .ok_or(anyhow::Error::msg("key not found in jwks"))?;
    let n_bytes = base64_url::decode(&jwk.n)?;
    let n = BigUint::from_bytes_be(&n_bytes);
    let e_bytes = base64_url::decode(&jwk.e)?;
    let e = BigUint::from_bytes_be(&e_bytes);
    let key = RsaPublicKey::new(n, e)?;
    Ok(key)
}
#[derive(Serialize, Deserialize)]
struct Jwks {
    keys: Vec<Jwk>,
}
#[derive(Serialize, Deserialize)]
struct Jwk {
    kid: String,
    n: String,
    e: String,
}

#[cfg(test)]
mod test {
    use httpmock::Method::GET;
    use httpmock::MockServer;
    use rand::SeedableRng;
    use rand_chacha::ChaCha20Rng;
    use rsa::pkcs1v15::SigningKey;
    use rsa::sha2::{Digest, Sha256};
    use rsa::signature::{Keypair, RandomizedSigner, SignatureEncoding};
    use rsa::traits::PublicKeyParts;
    use rsa::{RsaPrivateKey, RsaPublicKey};

    use crate::signature::verify_signature;

    use super::get_verifying_key;

    #[test]
    fn test_verify_signature_ok() {
        let (_, digest, signature, verifying_key) = test_verify_signature();
        verify_signature(&digest, &signature, verifying_key).unwrap()
    }

    #[test]
    #[should_panic]
    fn test_verify_signature_fail() {
        let (data, _, signature, verifying_key) = test_verify_signature();
        // attempt to verify the signature on the unhashed data
        verify_signature(&data, &signature, verifying_key).unwrap()
    }

    fn test_verify_signature() -> (Vec<u8>, Vec<u8>, Vec<u8>, RsaPublicKey) {
        let mut rng = ChaCha20Rng::seed_from_u64(42);

        let bits = 2048;
        let private_key = RsaPrivateKey::new(&mut rng, bits).unwrap();
        let signing_key = SigningKey::<Sha256>::new(private_key);
        let verifying_key = signing_key.verifying_key();

        let data = b"hello world";
        let signature = signing_key.sign_with_rng(&mut rng, data);
        let digest = {
            let mut sha256_input_bytes = Vec::new();
            sha256_input_bytes.append(&mut data.to_vec());
            Sha256::digest(sha256_input_bytes)
        };

        (
            data.to_vec(),
            digest.to_vec(),
            signature.to_bytes().into(),
            verifying_key.into(),
        )
    }

    #[test]
    fn test_get_verifying_key() {
        let server = MockServer::start();
        let mock_jwks_request = server.mock(|when, then| {
            when.method(GET).path("/keys");
            then.status(200)
                .header("content-type", "application/json")
                .body(MOCK_JWKS);
        });

        let key = get_verifying_key(
            &"7d334497506acb74cdeedaa66184d15547f83693".to_owned(),
            &server.url("/keys"),
            false,
        )
        .unwrap();

        mock_jwks_request.assert();
        assert_eq!(key.e().to_string(), "65537");
    }

    const MOCK_JWKS: &str = r#"{
        "keys": [
          {
            "kty": "RSA",
            "alg": "RS256",
            "e": "AQAB",
            "use": "sig",
            "n": "keFudaSl4KpJ2xC-fIGOb4eD4hwmCVF3eWxginhvrcLNx3ygDjcN7wGRC-CkzJ12ymBGsTPnSBiTFTpwpa5LXEYi-wvN-RkwA8eptcFXIzCXn1k9TqFxaPfw5Qv8N2hj0ZnFR5KPMr1bgK8vktlBu_VbptXr9IKtUEpV0hQCMjmc0JAS61ZIgx9XhPWaRbuYUvmBVLN3ButKAoWqUuzdlP1arjC1R8bUWek3xKUuSSJmZ9oHIGU5omtTEgXRDiv442R3tle-gLcfcr57uPnaAh9bIgBJRZw2mjqP8uBZurq6YkuyUDFQb8NFkBxHigoEdE7di_OtEef2GFNLseE6mw",
            "kid": "7d334497506acb74cdeedaa66184d15547f83693"
          },
          {
            "use": "sig",
            "kty": "RSA",
            "e": "AQAB",
            "kid": "a06af0b68a2119d692cac4abf415ff3788136f65",
            "alg": "RS256",
            "n": "yrIpMnHYrVPwlbC-IY8aU2Q6QKnLf_p1FQXNiTO9mWFdeYXP4cNF6QKWgy4jbVSrOs-4qLZbKwRvZhfTuuKW6fwj5lVZcNsq5dd6GXR65I8kwomMH-Zv_pDt9zLiiJCp5_GU6Klb8zMY_jEE1fZp88HIk2ci4GrmtPTbw8LHAkn0P54sQQqmCtzqAWp8qkZ-GGNITxMIdQMY225kX7Dx91ruCb26jPCvF5uOrHT-I6rFU9fZbIgn4T9PthruubbUCutKIR-JK8B7djf61f8ETuKomaHVbCcxA-Q7xD0DEJzeRMqiPrlb9nJszZjmp_VsChoQQg-wl0jFP-1Rygsx9w"
          },
          {
            "kty": "RSA",
            "use": "sig",
            "alg": "RS256",
            "kid": "c6263d09745b5032e57fa6e1d041b77a54066dbd",
            "e": "AQAB",
            "n": "0quLYDiZIxssFKreHcXeeUIbgyU-dctbQXTfBTbAKp4Jl_TH-FQt3EfBVbo2P_1bkH-6ofvDSkQDUbigOhN4zx7JwbjAl8P18-dgjxuhF9HRdZA2W54VxBspEuHhqpsFZKoH_409ywbnc0DtAT-OQR3oQ-6ZnJfUOkLvw7o62QSDyscEi_zh8NIAGQnBo98UVVWr6lbR_PIm7l_NZu0LAux-P5Av-CxAxf32Dvl6crfv_I8ME3_fRisfKaVn5qOt_XuSXmygtTtT94lwelCCuutT6VjjIe397j83yR6LDZACOY7aAw8dx_rb3TS-SgvxQoBshj3142B4RFTVwupyQQ"
          }
        ]
      }"#;
}
