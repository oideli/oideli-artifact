use std::{
    fs::File,
    io::{Read, Write},
    path::Path,
    sync::Once,
};

use anyhow::Context;
use clap::{Parser, Subcommand};
use itertools::Either;
use oideli_zkp::{
    jwt::{get_jwt, padded_nonce_b64, parse_jwt},
    merkle::{build_merkle, load_voter_ids},
    poseidon::{hashout_to_octets, pad_nonce, poseidon_hash},
    prove, verify, EligibilityProof,
};
use serde::{de::DeserializeOwned, Serialize};

fn main() -> Result<(), anyhow::Error> {
    init_logger();
    let cli = Cli::parse();

    let voter_ids = load_voter_ids(&cli.voter_ids_file)?;
    let merkle = build_merkle(&voter_ids, &cli.voter_id)?;

    match cli.command {
        Commands::ComputeCircuitSizing {
            jwt_file,
            output_file,
        } => {
            let jwt = get_jwt(&jwt_file)
                .with_context(|| format!("failed to parse jwt in [{}]", &jwt_file))?;
            let circuit_sizing = prove::compute_circuit_sizing(&jwt, &merkle)?;
            write_to_file(output_file, circuit_sizing)
        }
        Commands::Prove {
            jwt_file,
            ns_file,
            nv_file,
            output_file,
        } => {
            let jwt = get_jwt(&jwt_file)
                .with_context(|| format!("failed to parse jwt in [{}]", &jwt_file))?;
            println!("jwt body: {}", String::from_utf8(jwt.body.clone()).unwrap());
            let ns = load_from_file(&ns_file)
                .with_context(|| format!("failed to load ns from [{}]", &ns_file))?;
            let nv = load_from_file(&nv_file)
                .with_context(|| format!("failed to load nv from [{}]", &nv_file))?;

            let proof = prove::generate_proof(ns, nv, &jwt, &merkle, None, None)?;
            println!("proof successful");

            write_to_file(output_file, proof)
        }
        Commands::Verify {
            input_file,
            circuit_sizing_file,
            skip_jwks_request,
            skip_signature_verification,
        } => {
            let proof: EligibilityProof = load_from_file(&input_file)
                .with_context(|| format!("failed to load proof from [{}]", &input_file))?;
            let circuit_sizing = load_from_file(&circuit_sizing_file).with_context(|| {
                format!(
                    "failed to load circuit sizing from [{}]",
                    &circuit_sizing_file
                )
            })?;

            verify::verify_full_proof(
                proof.clone(),
                circuit_sizing,
                merkle.tree,
                Either::Right(&proof.kid),
                skip_jwks_request,
                skip_signature_verification,
            )
            .with_context(|| "failed to verify proof")?;

            println!("verification successful")
        }
        Commands::Poseidon { ns, nv } => {
            // get bytes from hex ns and nv
            let ns = hex::decode(ns)?;
            let nv = hex::decode(nv)?;
            // hash and pad
            let nonce = pad_nonce(&hashout_to_octets(&poseidon_hash(&ns, &nv)));
            // base64
            let mut nonce_b64 = String::new();
            base64_url::encode_to_string(&nonce, &mut nonce_b64);
            // pad id
            let nonce_b64_padded = padded_nonce_b64(&nonce_b64, &cli.voter_id);
            println!("{}", nonce_b64_padded)
        }
        Commands::ProveNoFile { jwt, ns, nv } => {
            let jwt = parse_jwt(jwt).with_context(|| format!("failed to parse jwt"))?;
            println!("jwt body: {}", String::from_utf8(jwt.body.clone()).unwrap());
            let ns = hex::decode(ns).with_context(|| format!("failed to parse ns"))?;
            let nv = hex::decode(nv).with_context(|| format!("failed to parse ns"))?;

            let circuit_sizing = prove::compute_circuit_sizing(&jwt, &merkle)?;
            let proof =
                prove::generate_proof(ns, nv, &jwt, &merkle, Some(circuit_sizing.clone()), None)?;

            println!("{}", serde_json::to_string(&proof)?);
            println!("{}", serde_json::to_string(&circuit_sizing)?);
        }
    }

    Ok(())
}

#[derive(Parser)]
struct Cli {
    /// File containg the ids of eligible voters
    #[arg(long, default_value_t = String::from("data/emails.txt"))]
    voter_ids_file: String,
    /// Id of the voter (email)
    #[arg(long, default_value_t = String::from("oideli.zkp@gmail.com"))]
    // 0.plonky.oauth.000000000000@gmail.com
    voter_id: String,
    #[command(subcommand)]
    command: Commands,
}

#[derive(Subcommand)]
enum Commands {
    /// Compute the circuit sizing
    ComputeCircuitSizing {
        /// File containg jwt in base64
        #[arg(short, long, default_value_t = String::from("data/id_token.txt"))]
        jwt_file: String,
        /// File where to write the circuit sizing
        #[arg(short, long, default_value_t = String::from("tmp/circuit_sizing.json"))]
        output_file: String,
    },
    /// Generate a proof
    Prove {
        /// File containg jwt in base64
        #[arg(short, long, default_value_t = String::from("data/id_token.txt"))]
        jwt_file: String,
        /// File containg ns
        #[arg(long, default_value_t = String::from("data/ns.json"))]
        ns_file: String,
        /// File containg nv
        #[arg(long, default_value_t = String::from("data/nv.json"))]
        nv_file: String,
        /// File where to write the proof
        #[arg(short, long, default_value_t = String::from("tmp/proof.json"))]
        output_file: String,
    },
    /// Generate a proof (arguments as string)
    ProveNoFile {
        /// File containg jwt in base64
        #[arg(short, long)]
        jwt: String,
        /// File containg ns
        #[arg(long)]
        ns: String,
        /// File containg nv
        #[arg(long)]
        nv: String,
    },
    /// Verify a proof
    Verify {
        /// File containing the proof
        #[arg(short, long, default_value_t = String::from("tmp/proof.json"))]
        input_file: String,
        /// File containing the circuit sizing
        #[arg(short, long, default_value_t = String::from("tmp/circuit_sizing.json"))]
        circuit_sizing_file: String,
        /// Whether or not to skip the request the jwks endpoint. If set, a "cached" static jwks is used.
        #[arg(short, long)]
        skip_jwks_request: bool,
        /// Whether or not to skip signature verification.
        #[arg(short, long)]
        skip_signature_verification: bool,
    },
    /// Hash ns and nv using poseidon
    Poseidon {
        /// Ns
        #[arg(long)]
        ns: String,
        /// Nv
        #[arg(long)]
        nv: String,
    },
}

fn load_from_file<T: DeserializeOwned>(filename: &str) -> Result<T, anyhow::Error> {
    let path = Path::new(&filename);
    let display = path.display();
    let mut file = File::open(&path)?;
    let mut json = String::new();
    file.read_to_string(&mut json)?;
    let data: T = serde_json::from_str(json.as_str())?;
    println!("data loaded from {}", display);

    Ok(data)
}

fn write_to_file<T: Serialize>(filename: String, data: T) {
    let path = Path::new(&filename);
    let display = path.display();
    let mut file = match File::create(&path) {
        Err(why) => panic!("failed to create {}: {}", display, why),
        Ok(file) => file,
    };
    match file.write_all(serde_json::to_string(&data).unwrap().as_bytes()) {
    // to test more compact serde:
    // match file.write_all(&rmp_serde::to_vec(&data).unwrap()) {
        Err(why) => panic!("failed to write to {}: {}", display, why),
        Ok(_) => println!("data written to {}", display),
    }
}

static INIT_LOGGER: Once = Once::new();
pub fn init_logger() {
    INIT_LOGGER.call_once(|| {
        env_logger::init();
    });
}
