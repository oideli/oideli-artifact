use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

use anyhow::bail;
use anyhow::Ok;
use plonky2::field::types::Field;
use plonky2::hash::poseidon::PoseidonHash;
use plonky2_field::goldilocks_field::GoldilocksField;

use crate::jwt::padded_id;
use crate::F;
use crate::ID_SUFFIX;
use crate::MAX_ID_LEN;
use crate::MIN_ID_LEN;

type Plonky2MerkleTree = plonky2::hash::merkle_tree::MerkleTree<GoldilocksField, PoseidonHash>;

/// Struct grouping a merkle tree, its dimensions and the voter index
#[derive(Clone)]
pub struct Merkle {
    /// Merkle tree
    pub tree: Plonky2MerkleTree,
    /// Depth of the tree
    pub depth: usize,
    /// Index of the voter in the leaves
    pub voter_index: usize,
    /// Id of the voter
    pub voter_id: String,
    /// Eligible voters
    pub voter_ids: Vec<String>,
}

/// Build a merkle tree for the given voters list
pub fn build_merkle(voters: &Vec<String>, voter_id: &str) -> Result<Merkle, anyhow::Error> {
    let n = voters.len().checked_next_power_of_two().unwrap(); // number of leaves in the merkle tree
    assert_eq!(n, voters.len()); // only accept full trees
    let log_n = n.trailing_zeros(); // merkle tree depth

    // position of the voter in the merkle tree leaves
    let voter_index = match voters.iter().position(|l| l == &voter_id) {
        Some(index) => Ok(index),
        None => bail!("voter id not present in voters list"),
    }?;

    let leaves = voters
        .into_iter()
        .map(|id| email_to_field(id.clone()).to_vec())
        .collect();
    let tree = Plonky2MerkleTree::new(leaves, 0);

    Ok(Merkle {
        tree,
        depth: log_n as usize,
        voter_index,
        voter_id: voter_id.to_owned(),
        voter_ids: voters.clone(),
    })
}

/// Returns the list of ids in the provided file
pub fn load_voter_ids(filename: &str) -> Result<Vec<String>, anyhow::Error> {
    let emails_file = File::open(filename)?;
    let lines = BufReader::new(emails_file).lines();

    let mut emails = Vec::new();
    for line in lines {
        let email = line?;
        assert!(email.len() >= MIN_ID_LEN, "email [{}] is too short", email);
        assert!(email.len() <= MAX_ID_LEN, "email [{}] is too long", email);
        assert!(
            email.ends_with(ID_SUFFIX),
            "email [{}] does not have suffix [{}]",
            email,
            ID_SUFFIX
        );
        emails.push(email);
    }

    Ok(emails)
}

/// Converts a string to field elements
pub fn email_to_field(email: String) -> [GoldilocksField; MAX_ID_LEN] {
    assert!(email.len() <= MAX_ID_LEN, "email is too long: [{}]", email);
    padded_id(&email)
        .into_bytes()
        .into_iter()
        .map(|c| F::from_canonical_u8(c))
        .collect::<Vec<GoldilocksField>>()
        .try_into()
        .unwrap()
}

/// Converts field elements to string
pub fn field_to_email(field_elems: &Vec<GoldilocksField>) -> String {
    field_elems
        .into_iter()
        .map(|e| (e.0 as u8) as char)
        .collect::<String>()
        .trim() // because of length hiding padding
        .to_owned()
}

#[cfg(test)]
mod test {
    use super::build_merkle;

    #[test]
    fn test_build_merkle_tree_ok() {
        let merkle_tree = build_merkle(
            &vec![
                "a@b.com".to_owned(),
                "b@c.com".to_owned(),
                "c@p.org".to_owned(),
                "d@b.com".to_owned(),
            ],
            "b@c.com",
        )
        .unwrap();
        assert_eq!(merkle_tree.tree.leaves.len(), 4);
        assert_eq!(merkle_tree.depth, 2);
        assert_eq!(merkle_tree.voter_index, 1);
        assert_eq!(merkle_tree.voter_id, "b@c.com");
    }

    #[test]
    #[should_panic]
    fn test_build_merkle_tree_wrong_email() {
        let _merkle_tree = build_merkle(
            &vec![
                "a@b.com".to_owned(),
                "b@c.com".to_owned(),
                "c@p.org".to_owned(),
                "d@b.com".to_owned(),
            ],
            "z@c.com",
        )
        .unwrap();
    }

    #[test]
    #[should_panic(expected = "email is too long")]
    fn test_build_merkle_tree_email_too_long() {
        let _merkle_tree = build_merkle(
            &vec![
                "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa@b.com".to_owned(),
                "b@c.com".to_owned(),
                "c@p.org".to_owned(),
                "d@b.com".to_owned(),
            ],
            "b@c.com",
        )
        .unwrap();
    }
}
