use std::collections::HashSet;

use itertools::Itertools;
use rand::{
    distributions::{Alphanumeric, Uniform},
    Rng, SeedableRng,
};
use rand_chacha::ChaCha20Rng;

use crate::{
    merkle::{build_merkle, Merkle},
    ID_SUFFIX, MAX_ID_LEN, MIN_ID_LEN,
};

pub fn mock_merkle(log_n: usize, seed: Option<usize>) -> Merkle {
    let n = 1 << log_n;
    let voter_index: usize = n / 2;
    let seed = seed.unwrap_or(42);
    let leaves = generate_leaves(n, seed);
    let voter_id = leaves[voter_index].clone();
    build_merkle(&leaves, &voter_id).unwrap()
}

fn generate_leaves(n: usize, seed: usize) -> Vec<String> {
    let mut rng = ChaCha20Rng::seed_from_u64(seed.try_into().unwrap());
    println!("generating {} leaves...", n);
    let mut leaves: HashSet<String> = HashSet::new();
    for _i in 0..n {
        let mut new_email = generate_email(&mut rng);
        let mut _dupl = 0;
        // avoid duplicates
        while leaves.contains(&new_email) {
            // println!(" got duplicate {}", new_email);
            _dupl += 1;
            new_email = generate_email(&mut rng);
        }
        // if _dupl > 0 {
        //     println!(" got {} duplicates for leaf {}", _dupl, _i);
        // }
        leaves.insert(new_email);
    }
    println!("generation done");
    leaves.into_iter().collect_vec()
}

pub fn generate_email(rng: &mut ChaCha20Rng) -> String {
    let max_id_len = MAX_ID_LEN - ID_SUFFIX.len();
    let min_id_len = MIN_ID_LEN - ID_SUFFIX.len();
    let id_len = rng.sample(&Uniform::new(min_id_len, max_id_len + 1));
    let id: String = rng
        .sample_iter(&Alphanumeric)
        .take(id_len)
        .map(char::from)
        .collect();

    format!("{}{}", id, ID_SUFFIX)
}
