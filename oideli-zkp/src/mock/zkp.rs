use crate::{
    zkp::{build_circuit, CircuitSizing, TargetValues, Targets},
    Circuit,
};
use plonky2_sha256::circuit::array_to_bits;

use super::{
    jwt::{mock_jwt, mock_nonces},
    merkle::mock_merkle,
};

pub fn mock_circuit_and_targets(
    seed: u64,
) -> Result<(CircuitSizing, Circuit, Targets, TargetValues), anyhow::Error> {
    // ns and nv
    let (ns, nv, _, _) = mock_nonces(seed);

    // merkle tree
    let log_n = 5;
    let merkle = mock_merkle(log_n, None);
    let merkle_witness = merkle.tree.prove(merkle.voter_index);
    let merkle_cap = merkle.tree.cap;
    let leaf_data = merkle.tree.leaves[merkle.voter_index].clone();

    // jwt
    let mock_jwt = mock_jwt(&ns, &nv, &merkle.voter_id);
    let header_b64_bits = array_to_bits(&mock_jwt.jwt.header_b64);
    let digest_bits = array_to_bits(&mock_jwt.jwt.digest);

    let sizing = CircuitSizing {
        jwt_header_b64_bits: header_b64_bits.len(),
        jwt_body: mock_jwt.jwt.body.len(),
        merkle_depth: log_n,
        id_min_index: mock_jwt.id_min_index,
        nonce_min_index: mock_jwt.nonce_min_index,
        sub_min_index: mock_jwt.sub_min_index,
        sub_len: mock_jwt.sub_len,
        aud_min_index: mock_jwt.aud_min_index,
        aud_len: mock_jwt.aud_len,
        is_nonce_after_id: true,
        is_sub_between_id_nonce: false,
    };
    let (circuit, targets) = build_circuit(&sizing).unwrap();

    let values = TargetValues {
        jwt_header_b64: header_b64_bits,
        jwt_body: mock_jwt.jwt.body,
        digest: digest_bits,
        sub: mock_jwt.sub,
        aud: mock_jwt.aud,
        ns,
        nv,
        merkle_witness,
        merkle_leaf_index: merkle.voter_index,
        merkle_cap,
        merkle_leaf: leaf_data,
    };

    Ok((sizing, circuit, targets, values))
}
