use plonky2::{hash::poseidon::PoseidonHash, plonk::config::Hasher};
use plonky2_field::goldilocks_field::GoldilocksField;
use plonky2_field::types::Field;
use rand::{Rng, SeedableRng};
use rand_chacha::ChaCha20Rng;
use rand_pcg::Pcg32;
use rsa::signature::SignatureEncoding;
use rsa::signature::{Keypair, RandomizedDigestSigner};
use rsa::{pkcs1v15::SigningKey, RsaPrivateKey, RsaPublicKey};
use sha2::{Digest, Sha256};

use crate::jwt::{self, Jwt};
use crate::poseidon::{self, hashout_to_octets};
use crate::{F, MIN_ID_LEN};

#[derive(Clone)]
pub struct MockJwt {
    pub jwt: Jwt,
    pub sub: Vec<u8>,
    pub aud: Vec<u8>,
    pub verifying_key: RsaPublicKey,
    pub nonce_min_index: usize,
    pub id_min_index: usize,
    pub sub_min_index: usize,
    pub sub_len: usize,
    pub aud_min_index: usize,
    pub aud_len: usize,
}

pub fn mock_jwt(ns: &Vec<u8>, nv: &Vec<u8>, voter_id: &str) -> MockJwt {
    let header_b64 = "eyJhbGciOiJSUzI1NiIsImtpZCI6IjkzNDFkZWRlZWUyZDE4NjliNjU3ZmE5MzAzMDAwODJmZTI2YjNkOTIiLCJ0eXAiOiJKV1QifQ".to_owned().into_bytes();
    let kid = "9341dedeee2d1869b657fa930300082fe26b3d92".to_owned();

    let (_, nonce_b64) = ns_nv_to_nonce(ns, nv);
    let nonce_b64_padded = jwt::padded_nonce_b64(&String::from_utf8(nonce_b64).unwrap(), voter_id);

    let sub = "103333552062856499099";
    let aud = "71307269411-0lrk3veg9jnqrbm19jkj1gd6ioommsqf.apps.googleusercontent.com";
    let body_str = format!(
        r#"{{"iss":"accounts.google.com","azp":"71307269411-0lrk3veg9jnqrbm19jkj1gd6ioommsqf.apps.googleusercontent.com","aud":"{}","sub":"{}","email":"{}","email_verified":true,"at_hash":"nZoXu5qgTg9CJXs97hD_1A","nonce":"{}","iat":1688563556,"exp":1688567156}}"#,
        aud, sub, voter_id, nonce_b64_padded
    );
    let body = body_str.into_bytes();
    let body_b64 = base64_url::encode(&body).into_bytes();

    // indexes counted by hand
    let offset = voter_id.len() - MIN_ID_LEN;
    let aud_index_body = 116;
    let aud_len = 71;
    let sub_index_body = aud_index_body + aud_len + 9;
    let sub_len = 21;
    let email_index_body = 228;
    let nonce_min_index = email_index_body + voter_id.len() + 68 - offset;

    let digest: Sha256 = {
        let mut bytes = Vec::new();
        bytes.append(&mut header_b64.clone());
        bytes.push('.' as u8);
        bytes.append(&mut body_b64.clone());
        let mut hasher = Sha256::new();
        hasher.update(bytes);
        hasher
    };

    let mut rng = ChaCha20Rng::seed_from_u64(42);
    let bits = 2048;
    let private_key = RsaPrivateKey::new(&mut rng, bits).unwrap();
    let signing_key = SigningKey::<Sha256>::new(private_key);
    let verifying_key = signing_key.verifying_key();
    let signature = signing_key.sign_digest_with_rng(&mut rng, digest.clone());

    MockJwt {
        jwt: Jwt {
            header_b64,
            body,
            digest: digest.finalize().to_vec(),
            signature: signature.to_bytes().into(),
            kid,
        },
        verifying_key: verifying_key.into(),
        sub: sub.to_owned().into_bytes(),
        aud: aud.to_owned().into_bytes(),
        nonce_min_index,
        id_min_index: email_index_body,
        sub_min_index: sub_index_body,
        sub_len,
        aud_min_index: aud_index_body,
        aud_len,
    }
}

pub fn mock_nonces(seed: u64) -> (Vec<u8>, Vec<u8>, [GoldilocksField; 4], Vec<u8>) {
    let mut rng = Pcg32::seed_from_u64(seed);
    let ns: Vec<u8> = (0..32).map(|_| rng.gen()).collect();
    let nv: Vec<u8> = (0..32).map(|_| rng.gen()).collect();
    let (nonce, nonce_b64) = ns_nv_to_nonce(&ns, &nv);

    (ns, nv, nonce, nonce_b64)
}

fn ns_nv_to_nonce(ns: &Vec<u8>, nv: &Vec<u8>) -> ([GoldilocksField; 4], Vec<u8>) {
    let mut ns_nv: Vec<GoldilocksField> = Vec::new();
    for &b in ns {
        ns_nv.push(F::from_canonical_u8(b));
    }
    for &b in nv {
        ns_nv.push(F::from_canonical_u8(b));
    }
    let nonce = PoseidonHash::hash_no_pad(&ns_nv).elements;
    let nonce_bytes = hashout_to_octets(&nonce); // 256 bits = 32 bytes
    let nonce_padded = poseidon::pad_nonce(&nonce_bytes); // + 1 = 33 bytes
    let nonce_b64 = base64_url::encode(&nonce_padded).into_bytes(); // 33 * 4/3  = 44

    (nonce, nonce_b64)
}
