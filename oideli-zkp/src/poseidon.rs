use crate::{F, NONCE_BYTES_LEN};
use plonky2::{field::types::Field, hash::poseidon::PoseidonHash, plonk::config::Hasher};
use plonky2_field::{goldilocks_field::GoldilocksField, types::PrimeField};
use plonky2_sha256::circuit::array_to_bits;

/// Returns the nonce resulting from hashing the inputs with poseidon
pub fn poseidon_hash(ns: &Vec<u8>, nv: &Vec<u8>) -> [GoldilocksField; 4] {
    let mut ns_nv: Vec<GoldilocksField> = Vec::new();
    ns.clone()
        .into_iter()
        .for_each(|b| ns_nv.push(F::from_canonical_u8(b)));
    nv.clone()
        .into_iter()
        .for_each(|b| ns_nv.push(F::from_canonical_u8(b)));
    PoseidonHash::hash_no_pad(&ns_nv).elements
}

/// Pads the number of bytes in the nonce to have a round number in base64 (* 4/3)
pub fn pad_nonce(unpadded: &Vec<u8>) -> Vec<u8> {
    assert_eq!(unpadded.len(), NONCE_BYTES_LEN);
    let mut padded = unpadded.clone();
    padded.push(0 as u8); // 32 + 1 = 33 bytes -> 44 in b64
    padded
}

/// Convert a hashout into bits in the same way as the plonky gates.
pub fn hashout_to_bits(hash: &[GoldilocksField]) -> Vec<bool> {
    let mut result = Vec::new();
    for i in 0..hash.len() {
        result.append(&mut array_to_bits(
            &hash[i].to_canonical_biguint().to_u64_digits()[0].to_be_bytes(),
        ))
    }
    result
}

/// Convert a hashout into octets in the same way as the plonky gates.
pub fn hashout_to_octets(hash: &[GoldilocksField]) -> Vec<u8> {
    let mut result = Vec::new();
    for i in 0..hash.len() {
        let octets = hash[i].to_canonical_biguint().to_u64_digits()[0].to_be_bytes();
        for j in 0..8 {
            result.push(octets[j])
        }
    }
    result
}
