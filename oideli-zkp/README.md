# OIDEli ZKP
This folder contains the codebase of our `oideli-zkp` library for computing eligibility ZKPs.
Any environment supporting the Rust toolchain is suitable to run this software.
Tested on Debian 12 and Fedora 38.
See `../README.md` for instructions to run this in the context of a Belenios election.

## Getting started

### Rust toolchain version

```
rustup toolchain install nightly-2023-12-01
rustup default nightly-2023-12-01
```

### Run the tests
- all tests: `RUST_BACKTRACE=1 cargo +nightly test`

- single test with stdout: `RUST_BACKTRACE=1 cargo +nightly test <test name> -- --nocapture`

- run in single thread: `RUST_BACKTRACE=1 cargo +nightly test <test name> -- --test-threads=1`

### run main
- prepare the circuit sizing (should be public data): 
    ```
    Usage: oideli_zkp compute-circuit-sizing [OPTIONS]

    Options:
    -j, --jwt-file <JWT_FILE>        File containg jwt in base64 [default: data/input_base64.txt]
    -o, --output-file <OUTPUT_FILE>  File where to write the circuit sizing [default: tmp/circuit_sizing.json]
    -h, --help                       Print help
    ```
- generate the proof:
    ```
    Usage: oideli_zkp prove [OPTIONS]

    Options:
    -j, --jwt-file <JWT_FILE>        File containg jwt in base64 [default: data/input_base64.txt]
        --ns-file <NS_FILE>          File containg ns [default: data/ns.json]
        --nv-file <NV_FILE>          File containg nv [default: data/nv.json]
    -o, --output-file <OUTPUT_FILE>  File where to write the proof [default: tmp/proof.json]
    -h, --help                       Print help
    ```
- verify the proof:
    ```
    Usage: oideli_zkp verify [OPTIONS]

    Options:
    -i, --input-file <INPUT_FILE>                    [default: tmp/proof.json]
    -c, --circuit-sizing-file <CIRCUIT_SIZING_FILE>  [default: tmp/circuit_sizing.json]
    -h, --help                                       Print help
    ```

run on specific cpus:
```
taskset --cpu-list 36-52 time ./target/release/oideli_zkp prove
```

time without build:
```
cargo +nightly build --release
time RUST_BACKTRACE=1 RUST_LOG=debug ./target/release/oideli_zkp prove
```

## More details about the tests and some example runs

See [README_notes.md](README_notes.md)

## Benchmarks

See [README_benchmarks.md](README_benchmarks.md)