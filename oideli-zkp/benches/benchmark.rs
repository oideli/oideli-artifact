use criterion::{criterion_group, criterion_main, BatchSize, Criterion, SamplingMode};
use itertools::Either;
use oideli_zkp::merkle::Merkle;
use oideli_zkp::verify::verify_full_proof;
use oideli_zkp::{
    mock::{
        jwt::{mock_jwt, mock_nonces},
        merkle::mock_merkle,
    },
    prove::{compute_circuit_sizing, generate_proof},
};
use rand::thread_rng;
use rand::Rng;

criterion_main!(proof);

criterion_group!(proof, bench_generate, bench_verify);

fn bench_generate(_c: &mut Criterion) {
    let merkle_depth = 17; // ~130k voters
    let merkle = mock_merkle(merkle_depth, None);
    let circuit_sizing = {
        let (ns, nv, _, _) = mock_nonces(43);
        let mock_jwt = mock_jwt(&ns, &nv, &merkle.voter_id);
        compute_circuit_sizing(&mock_jwt.jwt, &merkle).unwrap()
    };

    let mut rng = thread_rng();
    let mut group = _c.benchmark_group("generate");
    group.sample_size(100);
    group.sampling_mode(SamplingMode::Flat);
    group.bench_function("generate", |b| {
        b.iter_batched(
            || {
                let r: u64 = rng.gen();
                let (ns, nv, _, _) = mock_nonces(r);
                let voter_index = rng.gen_range(0..merkle.voter_ids.len());
                let voter_id = merkle.voter_ids[voter_index].clone();
                let merkle = Merkle {
                    voter_id: voter_id.clone(),
                    voter_index,
                    ..merkle.clone()
                };
                println!("voter: {}", voter_id);
                let mock_jwt = mock_jwt(&ns, &nv, &voter_id);
                (ns, nv, mock_jwt.jwt, merkle)
            },
            |(ns, nv, jwt, merkle)| {
                generate_proof(ns, nv, &jwt, &merkle, Some(circuit_sizing.clone()), None).unwrap();
            },
            BatchSize::SmallInput,
        )
    });
}

fn bench_verify(_c: &mut Criterion) {
    let merkle_depth = 17;
    let merkle = mock_merkle(merkle_depth, None);
    let (ns, nv, _, _) = mock_nonces(43);
    let mock_jwt = mock_jwt(&ns, &nv, &merkle.voter_id);
    let circuit_sizing = { compute_circuit_sizing(&mock_jwt.jwt, &merkle).unwrap() };
    // we bench verify with only one proof since it is less crucial than generate
    let proof = generate_proof(
        ns,
        nv,
        &mock_jwt.jwt,
        &merkle,
        Some(circuit_sizing.clone()),
        None,
    )
    .unwrap();
    let verifying_key = mock_jwt.verifying_key;

    let mut group = _c.benchmark_group("verify");
    group.sample_size(100);
    group.sampling_mode(SamplingMode::Flat);
    group.bench_function("verify", |b| {
        b.iter(|| {
            verify_full_proof(
                proof.clone(),
                circuit_sizing.clone(),
                merkle.tree.clone(),
                Either::Left(verifying_key.clone()),
                true,
                false,
            )
        })
    });
}
