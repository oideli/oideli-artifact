
[`oideli.patch`](oideli.patch) patches Belenios to plug OIDEli-zk in.

## Messages exchanged in implementation

```
- client -> server:     ballot                  (reviewed ballot)
- ...                                           (belenios voter auth, needed to know the voter id)
- client -> server:     nv                      (voter nonce)
- server -> client:     ns, nonce, voter_id     (server nonce and voter id needed for padding nonce)
- client -> server:                             (ask for auth req)
- server -> client:     auth-req                 
- client -> idp:        auth-req                (start oidc flow)
- ...                                           (server gets id token)
- client -> server:                             (ask for id token)
- server -> client:     id token
- client <-> idp:                               (get jwks to verify id token)
- client -> server:     nv0                     (reveal commited value and conclude voter role)
```