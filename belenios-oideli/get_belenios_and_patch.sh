#!/usr/bin/env bash

cd "$(dirname "$0")"

rm -rf belenios
git clone https://gitlab.inria.fr/belenios/belenios.git
cd belenios
git checkout 06292495
git clean -df
git apply --reject --whitespace=fix ../oideli.patch