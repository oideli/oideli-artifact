This folder contains a Rust implementation of the Poseidon hash function that can be compiled to WASM so that it can be used in the browser in voting clients.

Build:
```
wasm-pack build --target web
```