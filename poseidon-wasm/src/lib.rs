use plonky2::{
    field::types::Field,
    hash::poseidon::PoseidonHash,
    plonk::config::{GenericConfig, Hasher, PoseidonGoldilocksConfig},
};
use plonky2_field::{goldilocks_field::GoldilocksField, types::PrimeField};
use wasm_bindgen::prelude::*;

const D: usize = 2;
type C = PoseidonGoldilocksConfig;
type F = <C as GenericConfig<D>>::F;

const MAX_ID_LEN: usize = 50;
const NONCE_BYTES_LEN: usize= 32;

#[wasm_bindgen]
extern {
    pub fn alert(s: &str);
}

#[wasm_bindgen]
pub fn build_nonce(ns: &str, nv: &str, voter_id: &str) -> String {
    let ns = hex::decode(ns).unwrap();
    let nv = hex::decode(nv).unwrap();

    // hash and pad
    let nonce = pad_nonce(&hashout_to_octets(&poseidon_hash(&ns, &nv)));
    // base64
    let mut nonce_b64 = String::new();
    base64_url::encode_to_string(&nonce, &mut nonce_b64);
    // pad id
    let nonce_b64_padded = padded_nonce_b64(&nonce_b64, voter_id);

    nonce_b64_padded
}

// copied functions

pub fn poseidon_hash(ns: &Vec<u8>, nv: &Vec<u8>) -> [GoldilocksField; 4] {
    let mut ns_nv: Vec<GoldilocksField> = Vec::new();
    ns.clone()
        .into_iter()
        .for_each(|b| ns_nv.push(F::from_canonical_u8(b)));
    nv.clone()
        .into_iter()
        .for_each(|b| ns_nv.push(F::from_canonical_u8(b)));
    PoseidonHash::hash_no_pad(&ns_nv).elements
}

fn hashout_to_octets(hash: &[GoldilocksField]) -> Vec<u8> {
    let mut result = Vec::new();
    for i in 0..hash.len() {
        let octets = hash[i].to_canonical_biguint().to_u64_digits()[0].to_be_bytes();
        for j in 0..8 {
            result.push(octets[j])
        }
    }
    result
}

pub fn pad_nonce(unpadded: &Vec<u8>) -> Vec<u8> {
    assert_eq!(unpadded.len(), NONCE_BYTES_LEN);
    let mut padded = unpadded.clone();
    padded.push(0 as u8); // 32 + 1 = 33 bytes -> 44 in b64
    padded
}

pub fn padded_nonce_b64(nonce: &str, unpadded_id: &str) -> String {
    let padding = ".".repeat(MAX_ID_LEN - unpadded_id.len());
    nonce.to_owned() + &padding
}
