# OIDEli
This repository contains all the supplementary material for the paper:
    Election Eligibility with OpenID: Turning Authentication into Transferable Proof of Eligibility
by
    Véronique Cortier, Alexandre Debant, Anselme Goetschmann, Lucca Hirschi
from
    Inria, LORIA, CNRS, Université de Lorraine, France.

## Security analysis with ProVerif
See [`proverif-files`](proverif-files/).

## [`oideli-zkp`](oideli-zkp/): Eligibility ZKP Library
See [`oideli-zkp`](oideli-zkp/).

## [`belenios-oideli`](belenios-oideli/): OIDELi-zk Integration in Belenios
We provide a patch to the Belenios code-base for integrating OIDELi in [`belenios-oideli`](belenios-oideli/).
For simpler reproducibility, we provide below docker images and detailed explanations for launching a test election
using Belenios plugged to OIDELi-zk and Google as idP. We successfully tested this on Debian 12 and Fedora 38.

The overall process takes around 20 minutes.
Since a ZKP is generated when the ballot is cast, at least 5GB of RAM are required ([see ZKP benchmarks](./oideli-zkp/README_benchmarks.md)).


### Setup 

1. prepare OpenID service from Google Cloud console (documentation: https://developers.google.com/identity/openid-connect/openid-connect)
    - go to the console: https://console.developers.google.com/
    - create a new project and select it
    - setup the "OAuth consent screen"
        - no need to add a scope
        - add a test user <`voter's email address`> (e.g., a GMail email address you own) which will be the email address of the voter you will make cast a ballot
    - create new credentials of type "OAuth client ID" 
        - application type "Desktop app"
    - data needed to continue:
        - client ID
        - client secret
        - voter's email address
2. build the docker image:
    - use the following commands and use your client ID and client secret
```
# get belenios and apply the OIDELI patch
./belenios-oideli/get_belenios_and_patch.sh

# build the docker base environment for belenios (takes quite some time)
docker build -f belenios-oideli/belenios/Dockerfile_base_environment -t beleniosbase ./belenios-oideli/belenios
# build belenios and add the zkp tool
docker build -f Dockerfile_belenios_oideli -t belenios-oideli .

# start a container and keeps it running until the end
docker run --network host -e CLIENT_ID='<client id>' -e CLIENT_SECRET='<client secret>' --name belenios-oideli --rm -it belenios-oideli
```
Warning: this is a TEST election, do not use it as is for a real election! (see the Belenios instructions)

3. create a test election
    - visit the election admin website `http://localhost:8001/admin` and log in with the login `admin`, no need to enter an email address and then enter the confirmation code, directly fetch the api token `<api token>` from http://localhost:8001/api-token
    - run the following command without interrupting the previous `docker run` command:
```
export VOTER_ID=<`voter's email address`>
export TOKEN=<api token>
# Obtain the ELECTION_UUID
docker exec -it belenios-oideli bash -c "source ./.belenios/env.sh; dune exec -- tests/scaling/main.exe setup --url=http://127.0.0.1:8001/ --admin-id=1 --api-token=$TOKEN --voter=$VOTER_ID | xargs" > ELECTION_UUID
# Store in an environment variable and remove extra symbols
export ELECTION_UUID=$(cat ELECTION_UUID)
export ELECTION_UUID="$(echo $ELECTION_UUID | tr '\r' '#' | sed -E 's/.*#([0-9a-zA-Z]*)#.*/\1/')"
# check ELECTION_UUID
echo $ELECTION_UUID | xargs echo
# Obtain the private credential for the voter VOTER_ID (this requires jq, for installation, for example for Debian, type `apt-get install jq`)
export CRED="$(docker exec -it belenios-oideli bash -c "$(printf 'cat scaling.%s.privcreds.json' "$ELECTION_UUID")" | jq -r ".\"$VOTER_ID\"")"
```

### Cast ballots and verifications
1. You can now login and cast a ballot by visiting the election website displayed by:
```
echo "http://127.0.0.1:8001/static/frontend/booth/vote.html#uuid=$ELECTION_UUID&credential=$CRED"
``` 
Use <`voter's email address`> when prompted for your email address. A Google login-page will pop up, use <`voter's email address`> to login.

Note: for this test election, eligibility ZKPs are computed on the fly so that you can directly test the eligibility ZKP verification, which is supposed to only happen at the end of the election in production.

2. you can look your ballot up on the link displayed by:
```
http://127.0.0.1:8001/elections/$ELECTION_UUID/ballots
```
and click on your ballot. This will displays the JSON file encoding your ballot with a field `oideli_data` containing the eligibility proof in the `eliproof` field.

3. you can verify that all ballots from the ballot box are valid, including the verification of all eligibility ZKPs:
```
# verify the whole election
docker exec -it belenios-oideli ./_run/usr/bin/belenios-tool election verify --url "http://localhost:8001/elections/$ELECTION_UUID/"
```


## Licence

We distinguish between the content of the `belenios-oideli` folder and the one of the other folders (`proverif-files`, `oideli-zkp` and `poseidon-wasm`).

### `belenios-oideli`

The content of the [`belenios-oideli`](belenios-oideli/) folder is licensed under the GNU Affero General Public License, Version 3.0 ([LICENSE-AGPLv3](LICENSE-AGPLv3) or [https://www.gnu.org/licenses/agpl-3.0.txt](https://www.gnu.org/licenses/agpl-3.0.txt)).

### Other folder

The content of the folders [`proverif-files`](proverif-files/), [`oideli-zkp`](oideli-zkp/) and [`poseidon-wasm`](poseidon-wasm/) are licensed under either of

- Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or [http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0))
- MIT license ([LICENSE-MIT](LICENSE-MIT) or [https://mit-license.org/license.txt](https://mit-license.org/license.txt))

at your option.
